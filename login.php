<?php
require_once 'resources/inc.config.php';
require_once 'resources/inc.config.php';

if (!$lg->isLoggedIn()) {
	if(isset($_POST['login_new'])) {
		$lg->checkLogin($_POST['email_address'], $_POST['password']);
	}
}
else {
	// user is already logged in, so redirect to my account page
	header('Location: my-account/');
	exit();
}

require_once 'resources/templates/tpl.header.php';
?>
    <div id="login-form">
    <?php
	// user has supplied incorrect login details or tried to view a restricted page
	if(isset($_SESSION['email']) || isset($_SESSION['not_logged_in'])) {
		if(isset($_SESSION['email'])) {
			$errMsg = 'You have supplied an incorrect email address or password. Please try again.';
		}
		else {
			$errMsg = 'Sorry you need to be logged in to view that page.';
		}
		// display error that have occured
		echo notifications::showNotification('error', TRUE, $errMsg);
		
		$lg->showNotLoggedIn();
		session_destroy();
	}
	else {
		// user is not logged in and no data has been sent to server so show login form
		$lg->showNotLoggedIn();
	}
    ?>
    </div>
    
    <div id="register">
        <h3>Don't have an account?</h3>
        <p><a href="registration.php" title="<?php echo config::SITE_NAME; ?> Registration">Register</a> now for FREE.</p>
    </div>	
<?php
require_once 'resources/templates/tpl.footer.php';
?>