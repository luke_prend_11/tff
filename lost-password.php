<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';

$uf = new user_functions();
if (isset($_POST['lostpass'])) {
	if ($uf->lostPassword($_POST['email_address'])) {
		// display any errors that have occured
		echo notifications::showNotification('success', TRUE, 'Your password has been reset, an email containing your new password has been sent to your inbox.');
		echo '<p>Please <a href="'.config::$baseUrl.'/login.php" title="Login to '.config::SITE_NAME.'">click here</a> to login to your account.</p>';
	}
	else {
		// display any errors that have occured
		echo notifications::showNotification('error', TRUE, 'The email address you entered has not been found. Please try again.');
		// display lost password form
		$uf->lostPasswordForm();
	}
}
else {
	//user has not pressed the button
	echo '<p>To reset your password just enter the email address you registered with in the box below and click reset password.</p>';
	$uf->lostPasswordForm();	
}

require_once 'resources/templates/tpl.footer.php';
?>