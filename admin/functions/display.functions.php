<?php
function show_private_leagues($mysqli)
{
	$stmt = config::$mysqli->prepare("SELECT league_id, league_name, league_password, user_id, date
	FROM private_leagues
	ORDER BY date ASC
	");
	$stmt->execute();
	$stmt->store_result();
	
	echo '
	<table border="0" cellpadding="0" cellspacing="0">
		<tr class="table-header">
			<td>ID</td>
			<td>League Name</td>
			<td>League Passcode</td>
			<td>User ID</td>
			<td class="right">Date</td>
		</tr>';
		
	$row_class = 0;	
	
	if ($stmt->num_rows == 0) {
		echo '<tr class="table-row'.$row_class.'">No transfers have been made during '.date("F", mktime(0, 0, 0, $month, 10)).'.</tr>';
	}
	else {
		$stmt->bind_result($league_id, $league_name, $league_password, $user_id, $date);
	
		while($stmt->fetch()) {	
			echo '<tr class="table-row'.$row_class.'">
			<td>'.$league_id.'</td>
			<td>'.$league_name.'</td>
			<td>'.$league_password.'</td>
			<td>'.$user_id.'</td>
			<td class="right">'.$date.'</td>
			</tr>';
			$row_class = 1 - $row_class;
		}
	}
	echo '
	</table>';
	$stmt->close();
}

function show_user_leagues($mysqli)
{
	$stmt = config::$mysqli->prepare("SELECT user_team_id, league_id, date_joined
	FROM user_leagues
	ORDER BY date_joined ASC
	");
	$stmt->execute();
	$stmt->store_result();
	
	echo '
	<table border="0" cellpadding="0" cellspacing="0">
		<tr class="table-header">
			<td>User Team ID</td>
			<td>League ID</td>
			<td class="right">Date</td>
		</tr>';
		
	$row_class = 0;	
	
	if ($stmt->num_rows == 0) {
		echo '<tr class="table-row'.$row_class.'">No transfers have been made during '.date("F", mktime(0, 0, 0, $month, 10)).'.</tr>';
	}
	else {
		$stmt->bind_result($user_team_id, $league_id, $date_joined);
	
		while($stmt->fetch()) {	
			echo '<tr class="table-row'.$row_class.'">
			<td>'.$user_team_id.'</td>
			<td>'.$league_id.'</td>
			<td class="right">'.$date_joined.'</td>
			</tr>';
			$row_class = 1 - $row_class;
		}
	}
	echo '
	</table>';
	$stmt->close();
}

function show_but($mysqli)
{
	echo '
	<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post">
	<input name="submit_new_field" type="submit" value="Submit" />
	</form>';
}

function add_id($mysqli)
{
	$stmt = config::$mysqli->prepare("UPDATE user_leagues 
   SET league_id = 67
 WHERE league_id = 8
 AND YEAR(date_joined) = '2014'");
	$stmt->execute();
	$stmt->close();
}

?>