<?php
function submitFixtures($fixture_id, $fixture_date)
{
	global $mysqli;
	$fixturesAry = array();
	foreach(array_keys($fixture_id) as $n) {
		//Check to see if fixture has been postponed
		
        //Unset any rows that do not contain results
		if (($ht_goals[$n] == '') && ($at_goals[$n] == '') && ($postponed[$n] == '')) {
			unset($resultsAry[$n]);	
		}
		else {
			//Check if postponed has been selected and nothing else entered
			if (($postponed != '') && (($ht_goals[$n] != '') || ($at_goals[$n] != '') || ($et[$n] != '') || ($h_pens[$n] != '') || ($a_pens[$n] != ''))) {
				echo "Postponed games should not have any other entries";
				return false;
			}
			else {
				//Check scores are not empty
				if ((($ht_goals[$n] == '') || ($at_goals[$n] == '')) && ($postponed == '')) {
					echo "Enter scores for both teams";
					return false;
				}
				else {
					//Check numbers have been entered as scores
					if (((!is_numeric($ht_goals[$n])) || (!is_numeric($at_goals[$n]))) && ($postponed == '')) {
						echo "Enter numbers";
						return false;
					}
					else {
						//Check that both pens boxes haven't been selected
						if (($h_pens[$n] != '') && ($a_pens[$n] != '')) {
							echo "Two teams cannot win on penalties";
							return false;
						}
						else {
							//Check that scores are equal if a team has won on pens
							if ((($h_pens[$n] != '') || ($a_pens[$n] != '')) && ($ht_goals[$n] != $at_goals[$n])) {
								echo "Scores are not equal";
								return false;
							}
							else {
								//Check that pens and et havn't been selected
								if ((($h_pens[$n] != '') || ($a_pens[$n] != '')) && ($et[$n] != '')) {
									echo "Extra time and pens selected";
									return false;
								}
								else {
									//Check that scores are not equal if extra time has been selected
									if (($ht_goals[$n] == $at_goals[$n]) && ($et[$n] != '')) {
										echo "Scores cannot be equal if extra time is selected";
										return false;
									}
									else {
										//Enter team id of team that won on pens into $penalties
										if ($h_pens[$n] != '') {
											$penalties = $h_pens;
										}
										elseif ($a_pens[$n] != '') {
											$penalties = $a_pens;
										}
										$id = intval($fixture_id[$n]);
										$hid = isset($home_id[$n]) ? intval($home_id[$n]) : 0;
										$aid = isset($away_id[$n]) ? intval($away_id[$n]) : 0;
										$htGoals = intval($ht_goals[$n]);
										$atGoals = intval($at_goals[$n]);
										$etVal = isset($et[$n]) ? intval($et[$n]) : 0;
										$pens = isset($penalties[$n]) ? intval($penalties[$n]) : 0;
										$post = isset($postponed[$n]) ? intval($postponed[$n]) : 0;
								
										$resultsAry[] = array($id, $htGoals, $atGoals, $etVal, $pens, $post);
										if ($postponed[$n] == '') {
											//do points calc function here
											list($ht_points, $at_points) = pointsCalc($id, $htGoals, $atGoals, $pens);
											$hpointsAry[] = array($id, $hid, $ht_points);
											$apointsAry[] = array($id, $aid, $at_points);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(empty($resultsAry)) {
		echo "You have not submitted any results";
	}
	else {
		$stmt = config::$mysqli->prepare("INSERT INTO results (fixture_id, ht_goals, at_goals, et, pens, postponed) 
		VALUES (?, ?, ?, ?, ?, ?)");
		$stmt->bind_param("iiiiii", $id, $htGoals, $atGoals, $etVal, $pens, $post);
				
		foreach ($resultsAry as $values) {
			$id = $values[0];
			$htGoals = $values[1];
			$atGoals = $values[2];
			$etVal = $values[3];
			$pens = $values[4];
			$post = $values[5];
			$stmt->execute();
		}
		$stmt->close();
	}
}
?>