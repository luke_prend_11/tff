<?php
require_once '../resources/inc.config.php';

// increase time limit for inserting results
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

require_once '../resources/templates/tpl.admin_header.php';

$ar = new admin_results($ug, $uf, $db);
$ar->displayAdminResultsPage();

require_once '../resources/templates/tpl.admin_footer.php';
