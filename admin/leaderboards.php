<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.admin_header.php';

if (isset($_GET['id']) && !empty($_GET['id'])) {
	$_SESSION['ldb_page_id'] = htmlspecialchars($_GET['id']);
}
?>

<a href="leaderboards.php?id=1" title="Overall Leaderboard" class="btn blue">Overall</a>
<a href="leaderboards.php?id=2" title="Month Leaderboard" class="btn dark-grey">Month</a>
<a href="leaderboards.php?id=3" title="Week Leaderboard" class="btn orange">Week</a>

<?php
$l = new leaderboards($ug);


if(isset($_SESSION['ldb_page_id']) && $_SESSION['ldb_page_id'] == 2) {
	echo '<h2>Month</h2>';
	// show month leaderboard
	echo $l->showLeaderboard('full', 'month');
}
elseif(isset($_SESSION['ldb_page_id']) && $_SESSION['ldb_page_id'] == 3) {
	echo '<h2>Week</h2>';
	// show week leaderboard
	echo $l->showLeaderboard('full', 'week');
}
else {
	echo '<h2>Overall</h2>';
	// show overall leaderboard
	echo $l->showLeaderboard('full', 'overall');
}

require_once '../resources/templates/tpl.admin_footer.php';
?>