<?php
require_once '../../resources/inc.config.php';

// increase time limit for importing results
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

require_once '../../resources/templates/tpl.admin_header.php';

$ai = new admin_import_results($db, $uf, $ug);
$ai->displayImportResultsPage();

require_once '../../resources/templates/tpl.admin_footer.php';
