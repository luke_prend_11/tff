<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.admin_header.php';

$mu = new admin_member_update();

// display options to select which months data to display
$mu->showSelectMonth();

if (isset($_POST['select_month']) || (isset($_SESSION['month']) && isset($_SESSION['year']))) {
	if (isset($_POST['select_month'])) {
		$month = $_SESSION['month'] = $_POST['month'];
		$year = $_SESSION['year'] = $_POST['year'];
	}
	else {
		$month = $_SESSION['month'];
		$year = $_SESSION['year'];
	}
	
	echo '
	<h3>Monthly Leaderboard</h3>';		
		// display monthly leaderboard
		$mu->showMonthLeaderboard($month, $year);
	
	echo '<img id="#collapse2" class="nav-toggle float-left" title="Expand/Collapse" src="'.config::$baseUrl.'/Images/Misc/fixtures-more.png" />
	<h3>Leaderboard at the end of the month</h3>
	<div id="collapse2" style="display:none">';
		// display leaderboard as it was at the end of the selected month
		$mu->showLeaderboardAsOf($month, $year, 'current');
		
		// display leaderboard as it was at the end of the month before the end of the selected month for comparison
		$mu->showLeaderboardAsOf($month, $year, 'previous');
	echo '</div>';
	
	echo '<img id="#collapse3" class="nav-toggle float-left" title="Expand/Collapse" src="'.config::$baseUrl.'/Images/Misc/fixtures-more.png" />
	<h3>Top/Bottom 10 Teams</h3>
	<div id="collapse3" style="display:none">';
		// display top 10 temas for the selected month
		$mu->showMonthTop10Teams($month, $year, 'top');
		
		// display bottom 10 temas for the selected month
		$mu->showMonthTop10Teams($month, $year, 'bottom');
	echo '</div>';
		
	echo '<img id="#collapse4" class="nav-toggle float-left" title="Expand/Collapse" src="'.config::$baseUrl.'/Images/Misc/fixtures-more.png" />
	<h3>Top transfers</h3>
	<div id="collapse4" style="display:none">';
		// display top 10 transferred in teams for current month
		$mu->showMonthTop10Transferred($month, $year, "in");
		
		// display top 10 transferred out teams for current month
		$mu->showMonthTop10Transferred($month, $year, "out");
	echo '</div>';
}
?>

<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
	<textarea rows="30" cols="80" name="message">
	<?php
	if(isset($_POST['preview'])) {
		echo $_POST['message'];
	}
	else {
	?>
	<p>Email introduction...</p>
	<h2>Manager of the Month</h2>
	<p><strong><?php if(isset($_SESSION['mom'])) echo $_SESSION['mom']; else echo 'MANAGER_OF_THE_MONTH'; ?></strong> of <strong><?php if(isset($_SESSION['mom_team_name'])) echo $_SESSION['mom_team_name']; else echo 'TEAM_OF_THE_MONTH'; ?></strong> was <strong><?php if(isset($_SESSION['month'])) echo date("F", mktime(0, 0, 0, $_SESSION['month'], 10)); else echo 'MONTH'; ?></strong> Manager of the Month on <strong><?php if(isset($_SESSION['mom_points'])) echo $_SESSION['mom_points']; else echo 'TEAM_OF_THE_MONTH_PTS'; ?> points</strong> with <em>Team Name</em> and <em>Team Name</em> scoring highly. <strong><?php if(isset($_SESSION['mom_team_name'])) echo $_SESSION['mom_team_name']; else echo 'TEAM_OF_THE_MONTH'; ?></strong> have moved up <em>No.</em> positions and are now <em>Position</em> in the overall leaderboard.</p>
	<h2>Top of the Leaderboard</h2>
	<p><strong><?php if(isset($_SESSION['leaderboard_1_team'])) echo $_SESSION['leaderboard_1_team']; else echo 'LEADREBOARD_1_TEAM'; ?></strong> top the leaderboard at the end of <strong><?php if(isset($_SESSION['month'])) echo date("F", mktime(0, 0, 0, $_SESSION['month'], 10)); else echo 'MONTH'; ?></strong> on <strong><?php if(isset($_SESSION['leaderboard_1_points'])) echo $_SESSION['leaderboard_1_points']; else echo 'LEADERBOARD_1_PTS'; ?> points</strong>. In 2nd place were <strong><?php if(isset($_SESSION['leaderboard_2_team'])) echo $_SESSION['leaderboard_2_team']; else echo 'LEADREBOARD_2_TEAM'; ?></strong> on <strong><?php if(isset($_SESSION['leaderboard_2_points'])) echo $_SESSION['leaderboard_2_points']; else echo 'LEADERBOARD_2_PTS'; ?> points</strong> and in 3rd were <strong><?php if(isset($_SESSION['leaderboard_3_team'])) echo $_SESSION['leaderboard_3_team']; else echo 'LEADREBOARD_3_TEAM'; ?></strong> on <strong><?php if(isset($_SESSION['leaderboard_3_points'])) echo $_SESSION['leaderboard_3_points']; else echo 'LEADERBOARD_3_PTS'; ?> points</strong>.</p>
	<h2>Other Stats</h2>
	<h3>Top Team of the Month</h3>
	<p><strong><?php if(isset($_SESSION['top_team'])) echo $_SESSION['top_team']; else echo 'TOP_TEAM'; ?></strong> - <strong><?php if(isset($_SESSION['top_team_points'])) echo $_SESSION['top_team_points']; else echo 'TOP_TEAM_POINTS'; ?> points</strong></p>
	<h3>Bottom Team of the Month</h3>
	<p><strong><?php if(isset($_SESSION['bottom_team'])) echo $_SESSION['bottom_team']; else echo 'BOTTOM_TEAM'; ?></strong> - <strong><?php if(isset($_SESSION['bottom_team_points'])) echo $_SESSION['bottom_team_points']; else echo 'BOTTOM_TEAM_POINTS'; ?> points</strong></p>
	<h3>Most Transferred In Team</h3>
	<p><strong><?php if(isset($_SESSION['trasfer_in_team'])) echo $_SESSION['trasfer_in_team']; else echo 'TRANSFER_IN_TEAM'; ?></strong> - <strong><?php if(isset($_SESSION['trasfer_in_count'])) echo $_SESSION['trasfer_in_count']; else echo 'TRANSFER_IN_COUNT'; ?> times</strong></p>
	<h3>Most Transferred Out Team</h3>
	<p><strong><?php if(isset($_SESSION['trasfer_out_team'])) echo $_SESSION['trasfer_out_team']; else echo 'TRANSFER_OUT_TEAM'; ?></strong> - <strong><?php if(isset($_SESSION['trasfer_out_count'])) echo $_SESSION['trasfer_out_count']; else echo 'TRANSFER_OUT_COUNT'; ?> times</strong></p>
	<h3>Transfers</h3>
	<p>Don't forget to log in to your account to check on your teams progress and make any transfers where you feel necessary.</p>
	<p>Regards<br />
	<?php echo config::SITE_NAME; ?><br />
	<a href="http://www.thefootballfrenzy.co.uk">www.thefootballfrenzy.co.uk</a></p>
	<p>If you would no longer like to receive email updates from <?php echo config::SITE_NAME; ?> please reply to this email with Unsubscribe in the subject line.</p>
	<?php
	}
	?>
	</textarea>
	<input name="preview" type="submit" value="Preview" />
</form>

<?php
if (isset($_POST['preview'])) {
	echo $_POST['message'];
}
?>
<?php
require_once '../resources/templates/tpl.admin_footer.php';
?>