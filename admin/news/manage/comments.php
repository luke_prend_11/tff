<?php
require_once '../../../resources/inc.config.php';
require_once '../../../resources/templates/tpl.admin_header.php';
?>
<h2>Comment on News</h2>
<?php
if (isset($_GET['id'])) {
	$id = $_GET['id'];
}
else {
	echo 'Please select a news post to view the comments.';
	exit();
}
$stmt = config::$mysqli->prepare("SELECT id, nid, title, author, comment, date 
FROM news_comments 
WHERE nid = ?");
$stmt->bind_param("i", $id);
$stmt->execute();
$stmt->bind_result($id, $nid, $title, $author, $comment, $date);

while ($stmt->fetch()) {
	echo '<b>'.$title.'</b>
	<b>Author : </b>'.$author.'<br />
	<b>Comment : </b>'.$comment.'<br />
	<hr width="80%" />';
}

$stmt->close();

if (isset($_POST['submitted'])) {

	$errors = array();
	if (empty($_POST['title'])) {
		$errors[] = '<font color="red">Please enter in a title.</font>';
	}
	else {
		$title = $_POST['title'];
	}

	if (empty($_POST['author'])) {
		$errors[] = '<font color="red">Please enter in your name.</font>';
	}
	else {
		$author = $_POST['author'];
	}

	if (empty($_POST['comment'])) {
		$errors[] = '<font color="red">Please enter in a comment.</font>';
	}
	else {
		$comment = $_POST['comment'];
	}

	if (empty($errors)) {
		$stmt = config::$mysqli->prepare("INSERT INTO news_comments (nid, title, author, comment, date)
		VALUES (?, ?, ?, ?, CONVERT_TZ(NOW(), '-08:00', '+00:00'))");
		$stmt->bind_param("isss", $id, $title, $author, $comment);
		if ($stmt->execute()) {
			echo '<font color="blue">Your comment was added succesfully!</font>';
		}
		else {
			echo '<font color="red">There was an error when submitting your comment, please try again.</font>';
		}
		$stmt->close();
	}
	else {
		echo '<b>There were a couple of errors -</b><br />';
		foreach ($errors as $msg) {
			echo " - $msg<br />\n";
		}
	}
}
else {
?>
<form action="<?php $_SERVER['../../PHP_SELF']; ?>" method="post" />
<p>Comment Title : <input type="text" name="title" maxlength="70" value="<?php if(isset($_POST['title'])) echo $_POST['author'];?>" /></p>

<p>Your Name : <input type="text" name="author" length="25" maxlength="50" value="<?php if(isset($_POST['author'])) echo $_POST['author'];?>" /></p>

<p>Comment : <textarea columns="6" rows="6" name="comment"><?php if(isset($_POST['comment'])) echo $_POST['comment'];?></textarea></p>

<p><div align="center"><input type="submit" name="submit" value="Submit Comment" /></div></p>
<input type="hidden" name="submitted" value="TRUE" />
</form>
<?php
}
?>
<?php
require_once '../../../resources/templates/tpl.admin_footer.php';
?>