<?php
require_once '../../../resources/inc.config.php';
require_once '../../../resources/templates/tpl.admin_header.php';
?>
<h2>Edit News</h2>
<?php
if ((isset($_GET['id'])) && (is_numeric($_GET['id'])) ) {
	$id = $_GET['id'];
}
elseif ( (isset($_POST['id'])) && (is_numeric($_POST['id'])) ) {
	$id = $_POST['id'];
}
else {
	echo 'Please choose a news post to edit.';
	exit();
}

if (isset($_POST['submitted'])) {
	$errors = array();

	if (empty($_POST['title'])) {
		$errors[] = 'You forgot to enter a title.';
	}
	else {
		$title = $_POST['title'];
	}

	if (empty($_POST['name'])) {
		$errors[] = 'You forgot to enter an author.';
	}
	else {
		$name = $_POST['name'];
	}

	if (empty($_POST['message'])) {
		$errors[] = 'You forgot to enter a message';
	}
	else {
		$message = $_POST['message'];
	}
	$image = $_POST['image'];

	if (empty($errors)) {
		$stmt = config::$mysqli->prepare("UPDATE news_posts SET title = ?, author = ?, post = ?, image  = ? WHERE id = ?");
		$stmt->bind_param("ssssi", $title, $name, $message, $image, $id);

		if ($stmt->execute()) {
			echo "News Post Has Been Updated!";
		}
		else {
			echo "News post could not be updated.";
		}
		$stmt->close();
	}
	else {
		echo 'News post could not be updated for the following reasons -<br />';
		foreach ($errors as $msg) {
			echo " - $msg<br />\n";
		}
	}
}
else {
	$stmt = config::$mysqli->prepare("SELECT title, author, post, image, id 
	FROM news_posts 
	WHERE id = ?");
	$stmt->bind_param("i", $id);
	$stmt->execute();
			$stmt->store_result();
	
	if ($stmt->num_rows == 1) {
		$stmt->bind_result($title, $author, $post, $image, $id);
	$stmt->fetch();
		echo '<h3>Edit News Post</h3>
		<form action="?id=edit-news&num='.$id.'" method="post">
		<p>News Title : <input type="text" name="title" size="25" maxlength="255" value="'.$title.'" /></p>
		<p>Name : <input type="text" name="name" size="15" maxlength="255" value="'.$author.'" /></p>
		<p>Message : <br /><textarea rows="10" cols="80" name="message">'.$post.'</textarea></p>
		<p>Image : <br /><input type="input" name="image" size="25" maxlength="60" value="'.$image.'" /></p>
		<p><input type="submit" name="submit" value="Submit" /></p>
		<input type="hidden" name="submitted" value="TRUE" /></p>
		<input type="hidden" name="id" value="'.$id.'" />';
	}
	else {
		echo 'News post could not be edited, please try again.';
	}
	$stmt->close();
}
?>
<?php
require_once '../../../resources/templates/tpl.admin_footer.php';
?>