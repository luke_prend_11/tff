<?php
require_once '../../../resources/inc.config.php';
require_once '../../../resources/templates/tpl.admin_header.php';
?>
<h2>Delete News</h2>
<?php
if (isset($_GET['id'])) {
	if (is_numeric($_GET['id'])) {
		$id = $_GET['id'];
	
		$stmt = config::$mysqli->prepare("DELETE FROM news_posts 
		WHERE id = ?");
		$stmt->bind_param("i", $id);
		
		if ($stmt->execute()) {
			echo '<h3>Success!</h3><br />
			The news item was deleted succesfully.<br /><br />
			<b>Options :</b><br />
			Delete or Edit another news item : <a href="news-manage.php">[X]</a><br />
			Add a new news item : <a href="add-news.php">[X]</a><br />';
		}
		else {
			echo 'We are sorry to inform you but the news item you chose to delete could not be deleted. Please feel free to try again';
		}
		$stmt->close();
	}
	else {
		echo 'Invalid news item, please choose a news item.';
	}
}
else {
	echo 'Before visiting this page please choose a news item to delete first!';
}
?>
<?php
require_once '../../../resources/templates/tpl.admin_footer.php';
?>