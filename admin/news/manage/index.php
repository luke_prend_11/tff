<?php
require_once '../../../resources/inc.config.php';
require_once '../../../resources/templates/tpl.admin_header.php';
?>
<h2>Old News</h2>
<?php
$stmt = config::$mysqli->prepare("SELECT id, title, date 
FROM news_posts
ORDER by date DESC");

if ($stmt->execute()) {
	$stmt->bind_result($id, $title, $date);
	echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="table-header">
	<td>Title</td>
	<td>Date/Time</td>
	<td>Delete</td>
	<td>Edit</td>
	<td>Comments</td>
	</tr>';
	$row_class = 0;
	while ($stmt->fetch()) {
		echo '<tr class="table-row'.$row_class.'">
		<td>'.$title.'</td>
		<td>'.$date.'</td>
		<td><a href="delete-news.php?id='.$id.'">Delete</a></td>
		<td><a href="edit-news.php?id='.$id.'">Edit</a></td>
		<td><a href="comments.php?id='.$id.'">Comments</a></td>
		</tr>';
		$row_class = 1 - $row_class;
	}
	echo '</table>';
}
else {
	echo 'Sorry, but we could not retrieve any news item records.';
}
$stmt->close();
?>
<?php
require_once '../../../resources/templates/tpl.admin_footer.php';
?>