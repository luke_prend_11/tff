<?php
require_once '../resources/inc.config.php';

$a_login = new admin_login(); 
if(isset($_POST['email'], $_POST['p']))
{
   $email    = $_POST['email'];
   $password = $_POST['p']; // The hashed password
   
   if($a_login->login($email, $password) == true)
   {
      // Login success
	  header('Location: '.config::$baseUrl.'/admin/index.php');
   }
   else
   {
      // Login failed
      header('Location: '.config::$baseUrl.'/admin/login.php?error=1');
   }
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Admin Login</title>

<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/forms.js"></script>
<link href="<?php echo config::$baseUrl; ?>/css/master.css" rel="stylesheet" type="text/css" />
<link href="<?php echo config::$baseUrl; ?>/css/admin.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php
if(isset($_GET['error']))
{ 
	echo 'Error Logging In!';
}
?>
<form action="" method="post" name="login_form" id="login">
	<label for="email">Email</label>
	<input type="text" name="email" />
	<label for="password">Password</label>
	<input type="password" name="password" id="password"/>
	<input type="submit" value="Login" onclick="formhash(this.form, this.form.password);" />
	<br class="clearfloat" />
</form>
</body>
</html>