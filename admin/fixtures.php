<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.admin_header.php';

$af = new admin_fixtures();

$af->showInsertFixturesPage();
?>

<?php if(isset($_SESSION['fix_page_id']) && $_SESSION['fix_page_id'] == 1) {?>
	<h2>Next 15 Fixtures</h2>
	
	<?php
    if (isset($_POST['submit_fixtures'])) {
        if ($af->checkUpdateFixtures($_POST['fixture_id'], $_POST['fixture_date'], $_POST['old_fixture_date'])) {
            echo '<p class="success">Fixtures have been updated</p>';
            $af->showModifyFixtures();
        }
        else {
            $af->showModifyFixtures();
        }
    }
    else {
        // has not pressed the submit button
        $af->showModifyFixtures();    
    }
    ?>
<?php } if(isset($_SESSION['fix_page_id']) && $_SESSION['fix_page_id'] == 2) {?>

	<h2>Select Fixture</h2>
	<?php
    if (isset($_POST['reset_fixture'])) {
        $af->showSelectFixtureComp('fixtures');
    }
    else {
        if (isset($_POST['submit_fixture'])) {
            if ($af->checkUpdateFixtures($_POST['fixture_id'], $_POST['fixture_date'], $_POST['old_fixture_date'])) {
                echo '<p class="success">Fixture has been updated</p>';
                $af->showSelectFixtureComp('fixtures');
            }
            else {
                // re-display the form if there is an error
                $af->showModifyFixtures($_SESSION['fixture']);
            }
        }
        elseif (isset($_POST['select_fixture'])) {
            $_SESSION['fixture'] = $_POST['fixture'];
            $af->showModifyFixtures($_SESSION['fixture']);
        }
        elseif (isset($_POST['select_comp'])) {
            $af->showSelectFixtures($_POST['comp']);
        }
        else {
            $af->showSelectFixtureComp('fixtures');
        }
    }
    ?>
<?php } if(isset($_SESSION['fix_page_id']) && $_SESSION['fix_page_id'] == 3) {?>

	<h2>Insert New Fixture</h2>
	<?php
    $uf = new user_functions();
    if (isset($_POST['reset_nf']) || (!isset($_POST['step_1']) && !isset($_POST['step_2']) && !isset($_POST['step_3']))) {	
        // get list of competitions
        echo '
        <form method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
            <div class="very-light-grey">'
				.$uf->showStickyForm('select','select_competition',$uf->getCompetitionsList()).
            '</div>
            <input type="submit" value="Select Competition" name="step_1" class="button" />
        </form>
        ';
    }
    elseif (isset($_POST['step_1'])) {	
        // store competition for use later
        $_SESSION['nf_comp'] = $_POST['select_competition'];
        // get list of teams for selected competition
        echo '
        <form method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
            <div class="very-light-grey">'
				.$uf->showStickyForm('select','select_home_team',$uf->getTeamsList($_SESSION['nf_comp']))
				.$uf->showStickyForm('select','select_away_team',$uf->getTeamsList($_SESSION['nf_comp']))
				.$uf->showStickyForm('text','time_and_date')
				.$uf->showStickyForm('text','round').
            '</div>
            <input type="submit" value="Submit Fixture Details" name="step_2" class="button" />
        </form>
        ';
    }
    elseif (isset($_POST['step_2'])) {	
        // store home team for use later
        $_SESSION['nf_ht'] = $_POST['select_home_team'];
        $_SESSION['nf_at'] = $_POST['select_away_team'];
        $_SESSION['nf_ko'] = $_POST['time_and_date'];
        $_SESSION['nf_round'] = $_POST['round'];
        // display text box to enter time and date of kick off
        echo '
        <form method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
            <div class="very-light-grey">
                <p>'.$_SESSION['nf_comp'].' - '.$_SESSION['nf_round'].'<br />'
                .$_SESSION['nf_ht'].' v '.$_SESSION['nf_at'].'<br />'
                .$_SESSION['nf_ko'].'</p>
            </div>
            <input type="submit" value="Confirm Fixture" name="step_3" class="button" />
            <input type="submit" value="Reset" name="reset_nf" class="button" />
        </form>
        ';
    }
    else {
		$stmt = config::$mysqli->prepare('SELECT t.team_id
		FROM team_names t
		WHERE t.team_name = ?
		LIMIT 1');
		$stmt->bind_param("s", $_SESSION['nf_ht']);
		$stmt->execute();
		$stmt->bind_result($ht);
		$stmt->fetch();
		
		$stmt->bind_param("s", $_SESSION['nf_at']);
		$stmt->execute();
		$stmt->bind_result($at);
		$stmt->fetch();
		$stmt->close();
		
		$stmt = config::$mysqli->prepare('SELECT c.comp_id
		FROM competitions c
		WHERE c.comp_name = ?
		LIMIT 1');
		$stmt->bind_param("s", $_SESSION['nf_comp']);
		$stmt->execute();
		$stmt->bind_result($c);
		$stmt->fetch();
		$stmt->close();
		
		if($db->completeInsert('match_fixtures', array('comp_id', 'ht_id', 'at_id', 'date', 'round'), array($c, $ht, $at, $_SESSION['nf_ko'], $_SESSION['nf_round']), array('i','i','i','s','s')))
                {
                    echo notifications::showNotification('success', TRUE, 'Fixture Inserted');
		}
		else
                {
                    echo notifications::showNotification('error', FALSE, 'There appears to have been an error inserting the fixture');
		}
    }
    ?>
<?php } if(isset($_SESSION['fix_page_id']) && $_SESSION['fix_page_id'] == 4) {?>

<h2>Insert New Fixtures from CSV</h2>
<p>Example path: C:/xampp/htdocs/tff/_notes/fixtures_test.csv</p>
<?php 
if (!isset($_POST['load_file']) && !isset($_POST['import_data'])) {	
	$sticky = new user_functions();
	// show button that will begin the import process
	echo '
	<form method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
		<div class="very-light-grey">'
				.$sticky->showStickyForm('text','file_path').
		'</div>
		<input type="submit" value="Load" name="load_file" class="button" />
	</form>
	';
}
elseif (isset($_POST['load_file'])) {	
	// store the file path
	$_SESSION['file_path'] = $_POST['file_path'];
	
	// call public function processCSV
	$af->processCSV($_SESSION['file_path']);
	// display total number of fixtures
	echo 'Total Number of fixtures: '.$af->fixtures_total.'<br>';
	// display total number of fixtures to skip
	echo 'fixtures to Skip: '.$af->fixtures_skipped.'<br>';
	// display total number of fixtures to import
	echo 'fixtures to Import: '.$af->fixtures_import.'<br>';
	
	// show button that will begin the import process
	echo '
	<form method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
		<div class="very-light-grey">
			<div>
				<label>Would you like to import the data?</label>
				<input type="submit" value="Import Data" name="import_data" class="button" />
			</div>
		</div>
	</form>
	';
}
// if button has been pressed execute the code below to import the data
else {
	// call public function processCSV
	$af->processCSV($_SESSION['file_path']);
	// call public function to insert the data intp MySQL table
	$af->insertQuery($af->import_data);
	
	// display total number of fixtures
	echo 'Total Number of Fixtures: '.$af->fixtures_total.'<br>';
	// display total number of fixtures skipped
	echo 'Fixtures Skipped: '.$af->fixtures_skipped;
	// display list of skipped fixtures
	echo $af->fixtureList($af->header_row, $af->skipped_data).'<br>';
	// display total number of fixtures imported
	echo 'Import Success: '.$af->import_success;
	// display list of imported fixtures
	echo $af->fixtureList($af->header_row, $af->success_items).'<br>';
	// display total number of fixtures that failed to import
	echo 'Import Failed: '.$af->import_failed;
	// display list of fixtures that failed to import
	echo $af->fixtureList($af->header_row, $af->failed_items).'<br>';
}

// unset all objects once finished
unset($af);
?>

<?php }?>

<?php
require_once '../resources/templates/tpl.admin_footer.php';
?>