<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.admin_header.php';

$am = new admin_mail();

if (isset($_POST['submitted'])) {
	if (empty($_POST['subject'])) {
		echo '<p><font color="red">You need to enter a subject.</font></p>';
	}
	else {
		$subject = $_POST['subject'];
	}

	if (empty($_POST['message'])) {
		echo '<p><font color="red">You need to enter a message.</font></p>';
	}
	else {
		$message = $_POST['message'];
	}
	
	if (empty($_POST['recipients'])) {
		echo '<p><font color="red">You need to select a recipient.</font></p>';
	}
	else {
		$recipients = $_POST['recipients'];
	}

	if (isset($subject) && isset($message) && isset($recipients)) {
		require_once '../resources/swift-mailer/swift_required.php';

		// Create the Transport
		$transport = Swift_SmtpTransport::newInstance('mail.thefootballfrenzy.co.uk', 25)
		  ->setUsername('webmaster@thefootballfrenzy.co.uk')
		  ->setPassword('IB1333143435')
		  ;
		
		/*
		You could alternatively use a different transport such as Sendmail or Mail:
		
		// Sendmail
		$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
		
		// Mail
		$transport = Swift_MailTransport::newInstance();
		*/
		
		// Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);
		
		// And specify a time in seconds to pause for (30 secs)
		$mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 30));
		
		// Rate limit to 4 emails per-minute
		$mailer->registerPlugin(new Swift_Plugins_ThrottlerPlugin(12, Swift_Plugins_ThrottlerPlugin::MESSAGES_PER_MINUTE));
		
		// Or to use the Echo Logger
		$logger = new Swift_Plugins_Loggers_EchoLogger();
		$mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
		
		// Create a message
		$message = Swift_Message::newInstance($subject)
		  ->setFrom(array('webmaster@thefootballfrenzy.co.uk' => config::SITE_NAME))
		  ->setBody($message)
		  ;
		
		if ($_POST['recipients'] == 'Test Members') {
			$stmt = config::$mysqli->prepare("SELECT email
			FROM test_email");
		}
		elseif ($_POST['recipients'] == 'Season Members') {
			$stmt = config::$mysqli->prepare("SELECT email
			FROM members u
			LEFT JOIN user_teams ut
			ON u.user_id = ut.user_id
			WHERE ut.date > ?
			AND u.suspended <> 1
			AND u.opt_out <> 1
			GROUP BY u.user_id");
			$stmt->bind_param("s", config::$curSeasonStart);
		}
		elseif ($_POST['recipients'] == 'Season Members > 623') {
			$stmt = config::$mysqli->prepare("SELECT email
			FROM members u
			LEFT JOIN user_teams ut
			ON u.user_id = ut.user_id
			WHERE ut.date > ?
			AND u.suspended <> 1
			AND u.opt_out <> 1
			AND u.user_id < 2
			GROUP BY u.user_id");
			$stmt->bind_param("s", config::$curSeasonStart);
		}
		elseif ($_POST['recipients'] == 'All Members') {
			$stmt = config::$mysqli->prepare("SELECT email
			FROM members u
			WHERE u.suspended <> 1
			GROUP BY u.user_id");
		}
		elseif ($_POST['recipients'] == 'All Members > 441') {
			$stmt = config::$mysqli->prepare("SELECT email
			FROM members u
			WHERE u.suspended <> 1
			AND u.user_id > 441
			GROUP BY u.user_id");
		}
		elseif ($_POST['recipients'] == 'Season Unactive Members') {
			$stmt = config::$mysqli->prepare("SELECT email
			FROM members u
			LEFT JOIN user_teams ut
			ON u.user_id = ut.user_id
			WHERE u.user_id NOT IN(
				SELECT u.user_id
				FROM members u
				LEFT JOIN user_teams ut
				ON u.user_id = ut.user_id
				WHERE ut.date > ?
				GROUP BY u.user_id
			)
			AND u.registration_date < ?
			AND u.suspended <> 1
			GROUP BY u.user_id");
			$stmt->bind_param("ss", config::$curSeasonStart, config::$curSeasonStart);
		}
		elseif ($_POST['recipients'] == 'Season New Members - No Teams Submitted') {
			$stmt = config::$mysqli->prepare("SELECT email 
			FROM members u
			LEFT JOIN user_teams ut
			ON u.user_id = ut.user_id
			WHERE registration_date > ?
			AND user_team_id IS NULL
			AND u.suspended <> 1
			GROUP BY u.user_id");
			$stmt->bind_param("s", config::$curSeasonStart);
		}
		else {
			echo '<p>Error! No recipient entered</p>';
			die;
		}
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($email);
		
		//store all the email addresses in an array
		$email_addresses = array();
		while($stmt->fetch()) {
			$email_addresses[] = $email;
		}
		$stmt->close();
		
		// Send the message
		$failedRecipients = array();
		$numSent = 0;
		
		foreach ($email_addresses as $address => $name)
		{
		  	if (is_int($address)) {
				$message->setTo($name);
		  	}
		  	else {
				$message->setTo(array($address => $name));
		  	}
			$numSent += $mailer->send($message, $failedRecipients);
		}
		
		printf("Sent %d messages\n", $numSent);
		
		// Dump the log contents
		// NOTE: The EchoLogger dumps in realtime so dump() does nothing for it
		echo $logger->dump();
	}
	else {
		echo '<p><font color="red">Please fill in the appropriate information</font></p>';
	}
}
$am->showEmailForm();
?>
<?php
require_once '../resources/templates/tpl.admin_footer.php';
?>