<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.admin_header.php';
?>
<script type="text/javascript" src="../js/sha512.js"></script>
<script type="text/javascript" src="../js/forms.js"></script>

<h2>Current Administrators</h2>
<?php
$stmt = config::$mysqli->prepare("SELECT email
FROM members_admin");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($email);
echo '<ul class="list-none">';

while($stmt->fetch()) {
	echo '<li>'.$email.'</li>';
}
echo '</ul>';
$stmt->close();

$uf = new user_functions();
?>

<h2>Add New Administrator</h2>
<form action="functions/registration.php" method="post" name="reg_form" class="form">
	<div class="very-light-grey">
		<?php
		echo $uf->showStickyForm('text','username').
		$uf->showStickyForm('text','email').
		$uf->showStickyForm('password','password')
		?>
		<input type="submit" value="Register" class="button" onclick="formhash(this.form, this.form.password);" />
	</div>
</form>
<?php
require_once '../resources/templates/tpl.admin_footer.php';
?>