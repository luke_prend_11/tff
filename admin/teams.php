<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.admin_header.php';

$at = new admin_teams($uf, $db);
$at->showAdminTeamsPage();

require_once '../resources/templates/tpl.admin_footer.php';
