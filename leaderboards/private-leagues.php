<?php
require_once '../../resources/inc.config.php';

// turn header scroll on for leaderboards
$header_scroll = TRUE;

// redirect to leaderboard page
if (isset($_REQUEST['ldb']))
{
	// save the referral ID
	$_SESSION['ldb'] = $_REQUEST['ldb'];
	
	$uu = new url_utils();
	// obtain the URL with no referral ID
	$clean_url = $uu->remove_query_param($_SERVER['REQUEST_URI'], 'ldb');
	// 301 redirect to the new URL
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: '.$clean_url);
}

// create new instance of leaderboards class
$l = new leaderboards($ug); 

require_once '../../resources/templates/tpl.header.php';

echo $db->showUpdateTime('ldb_overall');
$l->showPrivateLeagueLeaderboardPage();
	
require_once '../../resources/templates/tpl.footer.php';
?>
