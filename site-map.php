<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';
?>
	<ul class="list-none level-1">
		<li><a href="<?php echo config::$baseUrl; ?>" title="Home">Home Page</a></li>
		<li><a href="registration" title="Register Now">Registration</a></li>
		<li><a href="my-account/" title="My Account">My Account</a></li>
		<li><a href="stats" title="<?php echo config::SITE_NAME; ?> Stats">Statistics</a>
			<ul class="list-none">
				<li><a href="stats/leaderboards/" title="Leaderboards">Leaderboards</a>
					<ul class="list-none">
						<li><a href="stats/leaderboards/overall" title="Overall Leaderboard">Overall Leaderboard</a></li>
						<li><a href="stats/leaderboards/monthly" title="Monthly Leaderboard">Monthly Leaderboards</a></li>
						<li><a href="stats/leaderboards/private-leagues" title="Private Leagues">Private Leagues</a></li>
					</ul>
				</li>
				<li><a href="stats/team-data/" title="All Teams">All Teams</a></li>
				<li><a href="stats/fixtures" title="Football Fixtures">Fixtures</a></li>
				<li><a href="stats/results" title="Football Results">Results</a></li>
				<li><a href="stats/tables" title="Football League Tables">League Tables</a>
					<ul class="list-none">
						<?php
						$url_generator = new url_generator();
						
						$stmt = config::$mysqli->prepare("SELECT comp_id, comp_name
						FROM competitions
						WHERE comp_id <= 4
						ORDER BY comp_id ASC");
						$stmt->execute();
						$stmt->store_result();
						$stmt->bind_result($lg_id, $league_name);
						while ($stmt->fetch()) {
						?>
							<li><a href="<?php echo $url_generator->makeUrl($league_name, $lg_id, 'league_table'); ?>" title="Full <?php echo $league_name; ?> Table"><?php echo $league_name; ?></a></li>
						<?php
						}
						$stmt->close();
						?>
					</ul>
				</li>
			</ul>
		</li>
		<li><a href="news/" title="Football Frenzy News">News</a></li>
		<li><a href="help/" title="Need Help?">Help</a>
			<ul class="list-none">
				<li><a href="help/rules" title="How to Play">How To Play</a></li>
				<li><a href="help/faqs" title="Frequently Asked Questions">FAQ's</a></li>
				<li><a href="help/about-us" title="About Us">About Us</a></li>
				<li><a href="help/contact-us" title="Contact Us">Contact Us</a></li>
				<li><a href="help/prizes" title="Prizes">Prizes</a></li>
			</ul>
		</li>	
	</ul>
<?php
require_once 'resources/templates/tpl.footer.php';
?>