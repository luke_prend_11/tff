<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$mp = new matches_page($ug, $uf);
$ur = new url_redirector($ug);

// fix url if necessary
$ur->fixUrl();

require_once '../resources/templates/tpl.header.php';

$mp->displayMatchesPage();

require_once '../resources/templates/tpl.footer.php';
