<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$ut = new stats_user_teams($ug);
$ur = new url_redirector($ug);

// fix url if necessary
$ur->fixUrl();

require_once '../resources/templates/tpl.header.php';

$ut->displayUserTeamPage();

require_once '../resources/templates/tpl.footer.php';
