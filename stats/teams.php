<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$t  = new stats_teams($ug, $uf);
$ur = new url_redirector($ug);

// fix url if necessary
$ur->fixUrl();

require_once '../resources/templates/tpl.header.php';

$t->displayTeamPage();

require_once '../resources/templates/tpl.footer.php';
