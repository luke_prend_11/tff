<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$s = new stats($ug);

require_once '../resources/templates/tpl.header.php';

$s->displayStatsPage();

require_once '../resources/templates/tpl.footer.php';
