<?php
require_once '../../resources/inc.config.php';

// instantiate libraries to be used
$m = new matches($ug, $uf);

require_once '../../resources/templates/tpl.header.php';

echo $m->showCompetitionSelectionForm();
echo $m->displayMatches('matches', $m->matchCompId, 40);

require_once '../../resources/templates/tpl.footer.php';
