<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';

echo show_update_time('results');

if(isset($_POST['competition'])) {
	$_SESSION['competition'] = $_POST['competition'];
}

// Display competition drop down
echo show_SelectComp();

if(isset($_SESSION['competition']) && $_SESSION['competition'] != 0) {
	$query_comp = '= '.htmlspecialchars($_SESSION['competition']);
}
else {
	$query_comp = '<> 999';
}


// Requested page
$requested_page = isset($_GET['page']) ? intval($_GET['page']) : 1;

// Get the dates count
$date_count = getDateCount($query_comp, 'results');

$dates_per_page = 5;

// 55 dates with fixtures => $page_count = 3
$page_count = ceil($date_count / $dates_per_page);

// Check if $requested_page is > $page_count OR < 1,
// and redirect to the page one
$first_item_shown = ($requested_page - 1) * $dates_per_page;

// Set the max number of the page links
$max_pages = 9;

// Display the page links
echo show_page_numbers($page_count, $max_pages, $requested_page);

// select 5 dates to display matches for
$x = selectDisplayDates($query_comp, $first_item_shown, $dates_per_page, 'results');

echo showResults($query_comp, $x);
	
require_once '../resources/templates/tpl.footer.php';
?>