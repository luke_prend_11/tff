<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$c  = new stats_competitions($ug, $uf);
$ur = new url_redirector($ug);

// fix url if necessary
$ur->fixUrl();

require_once '../resources/templates/tpl.header.php';

$c->displayCompetitionPage();

require_once '../resources/templates/tpl.footer.php';
