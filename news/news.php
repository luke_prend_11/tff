<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$na = new news_article($ug);
$ur = new url_redirector($ug);

// fix url if necessary
$ur->fixNewsUrl();

require_once '../resources/templates/tpl.header.php';

$na->displayNewsArticle();


// creat a comments class for articles and private league pages
// need to display comments at the bottom of article
// add insert comments code before page is loaded
	
require_once '../resources/templates/tpl.footer.php';
?>