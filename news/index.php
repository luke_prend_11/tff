<?php
require_once '../resources/inc.config.php';

// instantiate libraries to be used
$n = new news($ug);

require_once '../resources/templates/tpl.header.php';

$n->displayNews();
	
require_once '../resources/templates/tpl.footer.php';
?>