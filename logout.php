<?php 
require_once 'resources/inc.config.php';
if (!isset($_COOKIE['user_id']) && !isset($_COOKIE['email'])) {
	header('Location: '.config::$baseUrl);
}
else {
	// set cookie to a time in the past to cause it to expire
	setcookie('user_id', '', 1, '/');
	setcookie('email', '', 1, '/');
	header('Location: '.config::$baseUrl);
}
?>