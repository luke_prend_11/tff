<?php
require_once 'resources/inc.config.php';

// redirect referral links
if (isset($_REQUEST['referral_id'])) {
	// save the referral ID
	$_SESSION['referral'] = $_REQUEST['referral_id'];
	
	$uu = new url_utils();
	// obtain the URL with no referral ID
	$clean_url = $uu->remove_query_param($_SERVER['REQUEST_URI'], 'referral_id');
	// 301 redirect to the new URL
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: '.$clean_url);
}

$reg = new account_registration($uf, $db); 
$reg->registrationSuccessAction();

require_once 'resources/templates/tpl.header.php';

$reg->registrationPageDisplay();

require_once 'resources/templates/tpl.footer.php';
?>