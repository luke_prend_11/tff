<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
    <p>Every effort has been taken to make this site as easy to use as possible, if you are having any problems then some of these frequently asked questions may help you out. If you can't find the answer then go to the <a href="contact-us.php" title="Contact Us">contact us</a> page and send us your query.</p>
    <h2>Popular Questions</h2>
    <ul class="list-none">
        <li><a href="#1" title="How do I log in?">How do I log in?</a></li>
        <li><a href="#2" title="I have forgotten my password, what do I do?">I have forgotten my password, what do I do?</a></li>
        <li><a href="#3" title="How do I register?">How do I register?</a></li>
        <li><a href="#4" title="Do I have to register if I played last season?">Do I have to register if I played last season?</a></li>
        <li><a href="#5" title="How old do I have to be to play?">How old do I have to be to play?</a></li>
        <li><a href="#6" title="Can I have more than one account?">Can I have more than one account?</a></li>
        <li><a href="#7" title="When do I make my team selections?">When do I make my team selections?</a></li>
        <li><a href="#8" title="How many teams can I submit?">How many teams can I submit?</a></li>
        <li><a href="#9" title="How many teams can I choose?">How many teams can I choose?</a></li>
        <li><a href="#10" title="What if I make a mistake when selecting my teams?">What if I make a mistake when selecting my teams?</a></li>
        <li><a href="#11" title="What are transfers?">What are transfers?</a></li>
        <li><a href="#12" title="How do I make Transfers?">How do I make Transfers?</a></li>
        <li><a href="#13" title="When is the site updated?">When is the site updated?</a></li>
        <li><a href="#14" title="How are my teams performing?">How are my teams performing?</a></li>
        <li><a href="#15" title="What is a Private League?">What is a Private League?</a></li>
    </ul>
  
    <a name="1" id="1"></a>
    <h3 class="question">Q. How do I log in?</h3>
    <p class="answer">A. To log in you need to use the email address and password you registered with and enter them into the login box, which is located under the top menu bar of every page. Or alternatively, visit the <a href="../login.php" title="Log in to <?php echo config::SITE_NAME; ?>">login</a> page. If you don’t have an account you can <a href="../registration.php" title="Registration">register</a> for free.</p>
    <a name="2" id="2"></a>
    <h3 class="question">Q. I have forgotten my password, what do I do?</h3>
    <p class="answer">A. If you have forgotten your password then please <a href="../lost-password.php" title="Lost Password">click here</a> to get it reset.</p>
    <a name="3" id="3"></a>
    <h3 class="question">Q. How do I register?</h3>
    <p class="answer">A. If you don’t have an account you can <a href="../registration.php" title="Registration">register</a> for free. Fill in all required fields and click submit. You will then be directed to submit your first team. You may submit 3 teams in total.</p>
    <a name="4" id="4"></a>
    <h3 class="question">Q. Do I have to register if I played last season?</h3>
    <p class="answer">A. No, you will be able to use your existing log in details to submit a team for the new season.</p>
    <a name="5" id="5"></a>
    <h3 class="question">Q. How old do I have to be to play?</h3>
    <p class="answer">A. <?php echo config::SITE_NAME; ?> is open to all ages.</p>
    <a name="6" id="6"></a>
    <h3 class="question">Q. Can I have more than one account?</h3>
    <p class="answer">A. No, you can only have one account per email address. However, you can submit a maximum of 3 teams per account. Just <a href="../login.php">sign in</a> using the email address and password you registered with and click on 'Submit a Team'.</p>
    <a name="7" id="7"></a>
    <h3 class="question">Q. When do I make my team selections?</h3>
    <p class="answer">A. Playing <?php echo config::SITE_NAME; ?> is really easy. Once you've registered your details, you will be automatically directed to submit your first team. If you would like to submit extra teams or if you have already registered in a previous season then simply log in to your account and click 'Submit a Team' from your account menu on the left.</p>
    <a name="8" id="8"></a>
    <h3 class="question">Q. How many entries can I submit?</h3>
    <p class="answer">A. You can submit upto 3 teams.</p>
    <a name="9" id="9"></a>
    <h3 class="question">Q. How many teams do I choose?</h3>
    <p class="answer">A. For each entry you submit, you will be required to select 11 teams from the top four English divisions. The 11 teams will be made up of 3 from the Premiership, 3 from the Championship, 3 from League One and 2 from League Two.</p>
    <a name="10" id="10"></a>
    <h3 class="question">Q. What if I make a mistake when selecting my teams?</h3>
    <p class="answer">A. Don’t worry, you can make as many transfers as you like up until the first game of the season has kicked off. Once the season has started you are limited to 40 transfers, which you can make when needed throughout the season.</p>
    <a name="11" id="11"></a>
    <h3 class="question">Q. What are transfers?</h3>
    <p class="answer">A. Transfers allow you to replace one team for another. Transfers can only be done between teams from the same selection group and within the given budget. You have unlimited transfers until the first game of the season, after that you can make 40 transfers.</p>
    <a name="12" id="12"></a>
    <h3 class="question">Q. How do I make Transfers?</h3>
    <p class="answer">A. To make a transfer log into your account and select the team you would like to make transfers for. Then select transfers from the extended menu that appears down the left side of the screen. Choose which team you would like to be replaced before clicking next. Then select your new team, ensuring that your transfer budget is not exceeded.</p>
    <a name="13" id="13"></a>
    <h3 class="question">Q. When is the site updated?</h3>
    <p class="answer">A. We aim to update the site after every game has finished playing but in some cases it may take longer.</p>
    <a name="14" id="14"></a>
    <h3 class="question">Q. How are my teams performing?</h3>
    <p class="answer">A. You can find out how well your teams are doing by logging into your account and looking under 'My Teams', clicking on the team name will give you lots of information on how that team is doing. Alternatively you can go to the <a href="../leaderboards/" title="Overall Leaderboard">leaderboards</a> page to view your team's position in relation to the other teams.</p>
    <a name="15" id="15"></a>
    <h3 class="question">Q. What is a Private League?</h3>
    <p class="answer">A. A Private League is a league created by a user to play against friends, family or colleagues.</p>
    <h2>More Information</h2>
    <ul class="list-none">
        <li><a href="rules.php" title="How to Play TFF">How to Play</a></li>
        <li><a href="contact-us.php" title="Contact TFF">Contact Us</a></li>
        <li><a href="about-us.php" title="About TFF">About Us</a></li>
        <li><a href="prizes.php" title="Prizes">Prizes</a></li>
    </ul>
<?php
require_once '../resources/templates/tpl.footer.php';
?>