<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
    <p>Prizes for <?php echo config::SITE_NAME; ?> <?php echo config::$curSeasonName; ?> season will be distributed as follows:</p>
    <h2>Winner</h2>
    <p>The prize for finishing as the overall winner of the <?php echo config::$curSeasonName; ?> season is £50.</p>
    <!--<h2>Most Referrals</h2>
    <p>Congratulations to Russell Cane for winning £50 after referring 69 new members to <?php /* echo config::SITE_NAME; */ ?> before the deadline on <strong><?php /* echo date("l j F Y", strtotime(config::$curSeasonStart)).' at '.date("H:i", strtotime(config::$curSeasonStart)); */ ?></strong>. The top 5 most referrals are shown in the table below:</p>-->
    <?php
    /*
    $stmt = config::$mysqli->prepare("SELECT u1.first_name, u1.last_name, COUNT(*) AS total
    FROM members u
    LEFT JOIN members u1
    ON u.referral = u1.user_id
    WHERE u.referral <> 0 
    AND u.registration_date > ?
    AND u.registration_date < ?
    GROUP BY u.referral
    ORDER BY total DESC, u1.last_name ASC
    LIMIT 5");
    $stmt->bind_param('ss', $comp_open, config::$curSeasonStart);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows == 0) {
        echo '<p class="error">There have been no referrals to '.config::SITE_NAME.' for the current season.</p>';
    }
    else {
        echo '<table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr class="table-header">
            <td>Pos</td>
            <td>Name</td>
            <td class="right">Referrals</td>
          </tr>';
        $rank = 1;		  
        $row_class = 0;
        $stmt->bind_result($first_name, $last_name, $referrals);
        while($stmt->fetch()) {	
            echo '<tr class="table-row'.$row_class.'">
                <td>'.$rank.'</td>
                <td>'.$first_name.' '.$last_name.'</td>
                <td class="right">'.$referrals.'</td>
              </tr>';
                  
            $row_class = 1 - $row_class;
            $rank ++;
        }
        $stmt->close();
        
        echo '</table>';
        echo '<br />';
    }
    */
    ?>
    <h2>More Information</h2>
    <ul class="list-none">
        <li><a href="rules.php" title="How to Play TFF">How to Play</a></li>
        <li><a href="faqs.php" title="Frequently Asked Questions">FAQs</a></li>
        <li><a href="contact-us.php" title="Contact TFF">Contact Us</a></li>
        <li><a href="about-us.php" title="About TFF">About Us</a></li>
    </ul>	
<?php
require_once '../resources/templates/tpl.footer.php';
?>