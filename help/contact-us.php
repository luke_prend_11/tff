<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';

if (isset($_POST['submitform']))
{
    notifications::$formNotification = $uf->checkContactForm($_POST['name'], $_POST['email_address'], $_POST['comments']);
    if(empty(notifications::$formNotification))
    {
        if ($uf->sendContactForm($_POST['name'], $_POST['email_address'], config::CONTACT_EMAIL, $_POST['comments']))
        {
            // display error if contact form fails to send
            echo notifications::showNotification('success', TRUE, 'Thank you for contacting '.config::SITE_NAME.'.<br />We aim to respond to your message within 24 hours.');
        }
        else
        {
            // display error if contact form fails to send
            echo notifications::showNotification('error', FALSE, 'Sorry but there appears to have been an error, please try again.');
            // display contact form
            $uf->contactForm();
        }
    }
    else
    {
        // display any errors that have occured
        echo notifications::showNotification('error');
        // display contact form
        $uf->contactForm();
    }
}
else
{
    echo '<p>To get in touch please fill out the form below and we will get back to you as soon as possible.</p>';
    $uf->contactForm();
}

require_once '../resources/templates/tpl.footer.php';
