<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
    <ul class="list-none">
        <li><a href="#registration" title="How to Register">Registration</a></li>
        <li><a href="#submit" title="How to Submit a Team">Submit a Team</a></li>
        <li><a href="#points" title="How Teams Score Points">Scoring Points</a></li>
        <li><a href="#transfers" title="How to Make Transfers">Transfers</a></li>
        <li><a href="#more" title="More Information">More Information</a></li>
    </ul>
    <a name="registration" id="registration"></a>
    <h2>Registration</h2>
    <p>First you will need to <a href="../registration.php" title="Register">register</a> a few details. Simply, fill out the form, making sure all required fields are entered. Then, click submit. If you have played in previous seasons then there is no need to register again, you can log into your account as normal and submit a team.</p>
    <a name="submit" id="submit"></a>
    <h2>Submit a Team</h2>
    <p>Once you have registered you will be automatically redirected to submit your first team. If you are already registered then <a href="../login.php" title="Log in to My Account">login</a> to your account. From there you  just need to click the 'Submit a Team' link under 'My Teams'.</p>
    <p>Come up with a team name and enter it in the first box under 'Submit a Team'. Then choose 11 teams, one team from each selection group making sure the total value of the teams you choose does not exceed the given budget. Once you have made your choices click 'Submit Team'.</p>
    <p>You will then see a confirmation message given there were no errors.</p>
    <a name="points" id="points"></a>
	<h2>Scoring Points</h2>
    <?php echo notifications::$scoringPointsInfo; ?>
    <p>Teams can score points only in the following competitions:</p>
    <ul class="list-points">
        <li>Premier League</li>
        <li>Championship</li>
        <li>League One</li>
        <li>League Two</li>
        <li>FA Cup</li>
        <li>League Cup</li>
        <li>Football League Trophy</li>
    </ul>
    <p>In cup matches where games go to extra time, points for goals are counted as normal. If a game goes to penalties, the goals scored in the penalty shootout are not counted but the outcome of the shootout is.</p>
    <a name="transfers" id="transfers"></a>
    <h2>Transfers</h2>
    <p>You can make as many transfers as you like until the first game of the season has kicked off, after this point you will have a total of 40 transfers that can be made throughout the season.</p>
    <p>To make a transfer, log in to your account, select the team you require under 'My Teams'. Then select 'Transfers' from the menu down the sub menu. Then just follow the steps to select a team.</p>
    <a name="more" id="more"></a>
    <h2>More Information</h2>
    <ul class="list-none">
        <li><a href="about-us.php" title="About Us">About Us</a></li>
        <li><a href="faqs.php" title="Frequently Asked Questions">FAQs</a></li>
        <li><a href="contact-us.php" title="Contact TFF">Contact Us</a></li>
        <li><a href="prizes.php" title="Prizes">Prizes</a></li>
    </ul>
<?php
require_once '../resources/templates/tpl.footer.php';
?>