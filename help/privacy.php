<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
    <p>This site uses unobtrusive cookies. A cookie is a small amount of data that is stored in your web browser for a set time period, mainly as a way for websites to remember useful information like items saved in a shopping basket. This is so that if you close your browser window the items you added are remembered next time you visit.</p>
    <h2>Google Analytics</h2>
    <p>Google Analytics is a powerful tool which this site makes use of to track visitors behaviour whilst browsing the site. It works by placing two cookies in your browser that ultimately keep a record of which pages you visit and how long you are active for. This helps us to decide which pages to target and work on. No personal information is stored and kept by Google.</p>
    <h2>Login Information</h2>
    <p>There are also two cookies that are used when you log in. An email address and user ID are stored for a period of 30 days for your convenience so that you don't have to log in everytime you visit. You can safely delete these cookies simply by logging out of your account.</p> 
    <h2>EU Cookie Law</h2>
    <p>A cookie is also stored when you accept the cookie policy the first time you visit. Ensuring the message doesn't pop up every time you return!</p>
<?php
require_once '../resources/templates/tpl.footer.php';
?>