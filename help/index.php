<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

        <div class="row stretch-parent">
            <div class="col-1-5-x-1 stretch-child">
                <div class="very-light-grey">
                    <h2 class="news dark-grey">How To Play</h2>
                    <div class="inner-container">
                        <p>Visit the how to play page for detailed information on registration, submitting a new team and a breakdown on how your teams can score points.</p>
                        <a href="rules.php" class="btn blue" title="How to Play">How To Play</a>
                    </div>
                </div>
                
                <div class="very-light-grey">
                    <h2 class="news dark-grey">FAQ's</h2>
                    <div class="inner-container">
                        <p>Visit the FAQ's page For a list of common questions about the use of <?php echo config::SITE_NAME; ?> site in general.</p>
                        <a href="faqs.php" class="btn blue" title="Frequently Asked Questions">FAQ's</a>
                    </div>
                </div>
            </div>
            
            <div class="col-1-5-x-1 stretch-child">
                <div class="very-light-grey">
                    <h2 class="news dark-grey">Contact Us</h2>
                    <div class="inner-container">
                        <p>For anything else go to the contact us page and submit a form and we will get back to you asap.</p>
                        <a href="contact-us.php" class="btn blue" title="Contact Us">Contact Us</a>
                    </div>
                </div>
                
                <div class="very-light-grey">
                    <h2 class="news dark-grey">About Us</h2>
                    <div class="inner-container">
                        <p>The history of <?php echo config::SITE_NAME; ?>.</p>
                        <a href="about-us.php" class="btn blue" title="About Us">About Us</a>
                    </div>
                </div>
                
                <div class="very-light-grey">
                    <h2 class="news dark-grey">Prizes</h2>
                    <div class="inner-container">
                        <p>Read about what you could win if you play <?php echo config::SITE_NAME; ?>.</p>
                        <a href="prizes.php" class="btn blue" title="Prizes">Prizes</a>
                    </div>
                </div>
            </div>
            <span class="stretch"></span>
        </div>

<?php
require_once '../resources/templates/tpl.footer.php';
