<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
	<p><?php echo config::SITE_NAME; ?> is a free fantasy football game unlike any other. Each member is required to select 11 teams from the top four english leagues. Teams score points based on their real-life results.</p>
	<p><?php echo config::SITE_NAME; ?> has evolved over the last few years. It began in 2010 and was originally an email based competition with only 40 members taking part in the first season.</p>
	<p>Following on from the success of the first season, a website was built in order to attract more players and make the game more interactive. The site went live in July 2011 and since the launch, over 1000 members have signed up. The website and competition is constantly being improved and has had 3 full re-designs to keep up with the demand for new features and better functionality.</p>
	<h2>More Information</h2>
	<ul class="list-none">
		<li><a href="rules.php" title="How to Play TFF">How to Play</a></li>
		<li><a href="faqs.php" title="Frequently Asked Questions">FAQs</a></li>
		<li><a href="contact-us.php" title="Contact TFF">Contact Us</a></li>
		<li><a href="prizes.php" title="Prizes">Prizes</a></li>
	</ul>
<?php
require_once '../resources/templates/tpl.footer.php';
?>