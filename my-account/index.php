<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.account_header.php';
?>
<p class="welcome"><?php echo $ac->welcomeMessage; ?></p>
<h2>My Teams</h2>
<?php
$ac->showUserTeams();
?>

<h2>My Leagues</h2>
<?php
$al = new account_leagues($db, $ug, $uf); 
$al->checkLeaguePageDisplay();
?>

<?php
require_once '../resources/templates/tpl.account_footer.php';
?>