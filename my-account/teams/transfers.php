<?php
require_once '../../resources/inc.config.php';

// instantiate libraries to be used
$at = new account_teams($db, $ug, $uf, '');

require_once '../../resources/templates/tpl.account_header.php';

$at->showUserTeamTransfersPage();
require_once '../../resources/templates/tpl.account_footer.php';
?>