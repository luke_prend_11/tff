<?php
require_once '../../resources/inc.config.php';

// instantiate libraries to be used
$ur = new url_redirector($ug);
// fix url if necessary
$ur->fixUrl();
$at = new account_teams($db, $ug, $uf, $ur->linkString);


require_once '../../resources/templates/tpl.account_header.php';
$at->showUserTeamPage();
require_once '../../resources/templates/tpl.account_footer.php';
?>