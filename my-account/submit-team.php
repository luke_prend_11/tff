<?php
require_once '../resources/inc.config.php';

// turn submit team scripts on
$submit_team = TRUE;

require_once '../resources/templates/tpl.account_header.php';

$st = new account_submit_team();

if(isset($_SESSION['just_registered']))
{
	echo notifications::showNotification('success', TRUE, $st->registrationSuccessMessage);
	unset($_SESSION['just_registered']);
}		
$st->submitTeam();
?>

<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/show-hide.js"></script>
<?php
require_once '../resources/templates/tpl.account_footer.php';
?>