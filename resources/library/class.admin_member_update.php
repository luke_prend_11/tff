<?php
class admin_member_update
{
	public function showSelectMonth()
	{		
		echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post">';
		
		$stmt = config::$mysqli->prepare("SELECT date, MONTH(date)
		FROM fixtures
		LEFT JOIN results
		ON fixtures.fixture_id = results.fixture_id
		WHERE results.fixture_id IS NOT NULL
		GROUP BY MONTH(date)
		ORDER BY date ASC");
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows != 0) {
			$stmt->bind_result($date, $month);
			
			echo '<select name="month">';
			
			while($stmt->fetch()) {
				echo '<option value="'.$month.'"';
				if(isset($_SESSION['month']) && $_SESSION['month'] == $month) echo ' selected="selected"';
				echo '>'.date("F", strtotime($date)).'</option>';
			}
		}
		$stmt->close();
		
		echo '</select>';
		
		$stmt = config::$mysqli->prepare("SELECT date, YEAR(date)
		FROM fixtures
		LEFT JOIN results
		ON fixtures.fixture_id = results.fixture_id
		WHERE results.fixture_id IS NOT NULL
		GROUP BY YEAR(date)
		ORDER BY date ASC");
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows != 0) {
			$stmt->bind_result($date, $year);
			
			echo '<select name="year">';
			
			while($stmt->fetch()) {
				echo '<option value="'.$year.'"';
				if(isset($_SESSION['year']) && $_SESSION['year'] == $year) echo ' selected="selected"';
				echo '>'.date("y", strtotime($date)).'</option>';
			}
		}
		$stmt->close();
		
		echo '</select>
		
		<input name="select_month" type="submit" value="Submit" class="button" />
	</form>';
	}
	
	public function showMonthLeaderboard($month, $year)
	{	
		$stmt = config::$mysqli->prepare("SELECT members.first_name, members.last_name, ut.user_id, ut.user_team_id, ut.user_team_name, ut.date, x.total, x.overall_rank
		FROM user_teams ut
		LEFT JOIN (
			SELECT overall.user_team_id, overall.total, (@row := @row + 1) AS overall_rank
			FROM (
				SELECT user_teams.user_team_id, COALESCE(SUM(CASE WHEN team_selections.transfer_in < fixtures.date AND team_selections.transfer_out > fixtures.date AND fixtures.date > ? AND MONTH(fixtures.date) = ? AND YEAR(fixtures.date) = ? THEN team_points.points ELSE 0 END),0) as total
				FROM user_teams
				LEFT JOIN team_selections
				ON user_teams.user_team_id = team_selections.user_team_id
				LEFT JOIN team_points
				ON team_selections.team_id = team_points.team_id
				LEFT JOIN fixtures
				ON team_points.fixture_id = fixtures.fixture_id
				WHERE user_teams.date > ?
				GROUP BY user_teams.user_team_id
				ORDER BY total DESC, user_teams.user_team_name ASC
			) overall, (SELECT @row := 0) r
		) AS x
		ON x.user_team_id = ut.user_team_id
		LEFT JOIN members
		ON ut.user_id = members.user_id
		WHERE ut.date > ?
		ORDER BY overall_rank ASC, ut.user_team_name ASC
		LIMIT 10");
		$stmt->bind_param("sssss", config::$curSeasonStart, $month, $year, config::$curSeasonStart, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($first_name, $last_name, $user_id, $user_team_id, $user_team_name, $date, $total, $overall_rank);
		
		echo '
		<div>
		<h2>'.date("F", mktime(0, 0, 0, $month, 10)).' '.$year.' Leaderboard</h2>
		<table width="340" border="0" cellpadding="0" cellspacing="0">
			<tr class="table-header">
				<td>&nbsp;</td>
				<td>Manager</td>
				<td>Team Name</td>
				<td class="right">Pts</td>
			</tr>';
		$row_class = 0;
		$counter = 1;	
		while($stmt->fetch()) {	
			// store manager of the month for email
			if($counter == 1) {
				$_SESSION['mom'] = $first_name.' '.$last_name;
				$_SESSION['mom_team_name'] = $user_team_name;
				$_SESSION['mom_points'] = $total;
			}
			// display top 10 monthly leaderboard
			echo '<tr class="table-row'.$row_class.'">
			<td>'.$counter.'</td>
			<td>'.$first_name.' '.$last_name.'</td>
			<td>'.showLink($user_team_name, $user_team_id, 'user_team').'</td>
			<td class="right">'.$total.'</td>
			</tr>';
			$row_class = 1 - $row_class;
			$counter++;
		}
		echo '
		</table>
		</div>';
		$stmt->close();
	}
	
	public function showMonthTop10Teams($month, $year, $x)
	{
		if($x == 'top') {
			$query = 'DESC';
			$query1 = 'ASC';
		}
		else {
			$query = 'ASC';
			$query1 = 'DESC';
		}
		
		$stmt = config::$mysqli->prepare("SELECT teams.team_id, team_name, COUNT(DISTINCT fixtures.fixture_id) As pld, SUM(tpts.points) as month, competitions.short_name
		FROM tff_teams
		LEFT JOIN teams
		ON teams.team_id = tff_teams.team_id
		LEFT JOIN team_points tpts
		ON teams.team_id = tpts.team_id
		LEFT JOIN fixtures
		ON tpts.fixture_id = fixtures.fixture_id
		LEFT JOIN competitions
		ON tff_teams.league_id = competitions.comp_id
		WHERE league_id <> 0
		AND MONTH(fixtures.date) = ?
		AND YEAR(fixtures.date) = ?
		GROUP BY teams.team_id
		ORDER BY month ".$query.", pld ".$query1.", team_name ASC
		LIMIT 10");
		$stmt->bind_param("ss", $month, $year);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($team_id, $team_name, $pld, $month_points, $comp_name);
		
		echo '
		<div class="float-left">
		<h2>'.date("F", mktime(0, 0, 0, $month, 10)).' '.$year.' '.$x.' 10 Teams</h2>
		<table width="340" border="0" cellpadding="0" cellspacing="0">
			<tr class="table-header">
				<td>&nbsp;</td>
				<td>Team</td>
				<td>Div</td>
				<td class="right">Pld</td>
				<td class="right">Pts</td>
				<td class="right">Pts/Pld</td>
				<td class="right">%</td>
			</tr>';
		$row_class = 0;
		$counter = 1;	
		while($stmt->fetch()) {	
			// store team data for email message
			if($counter == 1 && $x == 'top') {
				$_SESSION['top_team'] = $team_name;
				$_SESSION['top_team_points'] = $month_points;
				$_SESSION['top_team_pld'] = $pld;
			}
			if($counter == 1 && $x == 'bottom') {
				$_SESSION['bottom_team'] = $team_name;
				$_SESSION['bottom_team_points'] = $month_points;
				$_SESSION['bottom_team_pld'] = $pld;
			}
			// display top 10 teams
			echo '<tr class="table-row'.$row_class.'">
			<td>'.$counter.'</td>
			<td>'.show_team_link($team_name, $team_id).'</td>
			<td>'.$comp_name.'</td>
			<td class="right">'.$pld.'</td>
			<td class="right">'.$month_points.'</td>
			<td class="right">'.round($month_points/$pld,1).'</td>';
			
			$stmt1 = config::$mysqli->prepare("SELECT COUNT(*) / (SELECT COUNT(*) FROM user_teams WHERE date > ?) * 100 AS percent
			FROM user_teams
			LEFT JOIN team_selections
			ON user_teams.user_team_id = team_selections.user_team_id
			WHERE team_selections.team_id = ?
			AND user_teams.date > ?");
			$stmt1->bind_param("sis", config::$curSeasonStart, $team_id, config::$curSeasonStart);
			$stmt1->execute();
			$stmt1->store_result();
			$stmt1->bind_result($percentage);
			$stmt1->fetch();
			$stmt1->close();
			
			echo '<td class="right">'.round($percentage).'%</td>
			</tr>';
			$row_class = 1 - $row_class;
			$counter++;
		}
		echo '
		</table>
		</div>';
		$stmt->close();
	}
	
	public function showLeaderboardAsOf($month, $year, $x)
	{	
		if($x == 'previous') {
			if($month == 1) {
				$month = 12;
				$year = $year - 1;
			}
			else {
				$month = $month - 1;
			}
		}
		$stmt = config::$mysqli->prepare("SELECT members.first_name, members.last_name, ut.user_id, ut.user_team_id, ut.user_team_name, ut.date, x.total, x.overall_rank
		FROM user_teams ut
		LEFT JOIN (
			SELECT overall.user_team_id, overall.total, (@row := @row + 1) AS overall_rank
			FROM (
				SELECT user_teams.user_team_id, COALESCE(SUM(CASE WHEN team_selections.transfer_in < fixtures.date AND team_selections.transfer_out > fixtures.date AND fixtures.date > ? AND ((MONTH(fixtures.date) <= ? AND YEAR(fixtures.date) = ?) OR (MONTH(fixtures.date) > ? AND YEAR(fixtures.date) < ?)) THEN team_points.points ELSE 0 END),0) as total
				FROM user_teams
				LEFT JOIN team_selections
				ON user_teams.user_team_id = team_selections.user_team_id
				LEFT JOIN team_points
				ON team_selections.team_id = team_points.team_id
				LEFT JOIN fixtures
				ON team_points.fixture_id = fixtures.fixture_id
				WHERE user_teams.date > ?
				GROUP BY user_teams.user_team_id
				ORDER BY total DESC, user_teams.user_team_name ASC
			) overall, (SELECT @row := 0) r
		) AS x
	
		ON x.user_team_id = ut.user_team_id
		LEFT JOIN members
		ON ut.user_id = members.user_id
		WHERE ut.date > ?
		ORDER BY overall_rank ASC, ut.user_team_name ASC
		LIMIT 10");
		$stmt->bind_param("sssssss", config::$curSeasonStart, $month, $year, $month, $year, config::$curSeasonStart, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($first_name, $last_name, $user_id, $user_team_id, $user_team_name, $date, $total, $overall_rank);
		
		echo '
		<div class="float-left">
		<h2> Leaderboard as of '.date("F", mktime(0, 0, 0, $month, 10)).' '.$year.'</h2>
		<table width="340" border="0" cellpadding="0" cellspacing="0">
			<tr class="table-header">
				<td>&nbsp;</td>
				<td>Manager</td>
				<td>Team Name</td>
				<td class="right">Pts</td>
			</tr>';
		$row_class = 0;
		$counter = 1;	
		while($stmt->fetch()) {	
			// store leaderboard 1st place team for email
			if($counter == 1 && $x == 'current') {
				$_SESSION['leaderboard_1_team'] = $user_team_name;
				$_SESSION['leaderboard_1_points'] = $total;
			}
			// store leaderboard 2nd place team for email
			if($counter == 2 && $x == 'current') {
				$_SESSION['leaderboard_2_team'] = $user_team_name;
				$_SESSION['leaderboard_2_points'] = $total;
			}
			// store leaderboard 3rd place team for email
			if($counter == 3 && $x == 'current') {
				$_SESSION['leaderboard_3_team'] = $user_team_name;
				$_SESSION['leaderboard_3_points'] = $total;
			}
			// display top 10 leaderboard
			echo '<tr class="table-row'.$row_class.'">
			<td>'.$counter.'</td>
			<td>'.$first_name.' '.$last_name.'</td>
			<td>'.showLink($user_team_name, $user_team_id, 'user_team').'</td>
			<td class="right">'.$total.'</td>
			</tr>';
			$row_class = 1 - $row_class;
			$counter++;
		}
		echo '
		</table>
		</div>';
		$stmt->close();
	}
	
	
	
	function showMonthTop10Transferred($month, $year, $transfer)
	{
		if($transfer == "in") {
			$query = 'user_teams.date <> team_selections.transfer_in
		AND MONTH(team_selections.transfer_in) = ?
		AND YEAR(team_selections.transfer_in) = ?';
		}
		else {
			$query = 'MONTH(team_selections.transfer_out) = ?
		AND YEAR(team_selections.transfer_out) = ?';
		}
		
		$stmt = config::$mysqli->prepare("SELECT COUNT(*) AS trans_in, teams.team_id, team_name
		FROM teams
		LEFT JOIN team_selections
		ON teams.team_id = team_selections.team_id
		LEFT JOIN user_teams
		ON team_selections.user_team_id = user_teams.user_team_id
		WHERE ".$query."
		GROUP BY teams.team_id
		ORDER BY trans_in DESC, team_name ASC
		LIMIT 10");
		$stmt->bind_param('ss', $month, $year);
		$stmt->execute();
		$stmt->store_result();
		
		echo '
		<div class="float-left">
		<h2>'.date("F", mktime(0, 0, 0, $month, 10)).' '.$year.' Top 10 Transferred '.$transfer.'</h2>
		<table width="340" border="0" cellpadding="0" cellspacing="0">
			<tr class="table-header">
				<td>&nbsp;</td>
				<td>Team Name</td>
				<td class="right">Total</td>
			</tr>';
			
		$row_class = 0;
		$counter = 1;	
		
		if ($stmt->num_rows == 0) {
			echo '<tr class="table-row'.$row_class.'">No transfers have been made during '.date("F", mktime(0, 0, 0, $month, 10)).'.</tr>';
		}
		else {
			$stmt->bind_result($trans, $team_id, $team_name);
		
			while($stmt->fetch()) {	
				// store team data for email message
				if($counter == 1 && $transfer == 'in') {
					$_SESSION['trasfer_in_team'] = $team_name;
					$_SESSION['trasfer_in_count'] = $trans;
				}
				if($counter == 1 && $transfer == 'out') {
					$_SESSION['trasfer_out_team'] = $team_name;
					$_SESSION['trasfer_out_count'] = $trans;
				}
				// display top 10 teams
				echo '<tr class="table-row'.$row_class.'">
				<td>'.$counter.'</td>
				<td>'.show_team_link($team_name, $team_id).'</td>
				<td class="right">'.$trans.'</td>
				</tr>';
				$row_class = 1 - $row_class;
				$counter++;
			}
		}
		echo '
		</table>
		</div>';
		$stmt->close();
	}
}
?>