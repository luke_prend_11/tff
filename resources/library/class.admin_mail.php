<?php
class admin_mail
{
	public function showEmailForm()
	{
		$uf = new user_functions();		
				
		echo '
		<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
			<div>'
				.$uf->showStickyForm('select','recipients',array('Test Members','Season Members','Season Members > 623','All Members','All Members > 441','Season Unactive Members','Season New Members - No Teams Submitted'))
				.$uf->showStickyForm('text','subject')
				.$uf->showStickyForm('textarea','message').'
				<input type="submit" name="submit" value="Send Email" class="button blue" />
				<input type="hidden" name="submitted" value="TRUE" />
			</div>
		</form>
		';
	}
}
?>