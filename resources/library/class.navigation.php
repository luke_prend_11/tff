<?php
class navigation
{	
	const LAST_ELEMENT_LIST_CLASS   = "align-right";
	const LAST_ELEMENT_ANCHOR_CLASS = "no-border";
	const ACTIVE_MENU_CLASS         = "active";
	const PLAY_NOW_LINK             = "<a href=\"%s%s\" title=\"%s\" class=\"blue\">%s</a>";
	
	private $currentMenuItem;
	private $currentSubMenuItem;
	private $currentSubSubMenuItem;
	private $lastMenuItem;
	private $navContent;
	
	public static $playNowUrl;
	public static $navMenu;
	public static $activeSubMenu;
	
	public function __construct()
	{
		$this->setNavContent();
		$this->buildNavMenu();
	}
	
	private function setNavContent()
	{
		$this->navContent = array(
			"home" => array(
				"pre_desc" => "TFF",
				"desc" 	   => "Home",
				"title"	   => config::SITE_NAME,
				"route"	   => "",
			),
                    
			"my_account" => array(
				"pre_desc" => "My",
				"desc" 	   => "Account",
				"title"	   => "My Account",
				"route"	   => "/my-account/",
				"children" => array(
					"teams_index" => array(
						"desc" 	=> "My Teams",
						"title"	=> "My Teams",
						"route" => "/my-account/teams/"
					),
					"submit_team" => array(
						"desc"  => "Submit a Team",
						"title" => "Submit a New Team",
						"route" => "/my-account/submit-team.php"
					),
					"leagues" => array(
						"desc"  => "My Leagues",
						"title" => "My Leagues",
						"route" => "/my-account/leagues/"
					),
					"profile" => array(
						"desc"  => "My Profile",
						"title" => "My Profile",
						"route" => "/my-account/profile.php"
					),
				),
			),
			"registration" => array(
				"pre_desc" => "Play",
				"desc" 	   => "Now",
				"title"	   => "Fantasy Football Registration",
				"route"	   => "/registration.php"
			),
			"leaderboards" => array(
				"pre_desc" => "TFF",
				"desc" 	   => "Leaderboards",
				"title"	   => "Leaderboards",
				"route"	   => "/leaderboards/index.php?ldb=season",
				"children" => array(
					"overall_leaderboard" => array(
						"desc" 	=> "Overall Leaderboard",
						"title"	=> "Overall Leaderboard",
						"route" => "/leaderboards/index.php?ldb=season"
					),
					"month_leaderboard" => array(
						"desc" 	=> "Month Leaderboard",
						"title"	=> "Month Leaderboard",
						"route" => "/leaderboards/index.php?ldb=month"
					),
					"week_leaderboard" => array(
						"desc" 	=> "Week Leaderboard",
						"title"	=> "Week Leaderboard",
						"route" => "/leaderboards/index.php?ldb=week"
					),
					/*"ultimate_leagues" => array(
						"desc"  => "Ultimate Leagues",
						"title" => "Ultimate Leagues",
						"route" => "/leaderboards/ultimate-leagues/"
					),*/
				),
			),
                      /*
			"news" => array(
				"pre_desc" => "Latest",
				"desc" 	   => "News",
				"title"	   => "Fantasy Football News",
				"route"	   => "/news/"
			),*/
			"stats" => array(
				"pre_desc" => "Team",
				"desc" 	   => "Stats",
				"title"	   => "Team Statistics",
				"route"	   => "/stats/",
				"children" => array(
					"premier-league" => array(
						"desc"  => "Premier League",
						"title" => "Premier League Table",
						"route" => "/stats/premier-league-C1",
						"children" => $this->getTeams(1, "premier-league"),
					),
					"championship" => array(
						"desc"  => "Championship",
						"title" => "Championship Table",
						"route" => "/stats/championship-C2",
						"children" => $this->getTeams(2, "championship"),
					),
					"league-one" => array(
						"desc"  => "League One",
						"title" => "League One Table",
						"route" => "/stats/league-one-C3",
						"children" => $this->getTeams(3, "league-one"),
					),
					"league-two" => array(
						"desc"  => "League Two",
						"title" => "League Two Table",
						"route" => "/stats/league-two-C4",
						"children" => $this->getTeams(4, "league-two"),
					),
				),
			),
			"help" => array(
				"pre_desc" => "Contact",
                                "desc"     => "TFF",
                                "title"    => "Contact Us",
                                "route"    => "/help/contact-us.php"
			),
                    /*
			"help" => array(
				"pre_desc" => "Need",
				"desc" 	   => "Help?",
				"title"	   => "Help",
				"route"	   => "/help/",
				"children" => array(
					"rules" => array(
						"desc"  => "How to Play",
						"title" => "How to Play",
						"route" => "/help/rules.php"
					),
					"faqs" => array(
						"desc"  => "FAQ's",
						"title" => "Frequently Asked Questions",
						"route" => "/help/faqs.php"
					),
					"prizes" => array(
						"desc"  => "Prizes",
						"title" => "Prizes",
						"route" => "/help/prizes.php"
					),
					"contact_us" => array(
						"desc"  => "Contact Us",
						"title" => "Contact Us",
						"route" => "/help/contact-us.php"
					),
					"about_us" => array(
						"desc"  => "About Us",
						"title" => "About Us",
						"route" => "/help/about-us.php"
					),
				),
			),
                     * *
                     */
		);
	}
	
	private function buildMainHeading()
	{
		if($this->isActiveMenuItem())
		{
			$elementListClass   = ' class="'.self::ACTIVE_MENU_CLASS.'"';
			$elementAnchorClass = '';
		}
		elseif($this->lastMenuItem == TRUE)
		{
			$elementListClass   = ' class="'.self::LAST_ELEMENT_LIST_CLASS.'"';
			$elementAnchorClass = ' class="'.self::LAST_ELEMENT_ANCHOR_CLASS.'"';
		}
		else
		{
			$elementListClass   = '';
			$elementAnchorClass = '';
		}
		
		return '
		<li'.$elementListClass.'>
			<a href="'.config::$baseUrl.$this->navContent[$this->currentMenuItem]["route"].'" title="'.$this->navContent[$this->currentMenuItem]["title"].'"'.$elementAnchorClass.'>'
				.$this->navContent[$this->currentMenuItem]["pre_desc"]
				.'<br />
				<span>'.$this->navContent[$this->currentMenuItem]["desc"].'</span>
			</a>
		';
	}
	
	private function buildSubHeading($active = FALSE, $lastArrayKey = FALSE, $subMenuItem = FALSE)
	{
		$string = $active == TRUE 
			    ? '
				<li class="stretch-child">' 
				: '<li>';
				
		$string .= '
			<a href="'.config::$baseUrl.$this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["route"].'" 
			   title="'.$this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["title"].'">'.
			   $this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["desc"].'
			</a>';
			
			
		if($this->childHasChildren() && $active == FALSE)
		{
			$string .= '<ul>';
			foreach($this->navContent[$this->currentMenuItem]['children'][$this->currentSubMenuItem]['children'] as $subSubMenuItem => $value)
			{
				$this->currentSubSubMenuItem = $subSubMenuItem;
				$string .= $this->buildSubSubHeading();
			}
			$string .= '</ul>';
		}
		$string .= '</li>';
		
		if($active == TRUE)
		{
			// check if menu item is last element to apply correct style class
			$string .= $subMenuItem == $lastArrayKey && $subMenuItem != FALSE && $lastArrayKey != FALSE ? '
			<li class="stretch-child blank"></li>' : '
			<li class="stretch-child border"></li>';
		}
		
		return $string;
	}
	
	private function buildSubSubHeading()
	{	
		return '
		<li>
			<a href="'.config::$baseUrl.$this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["children"][$this->currentSubSubMenuItem]["route"].'" 
			   title="'.$this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["children"][$this->currentSubSubMenuItem]["title"].'" 
                           style="background-image:url('.config::$baseUrl.'/img/content/team-badges/small/'.$this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["children"][$this->currentSubSubMenuItem]["team_id"].'.png); background-size: 18px 18px; background-repeat:no-repeat; background-position:4px center;">'.
			   $this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["children"][$this->currentSubSubMenuItem]["desc"].'
			</a>
		</li>';
	}
	
	private function hasChildren()
	{
		if (array_key_exists("children", $this->navContent[$this->currentMenuItem]) && is_array($this->navContent[$this->currentMenuItem]["children"])) {
			foreach ($this->navContent[$this->currentMenuItem]["children"] as $subMenuItem => $value ) {
				return true;
			}
		}
	}
	
	private function childHasChildren()
	{
		if (array_key_exists("children", $this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]) && is_array($this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem])) {
			foreach ($this->navContent[$this->currentMenuItem]["children"][$this->currentSubMenuItem]["children"] as $subMenuItem => $value ) {
				return true;
			}
		}
	}
	
	private function buildNavMenu()
	{
		// instantiate login class and check logged in status to choose menu item to unset
		$lg = new login();
		if($lg->isLoggedIn())
		{
			unset($this->navContent["registration"]);
			self::$playNowUrl = sprintf(self::PLAY_NOW_LINK, config::$baseUrl, "/my-account/", "My Account", "My Account");
		}
		else
		{
			unset($this->navContent["my_account"]);
			self::$playNowUrl = sprintf(self::PLAY_NOW_LINK, config::$baseUrl, "/registration.php", "Sign Up Now", "Play Now!");
		}
		   
		$lastArrayKey = $this->getlastMenuItem($this->navContent); // fetch last array key
		
		self::$navMenu .= '<nav class="nav-collapse">';
		self::$navMenu .= '<ul>';
			
		foreach($this->navContent as $menuItem => $value)
		{				
			// check if menu item is last element to apply style classes
			if($menuItem == $lastArrayKey)
			{
				$this->lastMenuItem = TRUE;
			}
			
			$this->currentMenuItem = $menuItem;
			self::$navMenu        .= $this->buildMainHeading();
			
			if($this->hasChildren() && $this->isActiveMenuItem())
			{
				$this->buildActiveSubMenu();
				$this->buildSubMenu($hidden = TRUE);
			}
			elseif($this->hasChildren())
			{
				$this->buildSubMenu();
				
			}
			
			self::$navMenu .= "</li>";
		}
		
		self::$navMenu .= '</ul>';
		self::$navMenu .= '</nav>';
	}
	
	private function buildSubMenu($hidden = FALSE)
	{
		self::$navMenu .= $hidden == TRUE ? '<ul class="hidden">' : '<ul>';
		$this->loopSubMenuItems();
		
		self::$navMenu .= '</ul>';
	}
	
	private function buildActiveSubMenu()
	{
		self::$activeSubMenu .= '<div id="active-menu">';
		self::$activeSubMenu .= '<div class="content">';
		self::$activeSubMenu .= '<ul class="stretch-parent">';
		self::$activeSubMenu .= '<li class="stretch-child blank"></li>';
		
		$lastArrayKey = $this->getlastMenuItem($this->navContent[$this->currentMenuItem]["children"]); // fetch last array key
			
		$this->loopSubMenuItems($active = TRUE, $lastArrayKey);
		self::$activeSubMenu .= '<span class="stretch"></span>';
		self::$activeSubMenu .= '</ul>';
		self::$activeSubMenu .= '</div>';
		self::$activeSubMenu .= '</div>';
	}
	
	private function loopSubMenuItems($active = FALSE, $lastArrayKey = FALSE)
	{
		foreach($this->navContent[$this->currentMenuItem]['children'] as $subMenuItem => $value)
		{
			$this->currentSubMenuItem = $subMenuItem;
			if($active == FALSE)
			{
				self::$navMenu .= $this->buildSubHeading();
			}
			else
			{
				self::$activeSubMenu .= $this->buildSubHeading($active = TRUE, $lastArrayKey, $subMenuItem);
			}
		}
	}
	
	private function isActiveMenuItem()
	{
		if (strpos(config::$currentPageRoute, $this->currentMenuItem) !== false) {
			return true;
		}
	}
	
	private function getlastMenuItem($menuElement)
	{
		$arrayKeys = array_keys($menuElement); // get array keys for use with last element
		return array_pop($arrayKeys);
	}
	
	private function getTeams($leagueId, $leagueRoute)
	{
		$ug = new url_generator();		
		
		$stmt = config::$mysqli->prepare("
		SELECT tn.team_id, tn.team_name
		FROM team_names tn
		LEFT JOIN tff_teams tt
		ON tn.team_id = tt.team_id
		WHERE tt.league_id = ?
		ORDER BY tn.team_name ASC");
		$stmt->bind_param("i", $leagueId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($teamId, $teamName);
		
		if($stmt->num_rows() != 0)
		{
			while($stmt->fetch())
			{
				$ug->linkString = $teamName;
				$ug->prepareUrlText();
				$ug->makeUrl($teamName, $teamId, 'team');
				
				$arrayString[$ug->urlString] = array(
						"desc"    => "$teamName",
						"team_id" => $teamId,
						"title"   => "$teamName Statistics",
						"route"   => "/$ug->urlFolder/$ug->urlString-$ug->urlParameter$teamId"
				);
			}
                        
                        $stmt->close();

                        return $arrayString;
		}
	}
}
