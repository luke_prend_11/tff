<?php
class meta_content
{	
	const DEFAULT_META_KEYWORDS    = "ff %s, f, ff, ff competition, fs";
	const DEFAULT_META_DESCRIPTION = "";
	const STYLESHEET_LINK          = '<link rel="stylesheet" type="text/css" href="%s/css/%s" />';
	const FANTASY_FOOTBALL_TEXT    = "Fantasy Football"; // Needs to be fantasy football when live
	
	public $pageTitle;
	public $metaDescription;
	public $metaKeywords;
	public $pageH1;
	public $pageH2;
	public $css;
	
	private $metaContent = array();
	
	public function __construct()
	{		
		$this->setMetaContent();
		$this->setPageTitle();
		$this->setMetaDescription();
		$this->setMetaKeywords();
		$this->setPageH1();
		$this->setPageH2();
		$this->setCss();
	}
	
	public function setMetaContent()
	{
		$this->metaContent = array(
			"index" => array(
				"page_title"       => "Free ".self::FANTASY_FOOTBALL_TEXT." ".config::$curSeasonName,
				"meta_description" => "Sign up to a free ".self::FANTASY_FOOTBALL_TEXT." game unlike any other for the ".config::$curSeasonName." season at ".config::SITE_NAME,
				"css"              => array("index.css", "forms.css")
			),
			"error" => array(
				"page_title"       => "Page Not Found",
				"meta_description" => "The page you have requested has not been found.",
				"h1"               => "Page Not Found",
			),
			"login" => array(
				"page_title"       => "Login",
				"meta_description" => "Log in to ".config::SITE_NAME." to get full access to your account.",
				"h1"               => "Login",
				"css"              => "forms.css"
			),
			"lost_password" => array(
				"page_title"       => "Password Reset",
				"meta_description" => "Forgotten your password? Reset it here.",
				"h1"               => "Reset Password",
				"css"              => "forms.css"
			),
			"registration" => array(
				"page_title"       => "".self::FANTASY_FOOTBALL_TEXT." Registration",
				"meta_description" => "Register for free ".self::FANTASY_FOOTBALL_TEXT." ".config::$curSeasonName." at ".config::SITE_NAME,
				"meta_keywords"    => "",
				"h1"               => "Register Now",
				"css"              => "forms.css"
			),
			"site_map" => array(
				"page_title"       => "Site Map",
				"meta_description" => "Links to all pages on ".config::SITE_NAME,
				"meta_keywords"    => "",
				"h1"               => "Site Map",
				"css"              => ""
			),
			"help_about_us" => array(
				"page_title"       => "About Us",
				"meta_description" => "Find out more about the history of ".config::SITE_NAME." and what it's all about.",
				"meta_keywords"    => "",
				"h1"               => "About Us",
				"css"              => ""
			),
			"help_contact_us" => array(
				"page_title"       => "Contact Us",
				"meta_description" => "Contact ".config::SITE_NAME." for any queries regarding this website or ".self::FANTASY_FOOTBALL_TEXT." in general.",
				"meta_keywords"    => "",
				"h1"               => "Contact Us",
				"css"              => "forms.css"
			),
			"help_faqs" => array(
				"page_title"       => "Frequently Asked Question's",
				"meta_description" => "Find the answer to a range of popular questions asked by members of ".config::SITE_NAME,
				"meta_keywords"    => "",
				"h1"               => "FAQ's",
				"css"              => ""
			),
			"help_index" => array(
				"page_title"       => "Help",
				"meta_description" => "Find the help you need regarding the use of ".config::SITE_NAME." website.",
				"meta_keywords"    => "",
				"h1"               => "Help",
				"css"              => ""
			),
			"help_privacy" => array(
				"page_title"       => "Privacy Policy",
				"meta_description" => "Find out more about the history of ".config::SITE_NAME." and what it\'s all about.",
				"meta_keywords"    => "",
				"h1"               => "Privacy Policy",
				"css"              => ""
			),
			"help_prizes" => array(
				"page_title"       => "".self::FANTASY_FOOTBALL_TEXT." Prizes",
				"meta_description" => "Prizes on offer for members competing in free ".self::FANTASY_FOOTBALL_TEXT." at ".config::SITE_NAME,
				"meta_keywords"    => "",
				"h1"               => "Prizes",
				"css"              => ""
			),
			"help_rules" => array(
				"page_title"       => "How To Play",
				"meta_description" => "Step by step guide detailing how to register, submit a team and score points.",
				"meta_keywords"    => "",
				"h1"               => "How To Play",
				"css"              => ""
			),
			"my_account_following" => array(
				"page_title"       => "Following",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Following",
				"css"              => "account.css"
			),
			"my_account_history" => array(
				"page_title"       => "My History",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "History",
				"css"              => "account.css"
			),
			"my_account_index" => array(
				"page_title"       => "My Account",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Overview",
				"css"              => "account.css"
			),
			"my_account_leagues_index" => array(
				"page_title"       => "My Leagues",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Leagues",
				"css"              => array("account.css","forms.css")
			),
			"my_account_leagues_leagues" => array(
				"page_title"       => url_utils::$breadcrumbLastElement,
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement,
				"css"              => "account.css"
			),
			"my_account_profile" => array(
				"page_title"       => "My Profile",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Profile",
				"css"              => array("account.css", "forms.css")
			),
			"my_account_submit_team" => array(
				"page_title"       => "Submit a Team",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Submit a New Team",
				"css"              => array("account.css", "forms.css")
			),
			"my_account_teams_index" => array(
				"page_title"       => "Teams",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Teams",
				"css"              => "account.css"
			),
			"my_account_teams_history" => array(
				"page_title"       => "Team History",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => "Team History",
				"css"              => "account.css"
			),
			"my_account_teams_matches" => array(
				"page_title"       => url_utils::$breadcrumbSecondToLastElement." Matches",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbSecondToLastElement." Matches",
				"css"              => "account.css"
			),
			"my_account_teams_stats" => array(
				"page_title"       => url_utils::$breadcrumbSecondToLastElement." Stats",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbSecondToLastElement." Stats",
				"css"              => "account.css"
			),
			"my_account_teams_teams" => array(
				"page_title"       => url_utils::$breadcrumbLastElement,
				"meta_description" => "Check your teams progress and make transfers as necessary",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement,
				"css"              => array("forms.css", "account.css")
			),
			"my_account_teams_transfers" => array(
				"page_title"       => url_utils::$breadcrumbSecondToLastElement." Transfers",
				"meta_description" => "",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbSecondToLastElement." Transfers",
				"css"              => array("forms.css", "account.css")
			),
			"news_index" => array(
				"page_title"       => "All News",
				"meta_description" => "Browse the latest ".self::FANTASY_FOOTBALL_TEXT." news and most recent updates from ".config::SITE_NAME,
				"meta_keywords"    => "",
				"h1"               => "News",
				"css"              => "news-home.css"
			),
			"news_news" => array(
				"page_title"       => url_utils::$breadcrumbLastElement,
				"meta_description" => "Read more on the article ".url_utils::$breadcrumbLastElement,
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement,
				"css"              => "news-articles.css"
			),
			"stats_fixtures" => array(
				"page_title"       => "Football Fixtures",
				"meta_description" => "Upcoming football fixtures for all leagues and competitions on ".config::SITE_NAME.", including the Premier League, Championship, FA Cup and Champions League",
				"meta_keywords"    => "",
				"h1"               => "Fixtures",
				"css"              => ""
			),
			"stats_index" => array(
				"page_title"       => "".self::FANTASY_FOOTBALL_TEXT." Statistics",
				"meta_description" => "".self::FANTASY_FOOTBALL_TEXT." ".config::$curSeasonName." staistics from ".config::SITE_NAME,
				"meta_keywords"    => "",
				"h1"               => "Stats",
				"css"              => ""
			),
			"leaderboards_index" => array(
				"page_title"       => "Leaderboards",
				"meta_description" => "Check out your ".self::FANTASY_FOOTBALL_TEXT." teams overall position on ".config::SITE_NAME." leaderboards.",
				"meta_keywords"    => "",
				"h1"               => "Leaderboards",
				"css"              => "forms.css"
			),
			"leaderboards_private_leagues" => array(
				"page_title"       => url_utils::$breadcrumbLastElement." Leaderboard",
				"meta_description" => "Check out your teams position in the ".url_utils::$breadcrumbLastElement." leaderboard.",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement." Leaderboard",
				"css"              => "forms.css"
			),
			"leaderboards_ultimate_leagues" => array(
				"page_title"       => url_utils::$breadcrumbLastElement." Ultimate League",
				"meta_description" => "Check out your teams position in the ".url_utils::$breadcrumbLastElement." Ultimate League leaderboard.",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement." Ultimate League",
				"css"              => ""
			),
			"stats_competitions" => array(
				"page_title"       => url_utils::$breadcrumbLastElement." Stats",
				"meta_description" => url_utils::$breadcrumbLastElement." fixtures, results and competition standings.",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement,
				"css"              => "forms.css"
			),
			"stats_matches" => array(
				"page_title"       => url_utils::$breadcrumbLastElement." - ".url_utils::$breadcrumbDateElement,
				"meta_description" => url_utils::$breadcrumbLastElement." - ".url_utils::$breadcrumbDateElement." match stats, form guide and betting tips.",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement,
				"h2"               => url_utils::$breadcrumbDateElement,
				"css"              => "team-data.css"
			),
			"stats_matches_index" => array(
				"page_title"       => "Matches",
				"meta_description" => "Recent football match results, stats, form guide and betting tips.",
				"meta_keywords"    => "",
				"h1"               => "Matches",
				"css"              => array("team-data.css", "forms.css")
			),
			"stats_results" => array(
				"page_title"       => "Football Results",
				"meta_description" => "Latest football results for all leagues and competitions on ".config::SITE_NAME.", including the Premier League, Championship, FA Cup and Champions League.",
				"meta_keywords"    => "",
				"h1"               => "Results",
				"css"              => ""
			),
			"stats_teams" => array(
				"page_title"       => url_utils::$breadcrumbLastElement." Stats",
				"meta_description" => config::$curSeasonName." ".self::FANTASY_FOOTBALL_TEXT." stats for ".url_utils::$breadcrumbLastElement.".",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement." Stats",
				"css"              => "team-data.css"
			),
			"stats_user_teams" => array(
				"page_title"       => url_utils::$breadcrumbLastElement." ".self::FANTASY_FOOTBALL_TEXT." Stats",
				"meta_description" => config::$curSeasonName." ".self::FANTASY_FOOTBALL_TEXT." stats for ".url_utils::$breadcrumbLastElement.".",
				"meta_keywords"    => "",
				"h1"               => url_utils::$breadcrumbLastElement." Stats",
				"css"              => "team-data.css"
			),
			"admin_administrators" => array(
				"page_title"       => config::SITE_NAME." Administrators",
				"meta_description" => config::SITE_NAME." Administrators",
				"h1"               => "Administrators",
			),
			"admin_email" => array(
				"page_title"       => config::SITE_NAME." Email Members",
				"meta_description" => config::SITE_NAME." Email Members",
				"h1"               => "Email Members",
			),
			"admin_fixtures" => array(
				"page_title"       => config::SITE_NAME." Fixtures",
				"meta_description" => config::SITE_NAME." Fixtures",
				"h1"               => "Fixtures",
			),
			"admin_index" => array(
				"page_title"       => config::SITE_NAME." Administration",
				"meta_description" => config::SITE_NAME." Administration",
				"h1"               => "TFF Administration",
			),
			"admin_leaderboards" => array(
				"page_title"       => config::SITE_NAME." Leaderboards",
				"meta_description" => config::SITE_NAME." Leaderboards",
				"h1"               => "Leaderboards",
			),
			"admin_leagues" => array(
				"page_title"       => config::SITE_NAME." Private Leagues",
				"meta_description" => config::SITE_NAME." Private Leagues",
				"h1"               => "Private Leagues",
			),
			"admin_login" => array(
				"page_title"       => config::SITE_NAME." Login",
				"meta_description" => config::SITE_NAME." Login",
				"h1"               => "Login",
			),
			"admin_member_update" => array(
				"page_title"       => config::SITE_NAME." Member Update",
				"meta_description" => config::SITE_NAME." Member Update",
				"h1"               => "Member Update",
			),
			"admin_news_add_news" => array(
				"page_title"       => config::SITE_NAME." Add News",
				"meta_description" => config::SITE_NAME." Add News",
				"h1"               => "News Manager",
			),
			"admin_news_index" => array(
				"page_title"       => config::SITE_NAME." News Manager",
				"meta_description" => config::SITE_NAME." News Manager",
				"h1"               => "News Manager",
			),
			"admin_news_manage_comments" => array(
				"page_title"       => config::SITE_NAME." News Comments",
				"meta_description" => config::SITE_NAME." News Comments",
				"h1"               => "News Manager",
			),
			"admin_news_manage_delete" => array(
				"page_title"       => config::SITE_NAME." Delete News",
				"meta_description" => config::SITE_NAME." Delete News",
				"h1"               => "News Manager",
			),
			"admin_news_manage_edit" => array(
				"page_title"       => config::SITE_NAME." Edit News",
				"meta_description" => config::SITE_NAME." Edit News",
				"h1"               => "News Manager",
			),
			"admin_news_manage_index" => array(
				"page_title"       => config::SITE_NAME." Manage News",
				"meta_description" => config::SITE_NAME." Manage News",
				"h1"               => "News Manager",
			),
			"admin_results" => array(
				"page_title"       => config::SITE_NAME." Results",
				"meta_description" => config::SITE_NAME." Results",
				"h1"               => "Results",
			),
			"admin_teams" => array(
				"page_title"       => config::SITE_NAME." Teams",
				"meta_description" => config::SITE_NAME." Teams",
				"h1"               => "Teams",
			),
		);
	}
	
	private function setPageTitle()
	{
		$this->pageTitle = (isset($this->metaContent[config::$currentPageRoute]["page_title"]) && $this->metaContent[config::$currentPageRoute]["page_title"] != ""
					     ? $this->metaContent[config::$currentPageRoute]["page_title"].config::PAGE_TITLE_SEPARATOR.config::SITE_NAME
					     : config::SITE_NAME);
	}
	
	private function setMetaDescription()
	{
		$this->metaDescription = (isset($this->metaContent[config::$currentPageRoute]["meta_description"]) && $this->metaContent[config::$currentPageRoute]["meta_description"] != ""
		                       ? $this->metaContent[config::$currentPageRoute]["meta_description"] 
							   : self::DEFAULT_META_DESCRIPTION);
	}
	
	private function setMetaKeywords()
	{
		$this->metaKeywords = (isset($this->metaContent[config::$currentPageRoute]["meta_keywords"]) && $this->metaContent[config::$currentPageRoute]["meta_keywords"] != ""
		                    ? $this->metaContent[config::$currentPageRoute]["meta_keywords"] 
							: sprintf(self::DEFAULT_META_KEYWORDS, config::$curSeasonName));
	}
	
	private function setPageH1()
	{
		$this->pageH1 = isset($this->metaContent[config_base::$currentPageRoute]["h1"]) ? $this->metaContent[config_base::$currentPageRoute]["h1"] : "My Account";
	}
	
	private function setPageH2()
	{
		if(isset($this->metaContent[config::$currentPageRoute]["h2"])) {
			$this->pageH2 = $this->metaContent[config::$currentPageRoute]["h2"];
		}
	}
	
	private function setCss()
	{
		if(isset($this->metaContent[config::$currentPageRoute]["css"])) {
			$this->buildCss($this->metaContent[config::$currentPageRoute]["css"]);
		}
	}
	
	private function createCssLink($css)
	{
		return sprintf(self::STYLESHEET_LINK, config::$baseUrl, $css);
	}
	
	private function buildCss($css)
	{
		if(is_array($css)) {
			$this->css = "";
			foreach ($css as $stylesheet) {
				$this->css .= $this->createCssLink($stylesheet);
				$this->css .= "\n";
			}
		}
		else {
			$this->css = $this->createCssLink($css);
			$this->css .= "\n";
		}
	}
}
?>