<?php
class login
{
	const DESTINATION_URL_TEMPLATE = "http://%s";
	
	public static $seed = "0dAfghRqSTgx";
	
	private $userId;
	
	public function __construct()
	{
		if (isset($_COOKIE['user_id']))
		{
			$this->userId = $_COOKIE['user_id'];
		}
	}
	
	private function setDestinationUrl()
	{
		$url        = sprintf(self::DESTINATION_URL_TEMPLATE, "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
		$escapedUrl = htmlspecialchars($url, ENT_QUOTES, 'UTF-8');
		
		setcookie('destinationUrl', $escapedUrl, time()+(60*60*24*30), '/');
	}
	
	public function isLoggedIn()
	{
		if (isset($_COOKIE['user_id']) && isset($_COOKIE['email']))
		{
        	return true; // the user is logged in
		}
		else
		{
			return false; // the user is not logged in
		}
	}
	
	public function notLoggedInRedirect()
	{
		if (!$this->isLoggedIn())
		{
			$_SESSION['not_logged_in'] = true;
			
			$this->setDestinationUrl();
			
			header ("Location: ".config::$baseUrl."/login.php");
			exit();
		}	
	}
	
	public function showLoginForm()
	{
		echo '<form name="login" method="post" action="'.htmlentities($_SERVER['PHP_SELF']).'">
			<input tabindex="1" name="email_address" onclick="this.value=\'\';" onfocus="this.select()" onblur="this.value=!this.value?\'Email address\':this.value;" value="Email Address" type="text" maxlength="150" id="email" class="text" />
			<input tabindex="2" name="password" onclick="this.value=\'\';" onfocus="this.select()" onblur="this.value=!this.value?\'Password\':this.value;" value="Password" type="password" maxlength="15" id="password" class="text" />
			<div class="button float-left">
				<input tabindex="3" type="submit" value="Login" name="login" class="blue" />
			</div>
			<div class="button float-right">
				<a href="'.config::$baseUrl.'/registration.php" title="Sign Up Now" class="dark-grey">Sign Up</a>
			</div>
			<div class="button float-right">
				<a href="'.config::$baseUrl.'/lost-password.php" title="Problems Signing In" class="orange">Can\'t Login?</a>
			</div>
			<br class="clearfloat" />
		</form>';
	}
	
	public function notLoggedInForm()
	{
		echo '<form method="post" action="'.htmlentities($_SERVER['PHP_SELF']).'" class="form">
			<h2>Account Details</h2>
			<div class="very-light-grey">
				<div>
					<label title="Email Address">Email Address</label>
					<input tabindex="4" name="email_address" onclick="this.value=\'\';" onfocus="this.select()" onblur="this.value=!this.value?\'Enter email address\':this.value;" value="Enter email address" type="text" maxlength="150" id="email" />
				</div>
				<div>
					<label title="Password">Password</label>
					<input tabindex="5" name="password" onclick="this.value=\'\';" onfocus="this.select()" onblur="this.value=!this.value?\'Enter password\':this.value;" value="Enter password" type="password" maxlength="15" id="password" />
				</div>
			</div>
			<input tabindex="6" type="submit" value="Login" class="button" name="login_new" />
		</form>';
	}
	
	public function notLoggedInMsg()
	{
		echo "<h3>Forgotten your password?</h3>";
		echo "<p>If you have can't remember your password, <a href='lost-password.php' title='Reset Password'>click here</a> to have your password reset.</p>";
	}
	
	public function showNotLoggedIn()
	{
		$this->notLoggedInForm();
		$this->notLoggedInMsg();
	}	
	
	public function showUserBox()
	{
		echo '<p class="float-left">You are signed in as: <strong>'.$_COOKIE['email'].'</strong> <span>| '.$this->getLastLogin().'</span></p>
		<div class="button float-right">
			<a href="'.config::$baseUrl.'/logout.php" title="Sign Out" class="orange">Sign Out</a>
		</div>
		<div class="button float-right">
			<a href="'.config::$baseUrl.'/my-account/" title="My Account" class="dark-grey">My Account</a>
		</div>
		<br class="clearfloat" />';
	}
	
	public function getLastLogin()
	{
		$stmt = config::$mysqli->prepare("SELECT date 
		FROM user_login 
		WHERE user_id = ?
		ORDER BY date DESC
		LIMIT 1,1");
		$stmt->bind_param("i", $this->userId);
		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows != 0) {
			$stmt->bind_result($date);
			$stmt->fetch();
			$stmt->close();
			return 'Last login: '.date("j M y", strtotime($date)).' at '.date("H:i", strtotime($date));
		}
	}
	
	public function showLoginStatus()
	{
		if ($this->isLoggedIn()) {
			// user is logged in
			$this->showUserBox();
		}
		else {
			// user is not logged in
			$this->showLoginForm();
		}
	}
	
	public function checkLogin($e, $p)
	{
		if (!$this->valid_email($e) || !$this->valid_password($p) || !$this->userExists($e)) {
			// the email or password were not valid, or the user did not exist
			$this->loginFailed($e);
		}
	 
		// look for the user in the database
		$seeded_password = sha1($p.self::$seed);
		$stmt = config::$mysqli->prepare("SELECT user_id 
		FROM members 
		WHERE email = ?
		AND password = ?
		LIMIT 1");
		$stmt->bind_param("ss", $e, $seeded_password);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($user_id);
		$stmt->fetch();
		// If the database returns a 1 as result we know the login details were correct and we proceed
		if ($stmt->num_rows != 1) {
			$this->loginFailed($e);
		}
		else {
			// Login was successful
			// Save the user ID and email for use later
			$this->setUserCookie($user_id, $e);
			
			// Insert data into user_login table to keep for records
			$this->insertLogin($user_id);
			
			if(isset($_COOKIE['destinationUrl'])) {
				$destinationUrl = $_COOKIE['destinationUrl'];
				
				header('Location: '.$destinationUrl);
				
				// set cookie to a time in the past to cause it to expire
				setcookie('destinationUrl', '', 1, '/');
				exit();
			}
			else {
				header('Location: '.config::$baseUrl.'/my-account/');
				exit();
			}
		}
		$this->loginFailed($e);
	}
	
	private function loginFailed($email)
	{
		$_SESSION['email'] = $email;
		header('Location: '.config::$baseUrl.'/login.php');
		exit();
	}
	
	public function setUserCookie($user_id, $e)
	{
		// set user id and email cookies for a period of 30 days
		setcookie('user_id', $user_id, time()+(60*60*24*30), '/');
		setcookie('email', $e, time()+(60*60*24*30), '/');
	}
	
	private function valid_email($e)
	{
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $e)) {
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $e);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if (!preg_match("/^(([A-Za-z0-9!#$%&#038;'*+=?^_`{|}~-][A-Za-z0-9!#$%&#038;'*+=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/",
				$local_array[$i])) {
				return false;
			}
		}
		if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
					return false;
				}
			}
		}
		return true;
	}
	
	private function valid_password($pass, $minlength=6, $maxlength=15)
	{
		$pass = trim($pass);
	 
		if (empty($pass)) {
			return false;
		}
		if (strlen($pass) < $minlength) {
			return false;
		}
		if (strlen($pass) > $maxlength) {
			return false;
		}
		$result = preg_match("/^[A-Za-z0-9_\-]+$/", $pass);
	 
		if ($result) {
			return true;
		}
		else {
			return false;
		}
		return false;
	}
	
	private function userExists($email)
	{
		if (!$this->valid_email($email)) {
			return false;
		}
	 
		$stmt = config::$mysqli->prepare("SELECT user_id FROM members WHERE email = ? LIMIT 1");
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
		
		if ($stmt->num_rows > 0) {
			return true;
		}
		else {
			return false;
		}
		$stmt->close();
		return false;
	}
	
	public function insertLogin($user_id)
	{
		$db = new database();
		$db->completeInsert('user_login', array('user_id', 'date'), array($user_id, config::$curDate), array('i', 's'));
		return true;
	}
}
?>