<?php
class user_functions
{
	public function valid_email($email)
	{
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if (!preg_match("/^(([A-Za-z0-9!#$%&#038;'*+=?^_`{|}~-][A-Za-z0-9!#$%&#038;'*+=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/",
				$local_array[$i])) {
				return false;
			}
		}
		if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
					return false;
				}
			}
		}
		return true;
	}
	
	public function userExists($email)
	{
		if (!$this->valid_email($email)) {
			return false;
		}
	 
		$stmt = config::$mysqli->prepare("SELECT user_id FROM members WHERE email = ? LIMIT 1");
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
		
		if ($stmt->num_rows > 0) {
			return true;
		}
		else {
			return false;
		}
		$stmt->close();
		return false;
	}
	
	public function valid_password($pass, $minlength=6, $maxlength=15)
	{
            $pass = trim($pass);

            if(empty($pass))
            {
                return false;
            }
            if(strlen($pass) < $minlength)
            {
                return false;
            }
            if(strlen($pass) > $maxlength)
            {
                return false;
            }
            $result = preg_match("/^[A-Za-z0-9_\-]+$/", $pass);

            if($result)
            {
                return true;
            }
            return false;
	}
	
	public function showStickyForm($type, $name, $values = '', $autocomplete = false, $label = '', $option_values = '', $showLabel = TRUE)
	{
            $string = '<div>';
            if($showLabel === TRUE)
            {
                $string .= '<label for="'.$name.'">';
                $string .= $label <> '' ? $label : ucwords(str_replace("_", " ", $name));
                $string .= '</label>';
            }
            $string .= $this->stickyFormField($type, $name, $values, $autocomplete, $option_values);
            $string .= '</div>';
            return $string;
	}
        
        public function stickyFormField($fieldType, $fieldName, $fieldValues = '', $fieldAutoComplete = false, $fieldOptionValues = '', $fieldSize = '')
        {
            $fieldAutoCompleteString = $fieldAutoComplete == TRUE ? ' autocomplete="off"' : '';
            $fieldNameString         = ' name="'.$fieldName.'"';
            $fieldSizeString         = $fieldSize != '' ? ' size="'.$fieldSize.'"' : '';
            $string                  = $fieldType == 'text' || $fieldType == 'password' || $fieldType == 'checkbox' 
                                     ? '<input type="'.$fieldType.'"'.$fieldNameString.$fieldSizeString.' value="' 
                                     : '<'.$fieldType.$fieldNameString.'>';
            
            switch($fieldType)
            {
                case 'text':
                    if(isset($_POST[$fieldName]))
                    {
                        $string .= $_POST[$fieldName];
                    }
                    elseif($fieldName == 'referred_by' && isset($_SESSION['referral']))
                    {
                        $string .= htmlspecialchars($_SESSION['referral'], ENT_QUOTES);
                    }
                    $string .= '"'.$fieldAutoCompleteString.'>';
                    break;
                    
                case 'password':
                    $string .= isset($_POST[$fieldName]) ? $_POST[$fieldName] : '';
                    $string .= '"'.$fieldAutoCompleteString.' maxlength="15">';
                    break;
                    
                case 'select':                    
                    $fieldValues = isset($fieldOptionValues) && !empty($fieldOptionValues) ? array_combine($fieldOptionValues, $fieldValues) : array_combine($fieldValues, $fieldValues);

                    foreach($fieldValues as $fieldOptionValue => $fieldValue)
                    {
                        $string .= '<option value="'.$fieldOptionValue.'"';
                        $string .= isset($_POST[$fieldName]) && ($_POST[$fieldName] == $fieldValue || $_POST[$fieldName] == $fieldOptionValue) ? ' selected="selected"' : '';
                        $string .= '>'.$fieldValue.'</option>';
                    }
                    $string .= '</select>';
                    break;
                
                case 'textarea':
                    $string .= isset($_POST[$fieldName]) ? htmlentities($_POST[$fieldName], ENT_QUOTES) : '';
                    $string .= '</textarea>';
                    break;
                
                case 'checkbox':
                    $string .= $fieldValues.'"';
                    $string .= isset($_POST[$fieldName]) && $_POST[$fieldName] == $fieldValues ? ' checked="checked"' : ''; 
                    $string .= '>';
                    break;
            }
            return $string;
        }
	
	public function lostPasswordForm()
	{
		echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
			<h2>Account Details</h2>
			<div>'.
				$this->showStickyForm('text', 'email_address').
			'</div>
			<input name="lostpass" type="submit" value="Reset Password" class="button">
		</form>';
	}
	
	public function lostPassword($email)
	{
		if (!$this->valid_email($email) || !$this->userExists($email)) {
			return false;
		}
	 
		$stmt = config::$mysqli->prepare("SELECT user_id FROM members WHERE email = ? LIMIT 1");
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
	 
		if ($stmt->num_rows != 1) {
			return false;
		}
		$stmt->close();
		
		$newpass = $this->generate_code(8);
		$new_seeded_password = sha1($newpass.login::$seed);
	 
		$stmt = config::$mysqli->prepare("UPDATE members SET password = ? WHERE email = ?");
		$stmt->bind_param("ss", $new_seeded_password, $email);
		
		if ($stmt->execute()) {
			if ($this->sendLostPasswordEmail($email, $newpass)) {
				return true;
			}
			else {
				return false;
			}      
		}
		else {
			return false;
		}
		$stmt->close();
		return false;
	}
	
	public function generate_code($length = 10)
	{
		if ($length <= 0) {
			return false;
		}
	 
		$code = "";
		$chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
		srand((double)microtime() * 1000000);
		for ($i = 0; $i < $length; $i++) {
			$code = $code . substr($chars, rand() % strlen($chars), 1);
		}
		return $code;
	}
	
	public function sendLostPasswordEmail($email, $newpassword)
	{
		$sendMail = new mail();
		
		$message = "Thank you for requesting a new password on ".config::SITE_NAME.
		
		"Your new password information:
		
		Password: ".$newpassword.
		
		"Please <a href='".config::$baseUrl."/login.php' title='Login to ".config::SITE_NAME."'>click here</a> to login with your new password.
		
		Regards"
		.config::SITE_NAME." Administration";
	 
		if ($sendMail->sendMail($email, "Password Reset", $message, config::SITE_NAME." <no_reply@".config::DOMAIN.">")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function contactForm()
	{
		echo '<form name="contactform" method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
		<div>'
			.$this->showStickyForm('text','name')
			.$this->showStickyForm('text','email_address')
			.$this->showStickyForm('textarea','comments').
		'</div>
		<input type="submit" value="Submit" name="submitform" id="Submit Form" class="button" />
		</form>';
	}
	
	public function checkContactForm($name, $email_from, $comments)
	{
		$errors = array();
		if(empty($name) || is_numeric($name)) {
			$errors[] = 'Enter a name';
		}
		if(!$this->valid_email($email_from)) {
			$errors[] = 'Enter a valid email address';
		}
		if(strlen($comments) < 4) {
			$errors[] = 'Enter your comments';
		}
		return $errors;
	}
	
	public function sendContactForm($name, $email_from, $email_to, $comments)
	{
		$sendMail = new mail();
		$email_subject = "Enquiries";
			 
		function clean_string($string) {
			$bad = array("content-type","bcc:","to:","cc:","href");
			return str_replace($bad,"",$string);
		}
		$email_message = "Form details below.\n\n";
		$email_message .= "Name: ".clean_string($name)."\n";
		$email_message .= "Email: ".clean_string($email_from)."\n";
		$email_message .= "Comments: ".clean_string($comments)."\n";
		
		if ($sendMail->sendMail($email_to, $email_subject, $email_message, $email_from)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function getCompetitionsList()
	{		
		$comps = array();
		$stmt = config::$mysqli->prepare('SELECT comp_name
		FROM competitions
		ORDER BY comp_id ASC');
		$stmt->execute();
		$stmt->bind_result($comp_name);
		while($stmt->fetch()) {
			$comps[] = $comp_name;
		}
		$stmt->close();
		return $comps;
	}
	
	public function getTeamsList($comp_name)
	{		
		$teams = array();
		$stmt = config::$mysqli->prepare('SELECT t.team_name
		FROM team_names t
		LEFT JOIN tff_teams tff
		ON t.team_id = tff.team_id
		ORDER BY team_name ASC');
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($team_name);
		
		if ($stmt->num_rows != 0) {
			while($stmt->fetch()) {
				$teams[] = $team_name;
			}
		}
		else {
			$stmt1 = config::$mysqli->prepare('SELECT t.team_name
			FROM teams t
			ORDER BY team_name ASC');
			$stmt1->execute();
			$stmt1->bind_result($team_name);
			while($stmt1->fetch()) {
				$teams[] = $team_name;
			}
			$stmt1->close();
		}
		$stmt->close();
		return $teams;
	}
}
?>