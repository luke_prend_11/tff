<?php
class url_utils
{
    const BREADCRUMB_SEPARATOR = ' &raquo; ';
    const BREADCRUMB_HOME_TEXT = 'Home';

    public static $breadcrumbLastElement;
    public static $breadcrumbSecondToLastElement;
    public static $breadcrumbDateElement;
    public static $fullBreadcrumb;

    public function __construct()
    {
        $this->buildBreadcrumb();
    }
	
    private function buildBreadcrumb()
    {
        $path        = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
        $base_url    = substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')).'://'.$_SERVER['HTTP_HOST'].'/';
        $breadcrumbs = array("<a href=\"$base_url\">".self::BREADCRUMB_HOME_TEXT."</a>");
        $tmp         = array_keys($path);
        $last        = end($tmp);
        unset($tmp);

        foreach ($path as $x => $crumb)
        {
            $title = ucwords(str_replace(array('.php', '.html', '_', '-'), array('', '', ' ', ' '), $crumb));
            $title = preg_replace('/[A-Z][0-9]+/', ' ', $title);

            if ($x == 1)
            {
                $breadcrumbs[] = "<a class=\"crumb$x\" href=\"$base_url$crumb\">$title</a>";
            }
            elseif ($x > 1 && $x < $last)
            {
                if($last - $x == 1)
                {
                    self::$breadcrumbSecondToLastElement = $title;
                }

                // do not inlcude link to stats portion of breadcrumb as there is currently no page for this
                if($title == "Stats")
                {
                    $breadcrumbs[] = "<span class=\"crumb$x\">$title</span>";
                }
                // check if is numeric to remove fixture dates from breadcrumb and save fixture date for page title
                elseif(!is_numeric($title))
                {
                    $tmp = "<a class=\"crumb$x\" href=\"$base_url";
                    for($i = 1; $i <= $x; $i++)
                    {
                        $tmp .= $path[$i] . '/';
                    }
                    $tmp .= "\">$title</a>";
                    $breadcrumbs[] = $tmp;
                    unset($tmp);
                }
                elseif(!is_numeric($title) && strlen($title) == 8)
                {
                    $date = DateTime::createFromFormat('Ymd', $title);
                    $matchDate = $date->format('j F Y');
                    self::$breadcrumbDateElement = $matchDate;
                }
                else
                {
                    self::$breadcrumbDateElement = substr_replace($title, 'bob', 4, 0);
                }
            }
            else
            {
                $title = isset($_GET['date']) && isset($_GET['fixture_id']) ? $this->hasResult() : $title;

                $breadcrumbs[] = "<span class=\"last-crumb\">$title</span>";

                // get the last breadcrumb item for page titles etc.
                self::$breadcrumbLastElement = $title;
            }
        }

        self::$fullBreadcrumb = implode(/* self::BREADCRUMB_SEPARATOR,*/ $breadcrumbs);
    }
    
    private function hasResult()
    {
        $fixtureId = htmlspecialchars($_GET['fixture_id']);
        
        $stmt = config::$mysqli->prepare("
        SELECT 
            ht.team_name, 
            at.team_name, 
            r.fthg, 
            r.ftag 
        FROM match_fixtures f
        LEFT JOIN match_results r
        ON r.fixture_id = f.fixture_id
        LEFT JOIN team_names ht
        ON f.ht_id = ht.team_id
        LEFT JOIN team_names at
        ON f.at_id = at.team_id
        WHERE f.fixture_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $fixtureId);
        $stmt->bind_result($homeTeamName, $awayTeamName, $homeScore, $awayScore);
        $stmt->execute();
        $stmt->store_result();
        $stmt->fetch();
        
        return $homeScore === NULL || $awayScore === NULL ? $homeTeamName." v ".$awayTeamName : $homeTeamName." ".$homeScore." - ".$awayScore." ".$awayTeamName;
    }

    public function curPageURL()
    {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on")
        {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80")
        {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
	
    public function parse_query_string($query_string)
    {
        // split the query string into individual name-value pairs
        $items = explode('&', $query_string);
        // initialize the return array
        $qs_array = array();
        // create the array
        foreach($items as $i)
        {
            // split the name-value pair and save the elements to $qs_array
            $pair = explode('=', $i);
            $qs_array[urldecode($pair[0])] = urldecode($pair[1]);
        }
        // return the array
        return $qs_array;
    }
	
    public function remove_query_param($url, $param)
    {
        // extract the query string from $url
        $tokens       = explode('?', $url);
        $url_path     = $tokens[0];
        $query_string = $tokens[1];
        // transform the query string into an associative array
        $qs_array = $this->parse_query_string($query_string);
        // remove the $param element from the array
        unset($qs_array[$param]);
        // create the new query string by joining the remaining parameters
        $new_query_string = '';
        if($qs_array)
        {
            foreach($qs_array as $name => $value)
            {
                $new_query_string .= ($new_query_string == '' ? '?' : '&').urlencode($name).'='.urlencode($value);
            }
        }
        // return the URL that doesn’t contain $param
        $full_path = $url_path . $new_query_string;
        $full_path = str_replace("index.php","",$full_path);
        return $full_path;
    }
}
