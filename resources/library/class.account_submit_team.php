<?php
class account_submit_team
{
    const SEASON_END_ACTIVE        = TRUE;
    const MAX_TEAMS_REACHED_ERROR  = "Sorry you cannot submit any additional teams this season as you have reached your maximum team allocation of %s.";
    const COMPETITION_CLOSED_ERROR = "The competition has closed for the current season. You will be notified by email once the new season is up and running.";
    const REGISTERATION_SUCCESS    = "Congratulations. You have successfully registered with %s<br /><br />A confirmation email has been sent to the email address you provided, you may need to check your junk mail folder. Please add %s to the safe senders list with your email provider to ensure you don\'t miss any updates.";
    const SUBMIT_TEAM_SUCCESS      = "Thank you for submitting a new team. You can make unlimited transfers until the season begins on <strong>%s</strong>. After that point you can make %s transfers throughout the season.";
	
    public $registrationSuccessMessage;

    private $submitTeamSuccessMessage;
    private $maxTeamsMessage;
	
    public function __construct()
    {		
        $this->setRegistrationSuccessMessage();
        $this->setSubmitTeamSuccessMessage();
        $this->setMaxTeamsMessage();
    }
	
    private function setRegistrationSuccessMessage()
    {
        $this->registrationSuccessMessage = sprintf(self::REGISTERATION_SUCCESS, config::SITE_NAME, config::CONTACT_EMAIL);
    }

    private function setSubmitTeamSuccessMessage()
    {
        $this->submitTeamSuccessMessage = sprintf(self::SUBMIT_TEAM_SUCCESS, date("l j F Y", strtotime(config::$curSeasonOpeningFixtureDate)), config::$maxTransfers);
    }

    private function setMaxTeamsMessage()
    {
        $this->maxTeamsMessage = sprintf(self::MAX_TEAMS_REACHED_ERROR, config::MAX_USER_TEAMS);
    }
	
    private function checkCompetitionOpen()
    {
        if(time() <= strtotime(config::$curSeasonEnd))
        {
            return true;
        }
        return false;
    }
	
    private function checkNumberOfTeamEntries()
    {
        if($this->getNumberOfTeamEnries() < config::MAX_USER_TEAMS)
        {
            return true;
        }
        return false;
    }
	
    private function getNumberOfTeamEnries()
    {
        $stmt = config::$mysqli->prepare("
        SELECT COUNT(user_team_id)
        FROM user_teams
        WHERE user_id = ?
        AND date > ?
        AND date < ?");
        $stmt->bind_param("iss", $_COOKIE['user_id'], config::$curSeasonStart, config::$curSeasonEnd);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($number_of_teams);
        $stmt->fetch();
        return $number_of_teams;
    }
	
	private function showTeamSelectionForm()
	{
		$u       = new url_generator();
		$f       = new user_functions();
		$team_id = array();
		
		echo '
		<form name="form1" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
			<h2>Enter a Team Name</h2>
			<div class="very-light-grey">
				<input name="user_id" type="hidden" value="';
				if(isset($_COOKIE['user_id'])) echo $_COOKIE['user_id'];
				echo '">'
				.$f->showStickyForm('text','user_team_name','',false,'Team Name').'
			</div>
			
			<h2>Make your team selections</h2>
			<p>Choose 1 team from each group and keep within your available budget.</p>
			
			<a id="test" onClick="toggle_visibility(\'points\');" class="show-hide closed very-light-grey">How do teams score points?</a>
			<div id="points" style="display:none;" class="very-light-grey">
				<h3>Scoring Points</h3>'.
				notifications::$scoringPointsInfo.
			'</div>
			
			<div id="teams-list">';
			
			$s         = '';
			$c         = '';
			$row_class = 1;
			
			$stmt = config::$mysqli->prepare("SELECT tt.team_id, t.team_name, tt.selection_group, tt.value, c.comp_name
			FROM tff_teams tt
			LEFT JOIN team_names t
			ON tt.team_id = t.team_id
			LEFT JOIN competitions c
			ON tt.league_id = c.comp_id
			ORDER BY tt.league_id ASC, tt.value DESC");
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($team_id, $team_name, $selection_group, $value, $comp_name);
			while ($stmt->fetch())
			{
				
				if($comp_name != $c)
				{
					echo '<fieldset id="'.$comp_name.'">';
					
					echo '
					<table>
						<thead>
							<tr>
								<th colspan="3"><span>'.$comp_name.'</span></th>
							</tr>
						</thead>';
				}
				
				if($selection_group != $s)
				{
					echo '
					<tr class="table-sub-header">
						<td colspan="3">SG'.$selection_group.'</td>
					</tr>';
				}
				
				$row_class = 1 - $row_class;
				
				echo '
				<tr class="table-row'.$row_class.'">
					<td width="15px"><input type="radio" class="rad" name="team_id['.$selection_group.']" id="'.$value.'" value="'.$team_id.'"';
					if (isset($_POST['team_id'][$selection_group]) && $_POST['team_id'][$selection_group] == $team_id) 
						echo ' checked="checked" ';
					echo '/></td>
					<td class="club" style="background-image:url('.config::$baseUrl.'/img/content/team-badges/'.$team_id.'.png); background-size:18px 18px;">'.
						$u->showLink($team_name, $team_id, 'team').'
					</td>
					<td class="align-right">£'.$value.'m</td>
				</tr>';
				$s = $selection_group;
				$c = $comp_name;
			}
			echo '</table>
			</fieldset>
			</div>';
			
			echo '
			<div id="budget-submit">
				<h4>BUDGET</h4>
				<div id="budget">
					<span id="total" class="budget">£'.config::BUDGET.'m</span>
					<input type="submit" name="submit_team" value="Submit" class="button">
				</div>
			</div>
			<br class="clearfloat" />
		</form>';
	}
	
	private function checkSubmitNewTeam($user_team_name, $team_ids)
	{
		$error_message = array();
		$number_teams_selected = $team_ids <> '' ? count($team_ids) : 0;
		
		if(empty($user_team_name) || strlen($user_team_name) < 4)
		{
			$error_message[] = "Please enter a Team Name of more than 3 characters";
		}
		if(count($team_ids) < 11)
		{
			$error_message[] = "Please ensure you have selected 11 teams. You currently only have ".$number_teams_selected." selected";
		}
		if($number_teams_selected <> 0 && $this->getTeamsTotalValue($team_ids) > config::BUDGET)
		{
			$error_message[] = "Please ensure your teams total value does not exceed the allocated budget of £".config::BUDGET."m";
		}
		
		return $error_message;
	}
	
	private function getTeamsTotalValue($team_ids)
	{
		$team_ids = implode(',', $team_ids);
		
		$stmt = config::$mysqli->prepare("SELECT SUM(value) FROM tff_teams WHERE team_id IN (".$team_ids.")");
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($total_value);
		$stmt->fetch();
		
		return $total_value;
	}
	
	private function insertNewTeam($user_team_name, $teamIDs)
	{
		$stmt = config::$mysqli->prepare("
		INSERT INTO user_teams (user_id, user_team_name, date, season_id)
		VALUES (?, ?, ?, ?)");
		$stmt->bind_param("isss", $_COOKIE['user_id'], $user_team_name, config::$curDate, config::$curSeasonId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->close();
		
		$user_team_id         = config::$mysqli->insert_id;
		$teamIDs              = array_filter(array_map('intval', $teamIDs));
		$valuesAry            = array();
		$default_transfer_out = config::DEFAULT_TRANSFER_OUT;
		
		foreach($teamIDs as $team_id) {
			$valuesAry[] = array($user_team_id, $team_id);
		}
		
		$stmt1 = config::$mysqli->prepare("
		INSERT INTO user_team_selections (user_team_id, team_id, transfer_in, transfer_out) 
		VALUES (?, ?, ?, ?)");
		$stmt1->bind_param("iiss", $user_team_id, $team_id, config::$curDate, $default_transfer_out);
		
		foreach ($valuesAry as $values) {
			$user_team_id = $values[0];
			$team_id      = $values[1];
			$stmt1->execute();
		}
		$stmt1->close();
		
		$stmt = config::$mysqli->prepare("
		INSERT INTO user_team_points (user_team_id)
		VALUES (?)");
		$stmt->bind_param("i", $user_team_id);
		$stmt->execute();
		$stmt->store_result();
		$stmt->close();
		
		return true;
	}
	
    public function submitTeam()
    {
        if(!$this->checkCompetitionOpen() && self::SEASON_END_ACTIVE == TRUE)
        {
            echo notifications::showNotification('error', FALSE, self::COMPETITION_CLOSED_ERROR);
        }
        elseif(!$this->checkNumberOfTeamEntries())
        {
            echo notifications::showNotification('error', FALSE, $this->maxTeamsMessage);
        }
        elseif(isset($_POST['submit_team']))
        {
            $user_team_name = isset($_POST['user_team_name']) ? $_POST['user_team_name'] : '';
            $team_ids       = isset($_POST['team_id']) ? $_POST['team_id'] : '';
            $errors         = $this->checkSubmitNewTeam($user_team_name, $team_ids);
            if(empty($errors))
            {
                $this->insertNewTeam($_POST['user_team_name'], $_POST['team_id']);
                echo notifications::showNotification('success', TRUE, $this->submitTeamSuccessMessage);
                echo '<br />
                <h2>What Now?</h2>
                <div class="row">
                    <h3 id="pl-heading">Create a Private League</h3>
                    <p>Create a new private league for <strong>'.$_POST['user_team_name'].'</strong> and challenge your friends.</p>
                    <a class="btn blue float-left" href="'.config::$baseUrl.'/my-account/leagues/create" title="Create a Private League" id="private-league">Create a Private League</a>
                    <br class="clearfloat" />
                </div>

                <div class="row">
                    <h3 id="pl-heading">Join a Private League</h3>
                    <p>Enter <strong>'.$_POST['user_team_name'].'</strong> into an existing private league.</p>
                    <a class="btn grey float-left" href="'.config::$baseUrl.'/my-account/leagues/join" title="Join a Private League" id="private-league">Join a Private League</a>
                    <br class="clearfloat" />
                </div>
                <br />';
            }
            else
            {
                echo notifications::showNotification('error', TRUE, $errors);
                $this->showTeamSelectionForm();
            }
        }
        else
        {
            $this->showTeamSelectionForm();    
        }
    }
}
