<?php
class pagination
{
    const PAGINATION_ERROR = "No results in array";
    
    private function sortResults(array $resultsArray, $limit = 10)
    {
        if(empty($resultsArray))
        {
            echo notifications::showNotification('error', FALSE, self::PAGINATION_ERROR);
        }
    }
    
    public function displayPageOptions($dataArray, $resultsPerPage = 10, $pageNumbers = FALSE, $previousNextButtons = TRUE)
    {
        if($previousNextButtons === TRUE)
        {
            return '
                <a href="#" title="Previous">
                    Previous
                </a>
                <a href="#" title="Next">
                    Next
                </a>
            ';
        }
    }
}