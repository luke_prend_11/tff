<?php
class admin_import_results extends admin_data
{
    const FOOTBALL_DATA_CSV    = "http://www.football-data.co.uk/mmz4281/%s/%s.csv";
    const FOOTBALL_DATA_API    = "http://api.football-data.org/v1/%s";
    const PERCENT_TO_MATCH     = 50;
    const ALT_TEAM_SUCCESS     = "Alternate team name has been entered successfully";
    const FIXTURES_SUCCESS     = "Fixtures have been entered successfully";
    const RESULTS_SUCCESS      = "Results have been entered successfully";
    const MAX_RESULTS_TO_ENTER = 25;
    
    private $userFunctions;
    private $urlGenerator;
    private $adminResults;
    
    private $compeitionIds    = array();
    private $competitionNames = array();
    private $seasonNames      = array();
    
    private $alternativeTeamName;
    private $teamNametoMatchTo;
    private $teamNameAlternate;
    private $allTeamsNoDuplicates;
    private $currentFixtureId;
    
    public function __construct($db, $uf, $ug)
    {
        parent::__construct($db);
                
        $this->checkFormSubmission();
        $this->userFunctions = $uf;
        $this->urlGenerator  = $ug;
        $this->setCompeitionDetails();
        $this->setSeasonDetails();
        
        $ar = new admin_results($this->urlGenerator, $this->userFunctions, $this->database);
        $this->adminResults = $ar;
    }
    
    private function checkFormSubmission()
    {
        if(filter_input(INPUT_POST, 'submit_process_csv'))
        {
            $_SESSION['competitionToImport'] = filter_input(INPUT_POST, 'competition');
            $_SESSION['seasonToImport']      = filter_input(INPUT_POST, 'season');
            $this->checkCsvDetails();
        }
        if(filter_input(INPUT_POST, 'submit_alt_team_name'))
        {
            $this->alternativeTeamName = filter_input(INPUT_POST, 'team_name_alt');
            $this->teamNametoMatchTo   = filter_input(INPUT_POST, 'team_to_match');
            $this->checkTeamNameMatching();
        }
        if(filter_input(INPUT_POST, 'submit_fixtures'))
        {
            $this->insertFixtures();
        }
        if(filter_input(INPUT_POST, 'submit_results'))
        {
            $this->insertResults();
        }
    }
    
    private function checkCsvDetails()
    {
        if(empty($_SESSION['competitionToImport']) || empty($_SESSION['seasonToImport']))
        {
            notifications::$formNotification = 'Both fields must be selected';
        }
    }
    
    private function checkTeamNameMatching()
    {
        $teamToMatchExplode = explode('|', $this->teamNametoMatchTo);
        $teamId             = $teamToMatchExplode[0];
        $teamName           = $teamToMatchExplode[1];
        similar_text($this->alternativeTeamName, $teamName, $percentMatch); 
        
        if($percentMatch >= self::PERCENT_TO_MATCH)
        {
            $this->insertTeamName($teamId, $this->alternativeTeamName);
            return;
        }
        notifications::$formNotification = 'Team names were less than a '.self::PERCENT_TO_MATCH.'% match.';
    }
    
    private function setCompeitionDetails()
    {
        $stmt = config::$mysqli->prepare("
        SELECT c.comp_id, c.comp_name
        FROM competitions c
        WHERE c.comp_id >= 1
        AND c.comp_id <= 4
        ORDER BY c.comp_id ASC");
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0)
        {
            exit(config::$mysqli->error);
        }
        else 
        {
            $stmt->bind_result($compId, $compName);
            while($stmt->fetch())
            {
                $this->compeitionIds[]    = $compId;
                $this->competitionNames[] = $compName;
            }
        }
    }
    
    private function setSeasonDetails()
    {
        $stmt = config::$mysqli->prepare("
        SELECT s.season_name
        FROM tff_seasons s
        WHERE s.season_start <= NOW()
        ORDER BY s.season_start ASC");
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0)
        {
            exit(config::$mysqli->error);
        }
        else 
        {
            $stmt->bind_result($seasonName);
            while($stmt->fetch())
            {
                $this->seasonNames[] = $seasonName;
            }
        }
    }
    
    public function displayImportResultsPage()
    {
        if(filter_input(INPUT_POST, 'submit_process_csv'))
        {
            if(empty(notifications::$formNotification))
            { 
                $this->processMatches();
            }
            else
            {
                echo notifications::showNotification('error');
                $this->showSelectSeasonAndCompetition();
            }
        }
        elseif(filter_input(INPUT_POST, 'submit_alt_team_name'))
        {
            if(empty(notifications::$formNotification))
            { 
                echo notifications::showNotification('success', TRUE, self::ALT_TEAM_SUCCESS);
                $this->processMatches();
            }
            else
            {
                echo notifications::showNotification('error');
            }
        }
        elseif(filter_input(INPUT_POST, 'submit_fixtures'))
        {
            if(empty(notifications::$formNotification))
            { 
                echo notifications::showNotification('success', TRUE, self::FIXTURES_SUCCESS);
                $this->processMatches();
            }
            else
            {
                echo notifications::showNotification('error');
            }
        }
        elseif(filter_input(INPUT_POST, 'submit_results'))
        {
            if(empty(notifications::$formNotification))
            { 
                echo notifications::showNotification('success', TRUE, self::RESULTS_SUCCESS);
                $this->showSelectSeasonAndCompetition();
            }
            else
            {
                echo notifications::showNotification('error');
            }
        }
        else
        {
            $this->showSelectSeasonAndCompetition();
        }
    }
    
    private function showSelectSeasonAndCompetition()
    {
        echo '<h2>Select Results to Import</h2>'
        .'<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">'
            .'<div>'
                .$this->userFunctions->showStickyForm('select', 'competition', $this->competitionNames, false, '', $this->compeitionIds)
                .$this->userFunctions->showStickyForm('select', 'season', $this->seasonNames)
            .'</div>'
            .'<input name="submit_process_csv" type="submit" value="Submit" class="button" />'
        .'</form>';
    }
    
    private function processMatches()
    {
        $this->processCSV($this->getCsvUrl());
        
        // make list of team names from fixtures with no duplicates
        $this->getTeamNamesList();

        if($this->checkTeamNames())
        {
            // add team ids
            $this->addTeamIdsToImportData();
        }
        else
        {
            echo notifications::showNotification('error');
            $this->showInsertAltTeamName();
            return;
        }

        // fixture ids will be added if home team, away team, date and competition match up or else fixture will be added to fixtureImport array
        if(!$this->checkFixtureEntry())
        {
            echo notifications::showNotification('error');
            $this->showInsertFixtures();
            return;
        }

        // unset any results that have already been entered
        $this->checkResultEntry();

        if(!empty($_SESSION['incorrectResultsImportData']))
        {
            $this->showInsertResults($_SESSION['incorrectResultsImportData']);
            return;
        }
        elseif(!empty($_SESSION['resultsImportData']))
        {
            $this->showInsertResults($_SESSION['resultsImportData']);
            return;
        }
        elseif(!empty($_SESSION['insertPointsData']))
        {
            $ar = new admin_results($this->urlGenerator, $this->userFunctions, $this->database);
            $ar->insertIndividualTeamPoints();
            echo notifications::showNotification('error', TRUE, 'Team points have been inserted.');
        }
        else
        {
            echo notifications::showNotification('error', TRUE, 'Results have already been entered.');
            $this->showSelectSeasonAndCompetition();
        }
    }
    
    private function addTeamIdsToImportData()
    {
        // add team ids to result data
        foreach($this->csvImportData as &$match)
        {
            $match['homeId'] = $this->getTeamId($match[2]);
            $match['awayId'] = $this->getTeamId($match[3]);
        }
    }
    
    private function getTeamId($teamName)
    {
        $stmt = config::$mysqli->prepare("
        SELECT tn.team_id
        FROM team_names tn
        LEFT JOIN team_names_alt tna
        ON tn.team_id = tna.team_id
        WHERE tn.team_name = ?
        OR tna.alt_team_name = ?
        LIMIT 1");
        $stmt->bind_param("ss", $teamName, $teamName);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0)
        {
            return false;
        }
        $stmt->bind_result($teamId);
        $stmt->fetch();
        return $teamId;
    }
    
    private function checkFixtureEntry()
    {
        foreach($this->csvImportData as &$match)
        {
            if($this->checkFixtureDetails($match))
            {
                $match['fixtureId'] = $this->currentFixtureId;
            }
            else
            {
                $_SESSION['fixturesImportData'][] = $match;
            }
        }
        
        if(!empty($_SESSION['fixturesImportData']))
        {
            notifications::$formNotification = 'Fixtures need inserting';
            return false;
        }
        return true;
    }
    
    private function checkFixtureDetails($match)
    {
        $stmt = config::$mysqli->prepare("
        SELECT f.fixture_id
        FROM match_fixtures f
        WHERE f.ht_id = ?
        AND f.at_id = ?
        AND date(f.date) = STR_TO_DATE(?, '%d/%m/%y')
        AND f.comp_id = ?
        LIMIT 1");
        $stmt->bind_param("iisi", $match['homeId'], $match['awayId'], $match[1], $_SESSION['competitionToImport']);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0)
        {
            return false;
        }
        $stmt->bind_result($this->currentFixtureId);
        $stmt->fetch();
        return true;
    }
    
    private function checkResultEntry()
    {
        $ar = new admin_results($this->urlGenerator, $this->userFunctions, $this->database);
        unset($_SESSION['incorrectResultsImportData']);
        unset($_SESSION['resultsImportData']);
        unset($_SESSION['insertPointsData']);
        $_SESSION['resultsImportData'] = array();
        foreach($this->csvImportData as $key => $match)
        {
            if($this->checkResultExists($match))
            {
                // check scores are correct
                if($this->checkResultExists($match, $checkGoals = TRUE))
                {
                    // result already exists so we need to unset
                    unset($this->csvImportData[$key]);
                }
                else
                {
                    // result is incorrect and needs updating
                    $_SESSION['incorrectResultsImportData'][] = $ar->setFieldNames('import', '', $match);
                }
                
                // check team points table has been entered and is correct
                if($this->checkTeamPoints($match))
                {
                    // result is incorrect and needs updating
                    $_SESSION['insertPointsData'][] = $ar->setFieldNames('import', '', $match);
                }
            }
            elseif(config::FANTASY_FOOTBALL_ACTIVE === TRUE)
            {
                if(count($_SESSION['resultsImportData']) < self::MAX_RESULTS_TO_ENTER)
                {
                    $_SESSION['resultsImportData'][] = $ar->setFieldNames('import', '', $match);
                }
            }
            else
            {
                $_SESSION['resultsImportData'][] = $ar->setFieldNames('import', '', $match);
            }
        }
    }
    
    private function checkResultExists($match, $checkGoals = FALSE)
    {
        date_default_timezone_set('Europe/London');
        // if either goals scored value is empty return true to unset the match from array i.e prevent entry to match_results
        if($match[4] == '' || $match[5] == '')
        {
            return true;
        }
        
        // if checking to make sure score was correct set query string accordingly
        $stmtQuery = $checkGoals === TRUE ? "AND r.fthg = $match[4] AND r.ftag = $match[5]" : "";
        
        $stmt = config::$mysqli->prepare("
        SELECT r.fixture_id
        FROM match_results r
        WHERE r.fixture_id = ?
        $stmtQuery
        LIMIT 1");
        $stmt->bind_param("i", $match['fixtureId']);
        $stmt->execute();
        $stmt->store_result();
        
        // if fixture already exists return true to unset the match from array i.e prevent re-entry to match_results
        if($stmt->num_rows != 0)
        {
            return true;
        }
        return false;
    }
    
    private function checkTeamPoints($match)
    {
        $stmt = config::$mysqli->prepare("
        SELECT tp.fixture_id
        FROM team_points tp
        WHERE tp.fixture_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $match['fixtureId']);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows != 0)
        {
            return false;
        }
        return true;
    }
    
    private function getCsvUrl()
    {
        return sprintf(self::FOOTBALL_DATA_CSV, $this->getSeasonUrlPart(), $this->getCompetitionUrlPart());
    }

    private function getSeasonUrlPart()
    {
        return str_replace("/", "", substr($_SESSION['seasonToImport'], 2, 5));
    }
    
    private function getCompetitionUrlPart()
    {
        return 'E'.($_SESSION['competitionToImport'] - 1);
    }
    
    private function checkTeamNames()
    {
        // for each team name check if it exists and if not add to team_names_alt table
        foreach($this->allTeamsNoDuplicates as $this->teamNameAlternate)
        {
            if(!$this->checkTeamName())
            {
                notifications::$formNotification = '<strong>'.$this->teamNameAlternate.'</strong> has no match. Select from team names below';
                return false;
            }
        }
        return true;
    }
    
    private function getTeamNamesList()
    {
        // merge home teeam and away team names
        foreach($this->csvImportData as $match)
        {
            $allTeams[] = $match[2];
            $allTeams[] = $match[3];
        }
        
        // remove duplicates
        $this->allTeamsNoDuplicates = array_unique($allTeams);
    }
    
    private function checkTeamName()
    {
        $stmt = config::$mysqli->prepare("
        SELECT tn.team_name, tna.alt_team_name
        FROM team_names tn
        LEFT JOIN team_names_alt tna
        ON tn.team_id = tna.team_id
        WHERE tn.team_name = ?
        OR tna.alt_team_name = ?
        LIMIT 1");
        $stmt->bind_param("ss", $this->teamNameAlternate, $this->teamNameAlternate);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0)
        {
            return false;
        }
        return true;
    }
    
    private function showInsertFixtures()
    {
        echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">'
            .'<h2>Fixtures to Import</h2>'
            .'<ul class="list-none">';

            foreach($_SESSION['fixturesImportData'] as $fixtures)
            {
                echo "<li>".$fixtures[1]." ".$fixtures[2]." v ".$fixtures[3]."</li>";
            }
            echo '</ul>'
            .'<input name="submit_fixtures" type="submit" value="Submit" class="button" />'
        .'</form>';
    }
    
    private function showInsertResults($resultsToShow)
    {
        echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">'
            .'<h2>Results to Import</h2>'
            .'<input name="submit_results" type="submit" value="Submit" class="button" />'
            .'<ul class="list-none">';

            foreach($resultsToShow as $results)
            {
                echo "<li>".$results['date']." ".$results['htName']." ".$results['fthg']." - ".$results['ftag']." ".$results['atName']."</li>";
            }
            echo '</ul>'
        .'</form>';
    }
    
    private function showInsertAltTeamName()
    {
        echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">'
                .'<input type="hidden" name="team_name_alt" value="'.$this->teamNameAlternate.'">'
                .'<h2>Team to Match</h2>'
                .'<div>'
                    .'<label>'.$this->teamNameAlternate.'</label>'
                    .'<select name="team_to_match">';
        
                    foreach($this->getTeamsList() as $teams)
                    {
                        echo '<option value="'.$teams['team_id'].'|'.$teams['team_name'].'">'.$teams['team_name'].'</option>';
                    }
                    echo '</select>'
                .'</div>'
                .'<input name="submit_alt_team_name" type="submit" value="Submit" class="button" />'
            .'</form>';
    }
    
    private function getTeamsList()
    {
        $stmt = config::$mysqli->prepare("
        SELECT tn.team_id, tn.team_name
        FROM team_names tn
        LEFT JOIN tff_teams tt
        ON tn.team_id = tt.team_id
        ORDER BY tn.team_name ASC");
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 0)
        {
            $teamDetails[] = 'Competition Id appears to be incorrect';
        }
        else 
        {
            $stmt->bind_result($teamId, $teamName);
            while($stmt->fetch())
            {
                $teamDetails[] = array("team_id" => $teamId, "team_name" => $teamName);
            }
        }
        return $teamDetails;
    }
    
    public function insertTeamName($teamId, $altTeamName)
    {
        if($this->database->completeInsert('team_names_alt', array('team_id', 'alt_team_name'), array($teamId, $altTeamName), array('i','s')))
        {
            return;
        }
    }
    
    public function insertResults()
    {
        $ar = new admin_results($this->urlGenerator, $this->userFunctions, $this->database);
        
        // set results array
        $ar->resultsArray = isset($_SESSION['incorrectResultsImportData']) ? $_SESSION['incorrectResultsImportData'] : $_SESSION['resultsImportData'];
        
        // insert match results
        $ar->insertMatchResults();

        // insert team points
        $ar->insertIndividualTeamPoints();

        // insert league tables
        $ar->createLeagueTables();

        if(config::FANTASY_FOOTBALL_ACTIVE === TRUE)
        {
            // create leaderboards
            $ar->createLeaderboards();
        }
    }
    
    public function insertFixtures()
    {
        date_default_timezone_set('Europe/London');
        foreach($_SESSION['fixturesImportData'] as $values)
        {
            $date = DateTime::createFromFormat('d/m/y', $values[1]);
            $fixtureDate = $date->format('Y-m-d');
            
            if($this->database->completeInsert('match_fixtures', array('comp_id', 'ht_id', 'at_id', 'date'), array($_SESSION['competitionToImport'], $values['homeId'], $values['awayId'], $fixtureDate), array('i','i','i','s')))
            {
                $this->importSuccess($values);
            }
            else
            {
                $this->importFail($values);
            }
        }
        unset($_SESSION['fixturesImportData']);
    }
    
    
    
    private function showFootballApi()
    {
        $uri = 'http://api.football-data.org/v1/competitions/?season=2015';
        $reqPrefs['http']['method'] = 'GET';
        $reqPrefs['http']['header'] = 'X-Auth-Token: 30e6defbbe154e1e86d16db3abd4ee3f';
        $stream_context = stream_context_create($reqPrefs);
        $response = file_get_contents($uri, false, $stream_context);
        $fixtures = json_decode($response);
        print_r($fixtures);
    }
}
