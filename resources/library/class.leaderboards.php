<?php
class leaderboards
{
	const HIGHLIGHT_TEAM_CLASS = "class=\"my-teams\"";
	const NO_TEAMS_ERROR       = "No teams have been submitted for the current season";
	
	private $urlGenerator;
	private $loggedInUserId;
	
	private $leaderboardSize;
	private $leaderboardType;
	private $leaderboardLimit;
	private $leaderboardTeamToHighlight;
	private $leaderboardprivateLeagueId;
	
	private $privateLeagueId;
	
	private $userTeamUserId;
	private $userTeamId;
	private $userTeamName;
	private $userTeamEntryDate;
	private $userTeamRank;
	private $userTeamPoints;
	private $userTeamPreviousRank;
	private $userTeamRankChange;
	private $userTeamUserName;
	
	private $leaderboardString;
	
	private $topPrivateLeagueName;
	private $topPrivateLeagueId;
	
	private $currentMonthId;
	private $currentWeekId;
	
	public function __construct($ug)
	{
		$this->urlGenerator = $ug;
		$this->loggedInUserId = isset($_COOKIE['user_id']) ? $_COOKIE['user_id'] : '';
		
		if (isset($_POST['find_team']))
		{
			$this->findTeamName = $_POST['team_name'];
			// find a team submit button has been clicked so check for any errors
			if($findUserTeamId = $this->checkFindTeam())
			{
				$_SESSION['anchor'] = $findUserTeamId;
			}
		}
	}
	
	private function setPrivateLeagueIdFromUrl()
	{
		$this->privateLeagueId = htmlspecialchars($_GET['pleague_id']);
	}
	
	public function showLeaderboardPage()
	{
		$this->showLeaderboardOptionButtons();
		$this->showFindTeam();
		
		$teamToHighlight = isset($_SESSION['anchor']) ? $_SESSION['anchor'] : '';
		
		$this->leaderboardType = isset($_SESSION['ldb']) ? $_SESSION['ldb'] : 'season';
		echo $this->showLeaderboard('full', $this->leaderboardType, $teamToHighlight);
		echo '<p class="scroll-up">&uarr; UP &uarr;</p>';
	}
	
	public function showPrivateLeagueLeaderboardPage()
	{
		$this->showLeaderboardOptionButtons();
		$this->showFindTeam();
		$this->setPrivateLeagueIdFromUrl();
		
		$teamToHighlight = isset($_SESSION['anchor']) ? $_SESSION['anchor'] : '';
		
		$this->leaderboardType = isset($_SESSION['ldb']) ? $_SESSION['ldb'] : 'season';
		echo $this->showLeaderboard('full', $this->leaderboardType, $teamToHighlight, '', $this->privateLeagueId);
		echo '<p class="scroll-up">&uarr; UP &uarr;</p>';
	}
	
	private function showLeaderboardOptionButtons()
	{
		$this->setLeaderboardOptionButtonCredentials();
		
		echo '
		<div class="row stretch-parent">';
		
		foreach($this->leaderboardTypes as $value => $attribute)
		{
			echo '
			<a href="index.php?ldb='.$value.'" title="'.ucwords($value).' Leaderboard" class="stretch-child ldb-btn '.$attribute['color'];
                        if((isset($_SESSION['ldb']) && $_SESSION['ldb'] != $value) || (!isset($_SESSION['ldb']) && $value != 'season')) { echo ' opaque'; }
			echo '">
				'.ucwords($value).'
			</a>';
		}
		
		echo '
			<span class="stretch"></span>
		</div>';
	}
	
	private function setLeaderboardOptionButtonCredentials()
	{			
		$this->leaderboardTypes = array(
			"season" => array(
				"color" => "blue"
			),
			"month" => array(
				"color" => "orange"
			),
			"week" => array(
				"color" => "dark-grey"
			)
		);
	}
	
	private function showFindTeam()
	{
		if(isset($_POST['find_team']))
		{
			if(!$this->checkFindTeam())
			{
				echo notifications::showNotification('error', TRUE, 'No team found for <strong>'.$this->findTeamName.'</strong>. Please try again.');
				$this->findTeam();
			}
			else
			{
				$this->findTeam();
			}
		}
		else
		{
			$this->findTeam();
		}
	}
	
	private function findTeam()
	{
		echo '
		<form action="'.htmlentities($_SERVER['PHP_SELF']).'" method="post" class="ldb-fnd">
			<div class="very-light-grey">
				<label for="team_name">Find Team</label>
				<input id="team_name" type="text" name="team_name" onclick="this.value=\'\';" onfocus="this.select()" onblur="this.value=!this.value?\'Enter Team Name...\':this.value;" value="Enter Team Name..." />
				<input type="submit" value="Go" name="find_team" class="button" />
			</div>
		</form>';
	}
	
	private function checkFindTeam()
	{
		$stmt = config::$mysqli->prepare("
		SELECT user_team_id
		FROM user_teams
		WHERE user_team_name = ?
		AND season_id = ?
		LIMIT 1");
		$stmt->bind_param("si", $this->findTeamName, config::$curSeasonId);
		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows == 0)
		{
			$stmt->close();
			return false;
		}
		$stmt->bind_result($userTeamId);
		$stmt->fetch();
		$stmt->close();
		return $userTeamId;
	}
	
	private function getRowClass()
	{
		if(($this->userTeamUserId == $this->loggedInUserId) || ($this->userTeamId == $this->leaderboardTeamToHighlight))
		{
			return self::HIGHLIGHT_TEAM_CLASS;
		}
		return 'class="table-row'.config::$rowClass.'"';
	}
	
	public function showLeaderboard($leaderboardSize, $leaderboardType, $teamToHighlight = '', $leaderboardLimit = '', $privateLeagueId = '')
	{
		$this->leaderboardSize            = $leaderboardSize;
		$this->leaderboardType            = $leaderboardType;
		$this->leaderboardLimit           = $leaderboardLimit;
		$this->leaderboardTeamToHighlight = $teamToHighlight;
		$this->leaderboardprivateLeagueId = $privateLeagueId;
		
		$this->setLeaderboardQueryCredentials();
		
		$stmt = $this->setLeaderboardDetails();
		
		if($stmt->num_rows() != 0)
		{
			$this->leaderboardString = $this->leaderboardHeader();
		
			if($this->leaderboardSize == 'full' || $this->leaderboardSize == 'short')
			{
				while($stmt->fetch())
				{
					$this->leaderboardString .= $this->leaderboardBody();
					config::updateRowClass();
				}
			}
			else
			{
				return notifications::showNotification('error', TRUE, 'Sorry there has been an unexpected error.');
			}
			$stmt->close();
			$this->leaderboardString .= '</table>';
			
			return $this->leaderboardString;
		}
		
		return notifications::showNotification('error', FALSE, self::NO_TEAMS_ERROR);
	}
	
	private function setLeaderboardQueryCredentials()
	{
		$this->leaderboardQueryLeaderboardType = $this->leaderboardprivateLeagueId == '' ? 'ldb' : 'pl';
		$this->leaderboardQueryPrivateLeague   = $this->leaderboardprivateLeagueId == '' ? '' : 'AND pl_id = '.$this->leaderboardprivateLeagueId;
		
		if($this->leaderboardType == 'month')
		{
			$this->setCurrentMonthId();
			$this->leaderboardQueryAndClause = 'AND ldb.month_id = '.$this->currentMonthId;
		}
		elseif($this->leaderboardType == 'week')
		{
			$this->setCurrentWeekId();
			$this->leaderboardQueryAndClause = 'AND ldb.week_id = '.$this->currentWeekId;
		}
		else
		{
			$this->leaderboardQueryAndClause = '';
		}
	}
	
	private function setLeaderboardDetails()
	{
		$stmt = config::$mysqli->prepare("
		SELECT
			ut.user_id,
			ut.user_team_id,
			ut.user_team_name,
			ut.date, 
			ldb.rank,
			ldb.points,
			ldb.previous_rank,
			(ldb.previous_rank - ldb.rank),
			CONCAT(u.first_name, ' ', u.last_name) AS name
		FROM user_teams ut
		LEFT JOIN members u
		ON u.user_id = ut.user_id
		LEFT JOIN ".$this->leaderboardQueryLeaderboardType."_".$this->leaderboardType." ldb
		ON ut.user_team_id = ldb.user_team_id
		WHERE ut.season_id = ? $this->leaderboardQueryPrivateLeague
		$this->leaderboardQueryAndClause
		ORDER BY rank ASC, ut.user_team_name ASC
		$this->leaderboardLimit");
		$stmt->bind_param("i", config::$curSeasonId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result(
			$this->userTeamUserId,
			$this->userTeamId, 
			$this->userTeamName, 
			$this->userTeamEntryDate, 
			$this->userTeamRank, 
			$this->userTeamPoints, 
			$this->userTeamPreviousRank, 
			$this->userTeamRankChange, 
			$this->userTeamUserName
		);
		
		return $stmt;
	}
	
	private function leaderboardHeader()
	{
		$tHeadClass   = $this->leaderboardSize == 'short' ? ' class="short"' : '';
		$tHeadRank    = $this->leaderboardSize == 'short' ? '<th>&nbsp;' : '<th><span>Pos</span>';
		$tHeadManager = $this->leaderboardSize == 'short' ? '' : '<th class="hidden"><span>Manager</span>';
		$tHeadPoints  = $this->leaderboardSize == 'short' ? 'Pts' : 'Points';
		
		return '
		<table>
			<thead'.$tHeadClass.'>
				<tr>'
					.$tHeadRank.'
					<th><span>Team</span>'
					.$tHeadManager.'
					<th class="align-right"><span>'.$tHeadPoints.'</span>
			<tbody>';
	}
	
	private function leaderboardBody()
	{
		$tBodyAnchor  = $this->leaderboardSize == 'short' ? '' : '<a id="'.$this->userTeamId.'" class="'.$this->userTeamId.'"></a>';
		$tBodyManager = $this->leaderboardSize == 'short' ? '' : '<td class="hidden">'.$this->userTeamUserName;
		
		return '
		<tr '.$this->getRowClass().'>
			<td class="'.$this->getRankChangeClass().'">'
				.$tBodyAnchor
				.$this->userTeamRank.'
			<td class="badge" style="background-image:url('.config::$baseUrl.'/img/content/default-team-badge.png)">'
				.$this->urlGenerator->showLink($this->userTeamName, $this->userTeamId, 'user_team')
				.$tBodyManager.'
			<td class="align-right">'
				.$this->userTeamPoints;
	}
	
	private function setCurrentMonthId()
	{
		$stmt = config::$mysqli->prepare("
		SELECT m.month_id
		FROM tff_months m
		WHERE m.month = MONTH(?) AND m.year = YEAR(?)
                GROUP BY m.month_id
		LIMIT 1");
		$stmt->bind_param("ss", config::$curDate, config::$curDate);
		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows == 0) {
			$stmt->close();
			// if no entry in ldb_month leaderboard for current month then select the latest month_id
			$stmt = config::$mysqli->prepare("SELECT MAX(month_id)
			FROM ldb_month
			LIMIT 1");
			$stmt->execute();
			$stmt->store_result();
		}
		$stmt->bind_result($this->currentMonthId);
		$stmt->fetch();
		$stmt->close();
		return;
	}
	
	private function setCurrentWeekId()
	{
		$stmt = config::$mysqli->prepare("
		SELECT w.week_id
		FROM tff_weeks w
		WHERE w.week_start < ? AND w.week_end > ?
                GROUP BY w.week_id
		LIMIT 1");
		$stmt->bind_param("ss", config::$curDate, config::$curDate);
		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows == 0) {
			$stmt->close();
			// if no entry in ldb_week leaderboard for current week then select the latest week_id
			$stmt = config::$mysqli->prepare("SELECT MAX(week_id)
			FROM ldb_week
			LIMIT 1");
			$stmt->execute();
			$stmt->store_result();
		}
		$stmt->bind_result($this->currentWeekId);
		$stmt->fetch();
		$stmt->close();
		return;
	}
	
	private function getRankChangeClass()
	{
		if ($this->userTeamRankChange < 0)
		{
			return "down";
		}
		elseif ($this->userTeamRankChange > 0)
		{
			return "up";
		}
		else
		{
			return "same";
		}
	}
	
	private function getTopPrivateLeague()
	{
		$stmt = config::$mysqli->prepare("
		SELECT pl.league_id, pl.league_name, (SUM(p.points) DIV x.total_members) AS avg_points
		FROM pl_season p
		LEFT JOIN private_leagues pl
		ON pl.league_id = p.pl_id
		LEFT JOIN
			(SELECT pl_id, COUNT(pl_id) AS total_members
			FROM pl_season
			GROUP BY pl_id) As x
			ON x.pl_id = p.pl_id
		WHERE x.total_members >= 5
		AND p.season_id = ?
		GROUP BY p.pl_id
		ORDER BY avg_points DESC, x.total_members DESC
		LIMIT 1");
		$stmt->bind_param("i", config::$curSeasonId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($pl_id, $pl_name, $pl_avg_points);
		$stmt->fetch();
		$stmt->close();
		
		$this->topPrivateLeagueName = $pl_name;
		$this->topPrivateLeagueId   = $pl_id;
	}
	
	public function showTopPrivateLeague()
	{
		$this->getTopPrivateLeague();
		if ($this->topPrivateLeagueName == NULL || $this->topPrivateLeagueId == NULL)
		{
			echo '<p>No private leagues have been created.</p>';
		}
		else 
		{
			echo '<h4>'.$this->topPrivateLeagueName.'</h4>';
			echo $this->showLeaderboard('short', 'season', '', 'LIMIT 3', $this->topPrivateLeagueId);
			echo '<a href="#" title="#" class="btn blue float-left margin-right">Full League</a>';
			echo '<a href="#" title="#" class="btn dark-grey float-left">All Leagues</a>';
		}
	}
	
}
?>