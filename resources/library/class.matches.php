<?php
class matches
{	
    const NO_MATCHES_ERROR = 'There are currently no %s to display.';

    private $urlGenerator;
    private $userFunctions;
    public $matchesData;
    
    private $compId;
    private $compName;

    private $checkDate        = '';
    private $checkCompetition = '';
    
    public $matchCompId;

    public function __construct($ug, $uf)
    {
        $this->urlGenerator  = $ug;
        $this->userFunctions = $uf;
        
        $this->checkFormSubmission();
    }
	
    private function checkFormSubmission()
    {
        if(filter_has_var(INPUT_POST, 'change_comp'))
        {
            $this->setResultFormFields();
        }
    }
	
    private function setResultFormFields()
    {
        $this->matchCompId     = isset($_POST['competition']) ? $_POST['competition'] : NULL;
        $this->tableLeagueId   = isset($_POST['league']) ? $_POST['league'] : 1;
    }
    
    public function setMatches($matchType = 'all', $compId = NULL, $limit = 20, $teamId = NULL)
    {
        unset($this->matchesData);
        
        $queryArray[]     = $compId !== NULL ? 'c.comp_id = '.$compId.' ' : '';
        $queryArray[]     = $teamId !== NULL ? '(f.ht_id = '.$teamId.' OR f.at_id = '.$teamId.') ' : '';
        $queryArray[]     = $matchType == 'fixtures' ? 'r.fixture_id IS NULL ' : '';
        $cleanQueryArray  = array_filter($queryArray);
        $queryWhere       = count($cleanQueryArray) >= 1 ? 'WHERE ' : '';
        $queryWhereString = $queryWhere.implode('AND ', $cleanQueryArray);
        if($matchType == 'fixtures')
        {
            //exit($queryWhereString);
        }
        $queryOrder       = $matchType == 'fixtures' ? 'ASC' : 'DESC';
        
        $stmt = config::$mysqli->prepare("
        SELECT 
            f.fixture_id,
            ht.team_id, 
            ht.team_name, 
            at.team_id, 
            at.team_name, 
            f.date, 
            c.comp_name,
            c.short_name,
            r.fthg, 
            r.ftag, 
            htp.points, 
            atp.points,
            r.fixture_id
        FROM match_fixtures f
        INNER JOIN tff_seasons s
        ON f.date > s.season_start AND f.date < s.season_end
        INNER JOIN team_names ht
        ON f.ht_id = ht.team_id
        INNER JOIN team_names at
        ON f.at_id = at.team_id
        INNER JOIN competitions c
        ON c.comp_id = f.comp_id
        LEFT JOIN match_results r
        ON r.fixture_id = f.fixture_id
        LEFT JOIN team_points htp
        ON f.ht_id = htp.team_id AND r.fixture_id = htp.fixture_id
        LEFT JOIN team_points atp
        ON f.at_id = atp.team_id AND r.fixture_id = atp.fixture_id
        $queryWhereString 
        GROUP BY f.fixture_id
        ORDER BY f.date $queryOrder, c.comp_id ASC, ht.team_name ASC
        LIMIT $limit");
        $stmt->bind_result(
            $fixtureId,
            $homeTeamId,
            $homeTeamName,
            $awayTeamId,
            $awayTeamName,
            $fixtureDate,
            $fixtureCompetition,
            $fixtureCompetitionShortName,
            $homeTeamGoals,
            $awayTeamGoals,
            $homeTeamPoints,
            $awayTeamPoints,
            $resultFixtureId
        );
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows <> 0)
        {
            while($stmt->fetch())
            {
                $this->matchesData[] = array (
                    'fixture_id'        => $fixtureId,
                    'home_team_id'      => $homeTeamId,
                    'home_team_name'    => $homeTeamName,
                    'away_team_id'      => $awayTeamId,
                    'away_team_name'    => $awayTeamName,
                    'match_date'        => $fixtureDate,
                    'match_competition' => $fixtureCompetition,
                    'match_comp_abbrev' => $fixtureCompetitionShortName,
                    'home_team_goals'   => $homeTeamGoals,
                    'away_team_goals'   => $awayTeamGoals,
                    'home_team_points'  => $homeTeamPoints,
                    'away_team_points'  => $awayTeamPoints,
                    'result_fixture_id' => $resultFixtureId
                );
            }
        }
    }
    
    public function showCompetitionSelectionForm($type = NULL)
    {
        $this->setCompetitions($type);
        
        $formInputName = $type === NULL ? 'competition' : $type;
        
        return '
        <form action="" method="post" class="mini-form">'.
            $this->userFunctions->showStickyForm('select', $formInputName, $this->compName, FALSE, 'Select '.ucwords($formInputName), $this->compId, TRUE).'
            <input type="submit" name="change_comp" value="Go" class="button">
        </form>';
    }
    
    public function displayMatches($matchesToDisplay, $compId = NULL, $limit = 20, $teamId = NULL, $separateDateAndComp = TRUE)
    {	
        $this->setMatches($matchesToDisplay, $compId, $limit, $teamId);
		
        if(!empty($this->matchesData))
        {
            $string = '';

            foreach($this->matchesData as $match)
            {
                $string .= $separateDateAndComp === TRUE ? $this->showFixtureDate($match['match_date']) : '';
                $string .= $separateDateAndComp === TRUE ? $this->showFixtureCompetition($match['match_competition']) : '';
                $string .= $this->buildMatches($match, $separateDateAndComp, $teamId);

                config::updateRowClass();
            }
        }
        else
        {
            return notifications::showNotification('error', FALSE, sprintf(self::NO_MATCHES_ERROR, $matchesToDisplay));
        }

        // check competition needs to be reset to '' after the wile loop incase it is reused as is on the home page by displaying results and then fixtures
        $this->unsetStrings();
        
        return $string;
    }
    
    private function setCompetitions($type)
    {
        $queryWhereString = $type !== NULL ? 'WHERE comp_type = "'.$type.'" ' : '';
        
        $stmt = config::$mysqli->prepare("
        SELECT c.comp_id, c.comp_name
        FROM competitions c
        $queryWhereString
        ORDER BY c.comp_id ASC");
        $stmt->bind_result(
            $compId,
            $compName
        );
        $stmt->execute();
        $stmt->store_result();
        
        unset($this->compId);
        unset($this->compName);
        
        while($stmt->fetch())
        {
            $this->compId[]   = $compId;
            $this->compName[] = $compName;
        }
    }

    private function buildMatches($match, $separateDateAndComp, $teamPageId)
    {
        $focusTeamGoals = !empty($teamPageId) && $teamPageId == $match['home_team_id'] ? $match['home_team_goals'] : $match['away_team_goals'];
        $opponentGoals  = !empty($teamPageId) && $teamPageId == $match['home_team_id'] ? $match['away_team_goals'] : $match['home_team_goals'];
        $resultClass    = $focusTeamGoals > $opponentGoals ? ' match-won' : ($focusTeamGoals < $opponentGoals ? ' match-lost' : '');
        
        $string = '<div class="match table-row'.config::$rowClass.'">';
        $string .= $separateDateAndComp != TRUE ? $this->getDateColumn($match['match_date']) : '';
        $string .= $separateDateAndComp != TRUE ? $this->getCompColumn($match['match_comp_abbrev']) : '';
        $string .= $this->getPointsColumn($match['home_team_points']);
        $string .= $this->getTeamColumn($match['home_team_name'], $match['home_team_id'], $teamPageId, 'align-right');
        $string .= $this->getClubBadgeColumn($match['home_team_id']);
        $string .= '<a class="result'.$resultClass.'" href="'.$this->urlGenerator->makeUrl($match['home_team_name'].'-'.$match['away_team_name'], $match['fixture_id'], 'match', $match['match_date']).'">';
        $string .= $this->getGoalsColumn($match['home_team_goals'], $match['away_team_goals']);
        $string .= $this->getGoalsColumn($match['away_team_goals'], $match['home_team_goals']);
        $string .= '</a>';
        $string .= $this->getClubBadgeColumn($match['away_team_id']);
        $string .= $this->getTeamColumn($match['away_team_name'], $match['away_team_id'], $teamPageId);
        $string .= $this->getPointsColumn($match['away_team_points'], 'align-right');
        $string .= '</div>';
        
        return $string;
    }
    
    private function getPointsColumn($points, $class = '')
    {
        return '<span class="points '.$class.'">'.$points.'</span>';
    }
    
    private function getTeamColumn($teamName, $teamId, $teamPageId, $class = '')
    {
        $focusTeamClass = $teamId == $teamPageId ? ' team-focus' : '';
        return '<span class="team '.$class.$focusTeamClass.'">'.$this->urlGenerator->showLink($teamName, $teamId, 'team').'</span>';
    }
    
    private function getClubBadgeColumn($teamId)
    {
        return '<span class="club-badge" style="background-image:url('.config::$baseUrl.'/img/content/team-badges/small/'.$teamId.'.png);">&nbsp;</span>';
    }
    
    private function getDateColumn($date)
    {
        $matchDate = DateTime::createFromFormat('Y-m-d', $date)->format('d/m/y');
        return '<span class="inline-date">'.$matchDate.'</span>';
    }
    
    private function getCompColumn($comp)
    {
        return '<span class="inline-comp">'.$comp.'</span>';
    }
    
    private function getGoalsColumn($teamGoals, $opponentGoals)
    {
        $goalsClass = $teamGoals > $opponentGoals ? 'win ' : ($teamGoals < $opponentGoals ? 'lose ' : '');
        $goals = $teamGoals === NULL ? '-' : $teamGoals;
        
        return '<span class="score '.$goalsClass.'align-center">'.$goals.'</span>';
    }
	
    private function showFixtureDate($matchDate)
    {
        
        date_default_timezone_set('Europe/London');
		//$timezone      = new DateTimeZone('Europe/London'); 
		//$date          = new DateTime(null, $timezone);
        if($this->checkDate != date("Y-m-d", strtotime($matchDate)))
        {
            // checkCompetition needs unsetting here incase the next fixture is the same competition but on a different date
            $this->unsetStrings();
            $string = '<span class="date">'.$this->getFixtureDate($matchDate).'</span>';

            $this->checkDate = date("Y-m-d", strtotime($matchDate));
            return $string;
        }
    }

    private function getFixtureDate($matchDate)
    {
        return date("l j F Y", strtotime($matchDate));
    }

    private function showFixtureCompetition($matchCompetition)
    {
        if($this->checkCompetition != $matchCompetition)
        {
            $string = '<span class="competition">'.$matchCompetition.'</span>';

            $this->checkCompetition = $matchCompetition;
            return $string;
        }
    }

    private function unsetStrings()
    {
        $this->checkCompetition = '';
        $this->checkDate        = '';
    }
}
	