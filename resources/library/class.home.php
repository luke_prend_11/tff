<?php
class home
{
    private $urlGenerator;
    private $leaderboards;
    private $matches;
    private $competitions;
    private $news;
    
    private $matchCompId   = NULL;
    private $tableLeagueId = 1;
    private $tableLeagueName;
    
    public function __construct($ug, $uf)
    {
        $this->urlGenerator = $ug;
        $this->leaderboards = new leaderboards($this->urlGenerator);
        $this->matches      = new matches($this->urlGenerator, $uf);
        $this->competitions = new stats_competitions($this->urlGenerator, $uf);
        $this->news         = config::NEWS_ACTIVE === TRUE ? new news($this->urlGenerator) : '';
        
        $this->checkFormSubmission();
        $this->setLeagueName();
    }
	
    private function checkFormSubmission()
    {
        if(filter_has_var(INPUT_POST, 'change_comp'))
        {
            $this->setResultFormFields();
        }
    }
	
    private function setResultFormFields()
    {
        $this->matchCompId     = isset($_POST['competition']) ? $_POST['competition'] : NULL;
        $this->tableLeagueId   = isset($_POST['league']) ? $_POST['league'] : 1;
    }
    
    private function setLeagueName()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            c.comp_name
        FROM competitions c
        WHERE c.comp_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $this->tableLeagueId);
        $stmt->bind_result($this->tableLeagueName);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
    }
	
    public function displayHomePage()
    {
        echo '
        <div class="row stretch-parent">
            <div class="col-2-x-3 very-very-light-grey stretch-child">';
        
            if(config::NEWS_ACTIVE === TRUE)
            {
                echo '
                <h2 class="news dark-grey">Latest News</h2>
                <div class="inner-container">'.
                    $this->news->displayHomePageNews().'
                    <a href="news/" title="All News" class="btn grey news">All News</a>
                </div>';
            }
            else
            {
                echo '
                <h2 class="news dark-grey">Matches</h2>'.
                $this->matches->showCompetitionSelectionForm().'
                <div class="inner-container">'.
                    $this->matches->displayMatches('matches', $this->matchCompId, 40).'
                </div>';
            }
            
            echo '
            </div>

            <div class="col-1-x-3 stretch-child">';
            
            if(config::FANTASY_FOOTBALL_ACTIVE === TRUE)
            {
                echo '
                <div class="very-light-grey">
                    <h2 class="leaderboard orange">Leaderboard</h2>
                    <div class="inner-container">'.
                        $this->leaderboards->showLeaderboard('short', 'season', '', 'LIMIT 17').'
                        <a href="leaderboards/index.php?ldb=season" title="Full Leaderboard" class="btn grey">Full Leaderboard</a>
                    </div>
                </div>';
            }

            echo '
                <div class="very-light-grey">
                    <h2 class="results blue">League Tables</h2>'.
                    $this->matches->showCompetitionSelectionForm('league').'
                    <div class="inner-container">'.
                        $this->competitions->showLeagueTable('short', 'season', NULL, $this->tableLeagueId).'
                        <a href="'.$this->urlGenerator->makeUrl($this->tableLeagueName, $this->tableLeagueId, 'league_table').'" title="Full '.$this->tableLeagueName.' Table" class="btn grey">Full Table</a>
                    </div>
                </div>

                <div class="very-light-grey">
                    <h2 class="fixtures dark-grey">Fixtures</h2>'.
                    $this->matches->showCompetitionSelectionForm().'
                    <div class="inner-container">'.
                        $this->matches->displayMatches('fixtures', $this->matchCompId).'
                    </div>
                </div>
            </div>
            <span class="stretch"></span>
        </div>';
    }
}