<?php
class admin_results extends admin_data
{
    const POINTS_FOR_WIN         = 30;
    const POINTS_FOR_DRAW        = 10;
    const POINTS_FOR_LOSS        = 0;
    const POINTS_FOR_GOAL_SCORED = 5;
    const POINTS_FOR_CLEAN_SHEET = 5;
    const INSERT_RESULTS_TEXT    = 'Insert Results';
    const BROWSE_RESULTS_TEXT    = 'Browse Results to Update';
    const SELECT_RESULTS_TEXT    = 'Select Result to Update';

    private $pageId;

    private $fixtureId;
    private $homeTeamId;
    private $awayTeamId;
    private $homeTeamGoals;
    private $awayTeamGoals;
    private $extraTimePlayed;
    private $homeTeamWinOnPenalties;
    private $awayTeamWinOnPenalties;
    private $matchPostponed;
    private $penaltyWinnersTeamId;
    private $formErrors;
    
    private $urlGenerator;
    private $userFunctions;
    
    private $fixturesWithoutResults;

    public $resultsArray     = array();
    private $teamPointsArray = array();
	
    public function __construct($ug, $uf, $db)
    {
        parent::__construct($db);
        
        $this->urlGenerator = $ug;
        $this->userFunctions = $uf;
        $this->setPageId();
    }

    private function setPageId()
    {
        if(filter_has_var(INPUT_GET, 'id'))
        {
            //$_SESSION['res_page_id'] = htmlspecialchars($_GET['id']);
            $this->pageId = htmlspecialchars($_GET['id']);
        }
    }
	
    public function displayAdminResultsPage()
    {
        $this->checkFormSubmission();
        $this->displayPageButtons();
        $this->setPageDisplay();		
    }

    private function setPageDisplay()
    {
        switch($this->pageId)
        {
            case 1:
                echo '<h2>'.self::INSERT_RESULTS_TEXT.'</h2>';
                $this->insertResults();
            break;
            case 2:
                echo '<h2>'.self::BROWSE_RESULTS_TEXT.'</h2>';
                //$this->pageFormToDisplay = browse results function
            break;
            case 3:
                echo '<h2>'.self::SELECT_RESULTS_TEXT.'</h2>';
                $this->selectFixtureComp('results');
            break;
        }
    }
	
    private function displayPageButtons()
    {
        echo '
        <a href="results.php?id=1" title="'.self::INSERT_RESULTS_TEXT.'" class="btn blue">'.self::INSERT_RESULTS_TEXT.'</a>
        <a href="results.php?id=2" title="'.self::BROWSE_RESULTS_TEXT.'" class="btn dark-grey">'.self::BROWSE_RESULTS_TEXT.'</a>
        <a href="results.php?id=3" title="'.self::SELECT_RESULTS_TEXT.'" class="btn orange">'.self::SELECT_RESULTS_TEXT.'</a>';
    }
	
    private function checkFormSubmission()
    {
        if(filter_has_var(INPUT_POST, 'submit_results'))
        {
            $this->setResultFormFields();
            $this->checkResultFormFields();
            
            if(empty($this->formErrors))
            {
                $this->insertResults();
            }
            else
            {
                echo notifications::showNotification('error', TRUE, $this->formErrors);
            }
        }
    }
	
    private function setResultFormFields()
    {
        $this->fixtureId              = isset($_POST['fixture_id']) ? $_POST['fixture_id'] : '';
        $this->homeTeamId             = isset($_POST['home_id']) ? $_POST['home_id'] : '';
        $this->awayTeamId             = isset($_POST['away_id']) ? $_POST['away_id'] : '';
        $this->homeTeamGoals          = isset($_POST['home_score']) ? $_POST['home_score'] : '';
        $this->awayTeamGoals          = isset($_POST['away_score']) ? $_POST['away_score'] : '';
        $this->extraTimePlayed        = isset($_POST['aet']) ? $_POST['aet'] : '';
        $this->homeTeamWinOnPenalties = isset($_POST['home_pens']) ? $_POST['home_pens'] : '';
        $this->awayTeamWinOnPenalties = isset($_POST['away_pens']) ? $_POST['away_pens'] : '';
        $this->matchPostponed         = isset($_POST['postponed']) ? $_POST['postponed'] : '';
    }
	
    private function checkResultFormFields()
    {
        foreach(array_keys($this->fixtureId) as $n)
        {
            //Skip any rows that do not contain results
            if($this->checkForResultEntry($n))
            {
                $this->validateResultEntries($n);
            }
        }
    }
    
    private function checkForResultEntry($n)
    {
        if($this->homeTeamGoals[$n] != '' || $this->awayTeamGoals[$n] != '' || !empty($this->matchPostponed[$n]))
        {
            return true;
        }
        return false;
    }
    
    private function validateResultEntries($n)
    {
        //Ensure nothing else was entered if match postponed has been selected 
        if(!empty($this->matchPostponed[$n]) && 
            ($this->homeTeamGoals[$n] != '' || $this->awayTeamGoals[$n] != '' || 
            !empty($this->extraTimePlayed[$n]) || !empty($this->homeTeamWinOnPenalties[$n]) || !empty($this->awayTeamWinOnPenalties[$n])))
        {
            $this->formErrors[] = "Postponed games should not have any other entries";
        }
        //Check scores are not empty if match is not postponed
        if(($this->homeTeamGoals[$n] == '' || $this->awayTeamGoals[$n] == '') && empty($this->matchPostponed[$n]))
        {
            $this->formErrors[] = "Enter scores for both teams";
        }
        //Check numbers have been entered as scores
        if((!is_numeric($this->homeTeamGoals[$n]) || !is_numeric($this->awayTeamGoals[$n])) && empty($this->matchPostponed[$n]))
        {
            $this->formErrors[] = "Only numbers can be entered for scores";
        }
        //Check that both pens boxes haven't been selected
        if(!empty($this->homeTeamWinOnPenalties[$n]) && !empty($this->awayTeamWinOnPenalties[$n]))
        {
            $this->formErrors[] = "Two teams cannot win on penalties";
        }
        //Check that pens and aet havn't been selected
        if((!empty($this->homeTeamWinOnPenalties[$n]) || !empty($this->awayTeamWinOnPenalties[$n])) && !empty($this->extraTimePlayed[$n]))
        {
            $this->formErrors[] = "Extra time and pens selected";
        }
    }

    private function insertResults()
    {
        if(filter_has_var(INPUT_POST, 'submit_results') && !empty($this->formErrors))
        {
            echo notifications::showNotification('error', TRUE, $this->formErrors);
            $this->showEnterResults();
        }
        elseif(filter_has_var(INPUT_POST, 'submit_results') && empty($this->formErrors))
        {
            // no errors have been detected so process the results
            if($this->processResults())
            {
                echo notifications::showNotification('success', TRUE, 'Results have been entered');
                $this->showEnterResults();
            }
            else 
            {
                // display any errors that have occured
                echo notifications::showNotification('error', TRUE, 'You have not submitted any results or there has been an error inserting data to the database. Please try again.');
                $this->showEnterResults();
            }
        }
        else
        {
            // submit button has not been pressed 
            $this->showEnterResults(); 
        }
    }
	
    private function showEnterResults()
    {
        if($this->getFixturesWithNoResults())
        {
            $this->enterResultsForm();
        }
        else
        {
            echo notifications::showNotification('success', TRUE, 'There are no results to enter');
        }
        
    }
    
    private function getFixturesWithNoResults()
    {
        $stmt = config::$mysqli->prepare("
        SELECT h.team_id, h.team_name, a.team_id, a.team_name, f.fixture_id, f.date, c.short_name
        FROM match_fixtures f
        LEFT JOIN team_names h
        ON f.ht_id = h.team_id
        LEFT JOIN team_names a
        ON f.at_id = a.team_id
        LEFT JOIN match_results r
        ON f.fixture_id = r.fixture_id
        LEFT JOIN competitions c
        ON f.comp_id = c.comp_id
        LEFT JOIN tff_seasons s
        ON f.date > s.season_start AND f.date < s.season_end
        WHERE r.fixture_id IS NULL 
        AND f.date <= NOW()
        GROUP BY f.fixture_id
        ORDER BY DATE(f.date) ASC, c.comp_id ASC, f.date ASC, h.team_name ASC
        ");
        $stmt->execute();
        $stmt->store_result();
        
        if($stmt->num_rows == 0)
        {
            return false;
        }
        $this->fixturesWithoutResults = $stmt;
        return true;
    }
    
    private function enterResultsForm()
    {
        $this->fixturesWithoutResults->bind_result($home_id, $home_team, $away_id, $away_team, $fixture_id, $date, $short_name);
        
        echo '
        <form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post">
            <table>
                <thead>
                <tr>
                    <th><span>Date
                    <th><span>Comp
                    <th class="align-right"><span>Home Team
                    <th><span>P
                    <th>&nbsp;
                    <th>&nbsp;
                    <th>&nbsp;
                    <th><span>P
                    <th><span>AET
                    <th><span>Away Team
                    <th><span>Postponed
                <tbody>';
        
        $i = 0;
        while($this->fixturesWithoutResults->fetch())
        {
            echo '
            <tr class="table-row'.config::$rowClass.'">
                <input name="fixture_id['.$i.']" value="'.$fixture_id.'" type="hidden" />
                <input name="home_id['.$i.']" value="'.$home_id.'" type="hidden" />
                <input name="home_pens['.$i.']" value="0" type="hidden" />
                <input name="away_id['.$i.']" value="'.$away_id.'" type="hidden" />
                <input name="away_pens['.$i.']" value="0" type="hidden" />
                <input name="aet['.$i.']" value="0" type="hidden" />
                <input name="postponed['.$i.']" value="0" type="hidden" />

                <td>'.date("d/m/y H:i", strtotime($date)).'
                <td>'.$short_name.'
                <td class="align-right">'.$this->urlGenerator->showLink($home_team, $home_id, 'team').'
                <td>'.$this->userFunctions->stickyFormField('checkbox', 'home_pens['.$i.']', $home_id).'
                <td>'.$this->userFunctions->stickyFormField('text', 'home_score['.$i.']', '', FALSE, '', 2).'
                <td class="align-center">-
                <td>'.$this->userFunctions->stickyFormField('text', 'away_score['.$i.']', '', FALSE, '', 2).'
                <td>'.$this->userFunctions->stickyFormField('checkbox', 'away_pens['.$i.']', $away_id).'
                <td>'.$this->userFunctions->stickyFormField('checkbox', 'aet['.$i.']', 1).'
                <td>'.$this->urlGenerator->showLink($away_team, $away_id, 'team').'
                <td>'.$this->userFunctions->stickyFormField('checkbox', 'postponed['.$i.']', 1);
            $i++;			
        }
        echo '
            </table>
            <input name="submit_results" type="submit" value="Submit" />
        </form>';
    }
	
    private function processResults()
    {
        $this->storeResults();
        if(empty($this->resultsArray) || (config::FANTASY_FOOTBALL_ACTIVE === TRUE && empty($this->teamPointsArray)))
        {
            return false;
        }
        else
        {                
            // insert match results
            $this->insertMatchResults();
                
            // insert team points
            $this->insertIndividualTeamPoints();
            
            // insert league tables
            $this->createLeagueTables();
            
            if(config::FANTASY_FOOTBALL_ACTIVE === TRUE)
            {
                // create leaderboards
                $this->createLeaderboards();
            }
            return true;		
        }
    }
	
    private function storeResults()
    {
        foreach(array_keys($this->fixtureId) as $n)
        {
            // for each fixture that contains a result add to results array
            if(($this->homeTeamGoals[$n] != '' && $this->awayTeamGoals[$n] != '') || (!empty($this->matchPostponed[$n])))
            {
                // save team id of team that won on pens
                if(!empty($this->homeTeamWinOnPenalties[$n]) || !empty($this->awayTeamWinOnPenalties[$n]))
                {
                    $this->penaltyWinnersTeamId = !empty($this->homeTeamWinOnPenalties[$n]) ? $this->homeTeamWinOnPenalties[$n] : $this->awayTeamWinOnPenalties[$n];
                }

                $this->resultsArray[] = $this->setFieldNames('manual', $n);
            }
        }
    }
    
    public function setFieldNames($resultType = 'import', $key = '', $array = '')
    {
        return array(
            "fixtureId" => $resultType == 'manual' ? $this->fixtureId[$key] : $array['fixtureId'], 
            "fthg"      => $resultType == 'manual' ? $this->homeTeamGoals[$key] : $array[4], 
            "ftag"      => $resultType == 'manual' ? $this->awayTeamGoals[$key] : $array[5], 
            "extraTime" => $resultType == 'manual' ? $this->extraTimePlayed[$key] : '', 
            "pens"      => $resultType == 'manual' ? $this->penaltyWinnersTeamId : '', 
            "postponed" => $resultType == 'manual' ? $this->matchPostponed[$key] : '',
            "date"      => $resultType == 'manual' ? '' : $array[1], 
            "htName"    => $resultType == 'manual' ? '' : $array[2], 
            "atName"    => $resultType == 'manual' ? '' : $array[3],  
            "hthg"      => $resultType == 'manual' ? '' : $array[7], 
            "htag"      => $resultType == 'manual' ? '' : $array[8], 
            "hsog"      => $resultType == 'manual' ? '' : $array[11], 
            "asog"      => $resultType == 'manual' ? '' : $array[12], 
            "hsot"      => $resultType == 'manual' ? '' : $array[13], 
            "asot"      => $resultType == 'manual' ? '' : $array[14], 
            "hfk"       => $resultType == 'manual' ? '' : $array[15], 
            "afk"       => $resultType == 'manual' ? '' : $array[16], 
            "hc"        => $resultType == 'manual' ? '' : $array[17], 
            "ac"        => $resultType == 'manual' ? '' : $array[18], 
            "hyc"       => $resultType == 'manual' ? '' : $array[19], 
            "ayc"       => $resultType == 'manual' ? '' : $array[20], 
            "hrc"       => $resultType == 'manual' ? '' : $array[21], 
            "arc"       => $resultType == 'manual' ? '' : $array[22]
        );
    }
    
    public function insertMatchResults()
    {
        if(isset($_SESSION['incorrectResultsImportData']))
        {
            $stmt = config::$mysqli->prepare("UPDATE match_results SET fixture_id = ?, fthg = ?, ftag = ?, hthg = ?, htag = ?, hsog = ?, asog = ?, hsot = ?, asot = ?, hfk = ?, afk = ?, hc = ?, ac = ?, hyc = ?, ayc = ?, hrc = ?, arc = ? WHERE fixture_id = ? LIMIT 1");
        }
        else
        {
            $stmt = config::$mysqli->prepare("INSERT INTO match_results (fixture_id, fthg, ftag, hthg, htag, hsog, asog, hsot, asot, hfk, afk, hc, ac, hyc, ayc, hrc, arc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        }
                  
        foreach($this->resultsArray as $values)
        {
            $fixtureId = isset($values["fixtureId"]) ? $values["fixtureId"] : '';
            $fthg      = isset($values["fthg"]) ? $values["fthg"] : NULL;
            $ftag      = isset($values["ftag"]) ? $values["ftag"] : NULL;
            $hthg      = isset($values["hthg"]) ? $values["hthg"] : '';
            $htag      = isset($values["htag"]) ? $values["htag"] : '';
            $hsog      = isset($values["hsog"]) ? $values["hsog"] : '';
            $asog      = isset($values["asog"]) ? $values["asog"] : '';
            $hsot      = isset($values["hsot"]) ? $values["hsot"] : '';
            $asot      = isset($values["asot"]) ? $values["asot"] : '';
            $hfk       = isset($values["hfk"]) ? $values["hfk"] : '';
            $afk       = isset($values["afk"]) ? $values["afk"] : '';
            $hc        = isset($values["hc"]) ? $values["hc"] : '';
            $ac        = isset($values["ac"]) ? $values["ac"] : '';
            $hyc       = isset($values["hyc"]) ? $values["hyc"] : '';
            $ayc       = isset($values["ayc"]) ? $values["ayc"] : '';
            $hrc       = isset($values["hrc"]) ? $values["hrc"] : '';
            $arc       = isset($values["arc"]) ? $values["arc"] : '';
            
            $extraTime = isset($values["extraTime"]) ? $values["extraTime"] : '';
            $penalties = isset($values["pens"]) ? $values["pens"] : '';
            $postponed = isset($values["postponed"]) ? $values["postponed"] : '';
            
            if(isset($_SESSION['incorrectResultsImportData']))
            {
                $stmt->bind_param("iiiiiiiiiiiiiiiiii", $fixtureId, $fthg, $ftag, $hthg, $htag, $hsog, $asog, $hsot, $asot, $hfk, $afk, $hc, $ac, $hyc, $ayc, $hrc, $arc, $fixtureId);
            }
            else
            {
                $stmt->bind_param("iiiiiiiiiiiiiiiii", $fixtureId, $fthg, $ftag, $hthg, $htag, $hsog, $asog, $hsot, $asot, $hfk, $afk, $hc, $ac, $hyc, $ayc, $hrc, $arc);
            }

            if(!$stmt->execute())
            {
                printf("Errormessage: %s\n", config::$mysqli->error);
            }
            
            if(!empty($extraTime))
            {
                $this->insertMatchExtraTime($fixtureId);
            }
            
            if($penalties != '')
            {
                $this->insertMatchPenalties($fixtureId, $penalties);
            }
            
            if(!empty($postponed))
            {
                $this->insertMatchPostponed($fixtureId);
            }
        }
        $stmt->close();
    }
    
    private function insertMatchExtraTime($fixtureId)
    {
        if($this->database->completeInsert('match_results_et', 'fixture_id', $fixtureId, 'i'))
        {
            return;
        }
    }
    
    private function insertMatchPenalties($fixtureId, $penaltyWinners)
    {
        if($this->database->completeInsert('match_results_pens', array('fixture_id', 'pens'), array($fixtureId, $penaltyWinners), array('i','i')))
        {
            return;
        }
    }
    
    private function insertMatchPostponed($fixtureId)
    {
        if($this->database->completeInsert('match_results_pp', 'fixture_id', $fixtureId, 'i'))
        {
            return;
        }
    }
    
    public function insertIndividualTeamPoints()
    {
        /*
         * Needs fixing
         * first look to see if already enter into team_points as some were imported from original database
         * then check to see which league each team was from when fixture took place - for league diff calculation
         * 
         * also need to include update script if incorrectRsultsImportData is set
         * 
         * 
         */
        
        $this->resultsArray = isset($_SESSION['insertPointsData']) ? array_merge($this->resultsArray, $_SESSION['insertPointsData']) : $this->resultsArray;
        
        foreach($this->resultsArray as $result)
        {
            if(empty($result['postponed']))
            {
                // do points calc function if match wasn't postponed
                $this->setTeamPoints($result['fixtureId'], $result['fthg'], $result['ftag'], $result['pens']);
            }
        }
        
        $this->insertTeamPoints();
    }
    
    private function setTeamPoints($fixtureId, $homeTeamGoals, $awayTeamGoals, $penaltyWinnersTeamId = 0)
    {
        list($homeTeamId, $homeTeamPoints, $awayTeamId, $awayTeamPoints) = $this->pointsCalc($fixtureId, $homeTeamGoals, $awayTeamGoals, $penaltyWinnersTeamId);
        $this->teamPointsArray[] = array($fixtureId, $homeTeamId, $homeTeamPoints);
        $this->teamPointsArray[] = array($fixtureId, $awayTeamId, $awayTeamPoints);
    }
    
    private function insertTeamPoints()
    {
        $stmt = config::$mysqli->prepare("INSERT INTO team_points (fixture_id, team_id, points)
        VALUES (?, ?, ?)");

        foreach($this->teamPointsArray as $values) 
        {
            $fixtureId  = $values[0];
            $teamId     = $values[1];
            $teamPoints = $values[2];
            $stmt->bind_param("iii", $fixtureId, $teamId, $teamPoints);
            
            if(!$stmt->execute())
            {
                printf("Errormessage: %s\n", config::$mysqli->error);
            }
        }
        $stmt->close();
    }
    
    public function createLeaderboards()
    {
        /* the following lines of code will first create the overall leaderbaord for the current season
        and then either insert the leaderboard into the ldb_overall table if it is the first instance for the current season
        or update each row with the correct details based on the user_team_id and the season_id */

        // declare statement that will create the overall leaderboard with the relevant fields
        $stmt = config::$mysqli->prepare("
        SELECT overall.user_team_id, overall.user_team_name, overall.date, overall.total, COALESCE(overall.previous_total, 0) AS previous_total, COALESCE(overall.previous_rank, 0) AS previous_rank, (@row := @row + 1) AS rank
        FROM (
            SELECT ut.user_team_id, ut.user_team_name, ut.date, COALESCE(SUM(CASE WHEN user_team_selections.transfer_in < match_fixtures.date AND user_team_selections.transfer_out > match_fixtures.date AND match_fixtures.date >= ? AND match_fixtures.date <= ? THEN team_points.points ELSE 0 END),0) as total, ldb.points AS previous_total, ldb.rank AS previous_rank
            FROM user_teams ut
            LEFT JOIN ldb_season ldb
            ON ldb.user_team_id = ut.user_team_id
            LEFT JOIN user_team_selections
            ON ut.user_team_id = user_team_selections.user_team_id
            LEFT JOIN team_points
            ON user_team_selections.team_id = team_points.team_id
            LEFT JOIN match_fixtures
            ON team_points.fixture_id = match_fixtures.fixture_id
            WHERE ut.date >= ?
            GROUP BY ut.user_team_id
            ORDER BY total DESC, ut.user_team_name ASC
        ) overall, (SELECT @row := 0) r
        WHERE overall.date >= ?
        ORDER BY rank ASC, overall.user_team_name ASC");
        $stmt->bind_param("ssss", config::$curSeasonStart, config::$curSeasonEnd, config::$curSeasonStart, config::$curSeasonStart);
							
			// declare statements to insert and update ldv_overall table
			$stmt2 = config::$mysqli->prepare("INSERT INTO ldb_season (user_team_id, rank, points, previous_rank, previous_points, season_id) 
			VALUES (?, ?, ?, ?, ?, ?)");
			$stmt2->bind_param("iiiiii", $user_team_id, $rank, $total, $previous_rank, $previous_total, config::$curSeasonId);
			
			$stmt3 = config::$mysqli->prepare("UPDATE ldb_season 
			SET rank = ?, points = ?, previous_rank = ?, previous_points = ? 
			WHERE user_team_id = ? AND season_id = ? 
			LIMIT 1");
			$stmt3->bind_param("iiiiii", $rank, $total, $previous_rank, $previous_total, $user_team_id, config::$curSeasonId);
			
			// declare statement that will check if team has already been entered for current season
			$stmt4 = config::$mysqli->prepare("SELECT user_team_id
			FROM ldb_season
			WHERE user_team_id = ?
			AND season_id = ?
			LIMIT 1");
			$stmt4->bind_param("ii", $user_team_id, config::$curSeasonId);
			
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($user_team_id, $user_team_name, $date, $total, $previous_total, $previous_rank, $rank);
			while($stmt->fetch()) {
				$stmt4->execute();
				$stmt4->store_result();
				if ($stmt4->num_rows == 0) {
					$stmt2->execute();
				}
				else {
					$stmt3->execute();
				}
			}		
			$stmt->close();
			$stmt2->close();
			$stmt3->close();
			$stmt4->close();
			
			// update table_updates table
			$db = new database();
			$db->insertUpdateTime('ldb_season');
			
			/* the following lines of code will first select the months for which results have been entered
			then use a select statement that will create the monthly leaderbaord
			then loop through each month where there has been a result entered
			and then either insert the leaderboard into the ldb_month table if it is the first instance for the specific month
			or update each row with the correct details based on the user_team_id and the month_id */
                        
			// get only fixture IDs from $resultsArray - fixtureId if coming from admin_import_results else column 0	
                        $getFixIds = array_key_exists('fixtureId', $this->resultsArray[0]) ? array_column($this->resultsArray, 'fixtureId') : array_column($this->resultsArray, 0);
                        
			// store fixture IDs in the format 1,2,3 for use in SELECT statement			
			$fix_IDs = implode(",", $getFixIds);
			
			// select every month and year where results have been entered using fixture_id's
			$stmt = config::$mysqli->prepare("SELECT MONTH(f.date) AS month, YEAR(f.date) AS year, s.season_id, m.month_id
			FROM match_fixtures f
			LEFT JOIN tff_seasons s
			ON s.season_start < f.date AND s.season_end > f.date
			LEFT JOIN tff_months m
			ON m.month = MONTH(f.date) AND m.year = YEAR(f.date)
			WHERE f.fixture_id IN ($fix_IDs)
			GROUP BY year, month
			ORDER BY year ASC, month ASC");
			
			// declare statement that will create the month leaderboard with the relevant fields
			$stmt2 = config::$mysqli->prepare("SELECT month.user_team_id, month.user_team_name, month.date, month.total, COALESCE(month.previous_total, 0) AS previous_total, COALESCE(month.previous_rank, 0) AS previous_rank, (@row := @row + 1) AS rank
			FROM (
				SELECT ut.user_team_id, ut.user_team_name, ut.date, COALESCE(SUM(CASE WHEN ts.transfer_in < f.date AND ts.transfer_out > f.date AND MONTH(f.date) = ? AND YEAR(f.date) = ? THEN tp.points ELSE 0 END),0) as total, ldb.points AS previous_total, ldb.rank AS previous_rank
				FROM user_teams ut
				LEFT JOIN ldb_month ldb
				ON ldb.user_team_id = ut.user_team_id AND ldb.month_id = ?
				LEFT JOIN user_team_selections ts
				ON ut.user_team_id = ts.user_team_id
				LEFT JOIN team_points tp
				ON ts.team_id = tp.team_id
				LEFT JOIN match_fixtures f
				ON tp.fixture_id = f.fixture_id
				LEFT JOIN tff_seasons s
				ON f.date > s.season_start AND f.date < s.season_end
				WHERE ut.season_id = ?
					AND ((Month(ut.date) <= ? AND Year(ut.date) = ?)
					OR (Month(ut.date) > ? AND Year(ut.date) < ?))
				GROUP BY ut.user_team_id
				ORDER BY total DESC, ut.user_team_name ASC
			) month, (SELECT @row := 0) r
			ORDER BY rank ASC, month.user_team_name ASC");
			$stmt2->bind_param("ssiissss", $month, $year, $month_id, $s_id, $month, $year, $month, $year);
			
			// declare statements to insert and update ldb_month table
			$stmt3 = config::$mysqli->prepare("INSERT INTO ldb_month (user_team_id, rank, points, previous_rank, previous_points, month_id) VALUES (?, ?, ?, ?, ?, ?)");
			$stmt3->bind_param("iiiiii", $user_team_id, $rank, $total, $previous_rank, $previous_total, $month_id);
			
			$stmt4 = config::$mysqli->prepare("UPDATE ldb_month SET rank = ?, points = ?, previous_rank = ?, previous_points = ? WHERE user_team_id = ? AND month_id = ? LIMIT 1");
			$stmt4->bind_param("iiiiii", $rank, $total, $previous_rank, $previous_total, $user_team_id, $month_id);
			
			// declare statement that will check if team has already been entered for current month
			$stmt5 = config::$mysqli->prepare("SELECT user_team_id
			FROM ldb_month
			WHERE user_team_id = ?
			AND month_id = ?
			LIMIT 1");
			$stmt5->bind_param("ii", $user_team_id, $month_id);
			
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($month, $year, $s_id, $month_id);
			
			while($stmt->fetch()) {
				$stmt2->execute();
				$stmt2->store_result();
				$stmt2->bind_result($user_team_id, $user_team_name, $date, $total, $previous_total, $previous_rank, $rank);
				while($stmt2->fetch()) {
					// insert or update each months leaderboard
					$stmt5->execute();
					$stmt5->store_result();
					if ($stmt5->num_rows == 0) {
						$stmt3->execute();
					}
					else {
						$stmt4->execute();
					}
				}
			}
			$stmt->close();
			$stmt2->close();
			$stmt3->close();
			$stmt4->close();
			$stmt5->close();
			
			// update table_updates table
			$db->insertUpdateTime('ldb_month');
			
			
			
			/* the following lines of code will first select the weeks for which results have been entered
			then use a select statement that will create the weekly leaderbaord
			then loop through each week where there has been a result entered
			and then either insert the leaderboard into the ldb_week table if it is the first instance for the specific week
			or update each row with the correct details based on the user_team_id and the week_id */
			
			// select every week where results have been entered using fixture_id's
			$stmt = config::$mysqli->prepare("SELECT s.season_id, w.week_id, w.week_start, w.week_end
			FROM match_fixtures f
			LEFT JOIN tff_seasons s
			ON s.season_start < f.date AND s.season_end > f.date
			LEFT JOIN tff_weeks w
			ON w.week_start < f.date AND w.week_end > f.date
			WHERE f.fixture_id IN ($fix_IDs)
			GROUP BY w.week_id
			ORDER BY w.week_end ASC");
			
			// declare statement that will create the week leaderboard with the relevant fields
			$stmt2 = config::$mysqli->prepare("SELECT week.user_team_id, week.user_team_name, week.date, week.total, COALESCE(week.previous_total, 0) AS previous_total, COALESCE(week.previous_rank, 0) AS previous_rank, (@row := @row + 1) AS rank
			FROM (
				SELECT ut.user_team_id, ut.user_team_name, ut.date, COALESCE(SUM(CASE WHEN ts.transfer_in < f.date AND ts.transfer_out > f.date AND f.date > ? AND f.date < ? THEN tp.points ELSE 0 END),0) as total, ldb.points AS previous_total, ldb.rank AS previous_rank
				FROM user_teams ut
				LEFT JOIN ldb_week ldb
				ON ldb.user_team_id = ut.user_team_id AND ldb.week_id = ?
				LEFT JOIN user_team_selections ts
				ON ut.user_team_id = ts.user_team_id
				LEFT JOIN team_points tp
				ON ts.team_id = tp.team_id
				LEFT JOIN match_fixtures f
				ON tp.fixture_id = f.fixture_id
				LEFT JOIN tff_seasons s
				ON f.date > s.season_start AND f.date < s.season_end
				WHERE ut.season_id = ?
					AND ut.date < ?
				GROUP BY ut.user_team_id
				ORDER BY total DESC, ut.user_team_name ASC
			) week, (SELECT @row := 0) r
			ORDER BY rank ASC, week.user_team_name ASC");
			$stmt2->bind_param("ssiis", $week_start, $week_end, $week_id, $s_id, $week_end);
			
			// declare statements to insert and update ldb_week table
			$stmt3 = config::$mysqli->prepare("INSERT INTO ldb_week (user_team_id, rank, points, previous_rank, previous_points, week_id) VALUES (?, ?, ?, ?, ?, ?)");
			$stmt3->bind_param("iiiiii", $user_team_id, $rank, $total, $previous_rank, $previous_total, $week_id);
			
			$stmt4 = config::$mysqli->prepare("UPDATE ldb_week SET rank = ?, points = ?, previous_rank = ?, previous_points = ? WHERE user_team_id = ? AND week_id = ? LIMIT 1");
			$stmt4->bind_param("iiiiii", $rank, $total, $previous_rank, $previous_total, $user_team_id, $week_id);
			
			// declare statement that will check if team has already been entered for current week
			$stmt5 = config::$mysqli->prepare("SELECT user_team_id
			FROM ldb_week
			WHERE user_team_id = ?
			AND week_id = ?
			LIMIT 1");
			$stmt5->bind_param("ii", $user_team_id, $week_id);
			
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($s_id, $week_id, $week_start, $week_end);
			
			while($stmt->fetch()) {
				$stmt2->execute();
				$stmt2->store_result();
				$stmt2->bind_result($user_team_id, $user_team_name, $date, $total, $previous_total, $previous_rank, $rank);
				while($stmt2->fetch()) {
					// insert or update each months leaderboard
					$stmt5->execute();
					$stmt5->store_result();
					if ($stmt5->num_rows == 0) {
						$stmt3->execute();
					}
					else {
						$stmt4->execute();
					}
				}
			}
			$stmt->close();
			$stmt2->close();
			$stmt3->close();
			$stmt4->close();
			$stmt5->close();
			
			// update table_updates table
			$db->insertUpdateTime('ldb_week');
			
			
			/* the following lines of code will first select each private league for the current season
			then create the overall leaderbaord for each private league
			and then either insert the leaderboard into the pl_overall table if it is the first instance for the current season
			or update each row with the correct details based on the user_team_id and the season_id */
			
			// select each private league for the current season
			$pls = config::$mysqli->prepare("SELECT league_id
			FROM private_leagues 
			WHERE date > ? AND date < ?
			ORDER BY date ASC");
			$pls->bind_param("ss", config::$curSeasonStart, config::$curSeasonEnd);
			
			$pls->execute();
			$pls->store_result();
			$pls->bind_result($pl_id);
			while($pls->fetch()) {
				// declare statement that will create the private league leaderboard with the relevant fields
				$stmt = config::$mysqli->prepare("SELECT overall.user_team_id, overall.user_team_name, overall.date, overall.total, COALESCE(overall.previous_total, 0) AS previous_total, COALESCE(overall.previous_rank, 0) AS previous_rank, (@row := @row + 1) AS rank
				FROM (
					SELECT ut.user_team_id, ut.user_team_name, ut.date, COALESCE(SUM(CASE WHEN user_team_selections.transfer_in < match_fixtures.date AND user_team_selections.transfer_out > match_fixtures.date AND match_fixtures.date >= ? AND match_fixtures.date <= ? THEN team_points.points ELSE 0 END),0) as total, pl.points AS previous_total, pl.rank AS previous_rank
					FROM user_teams ut
					LEFT JOIN pl_season pl
					ON pl.user_team_id = ut.user_team_id
					LEFT JOIN user_team_selections
					ON ut.user_team_id = user_team_selections.user_team_id
					LEFT JOIN team_points
					ON user_team_selections.team_id = team_points.team_id
					LEFT JOIN match_fixtures
					ON team_points.fixture_id = match_fixtures.fixture_id
					LEFT JOIN user_leagues ul
					ON ut.user_team_id = ul.user_team_id
					WHERE ut.date >= ?
					AND ul.league_id = ?
					GROUP BY ut.user_team_id
					ORDER BY total DESC, ut.user_team_name ASC
				) overall, (SELECT @row := 0) r
				WHERE overall.date >= ?
				ORDER BY rank ASC, overall.user_team_name ASC");
				$stmt->bind_param("sssis", config::$curSeasonStart, config::$curSeasonEnd, config::$curSeasonStart, $pl_id, config::$curSeasonStart);
								
				// declare statements to insert and update pl_overall table
				$stmt2 = config::$mysqli->prepare("INSERT INTO pl_season (pl_id, user_team_id, rank, points, previous_rank, previous_points, season_id) 
				VALUES (?, ?, ?, ?, ?, ?, ?)");
				$stmt2->bind_param("iiiiiii", $pl_id, $user_team_id, $rank, $total, $previous_rank, $previous_total, config::$curSeasonId);
				
				$stmt3 = config::$mysqli->prepare("UPDATE pl_season 
				SET rank = ?, points = ?, previous_rank = ?, previous_points = ? 
				WHERE user_team_id = ? AND season_id = ? AND pl_id = ?
				LIMIT 1");
				$stmt3->bind_param("iiiiiii", $rank, $total, $previous_rank, $previous_total, $user_team_id, config::$curSeasonId, $pl_id);
				
				// declare statement that will check if team has already been entered for current season
				$stmt4 = config::$mysqli->prepare("SELECT user_team_id
				FROM pl_season
				WHERE user_team_id = ?
				AND season_id = ?
				AND pl_id = ?
				LIMIT 1");
				$stmt4->bind_param("iii", $user_team_id, config::$curSeasonId, $pl_id);
				
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($user_team_id, $user_team_name, $date, $total, $previous_total, $previous_rank, $rank);
				while($stmt->fetch()) {
					$stmt4->execute();
					$stmt4->store_result();
					if ($stmt4->num_rows == 0) {
						$stmt2->execute();
					}
					else {
						$stmt3->execute();
					}
				}		
				$stmt->close();
				$stmt2->close();
				$stmt3->close();
				$stmt4->close();
				
				// update table_updates table
				$db->insertUpdateTime('pl_season');
				
				
				// select every month and year where results have been entered using fixture_id's
				$stmt = config::$mysqli->prepare("SELECT MONTH(f.date) AS month, YEAR(f.date) AS year, s.season_id, m.month_id
				FROM match_fixtures f
				LEFT JOIN tff_seasons s
				ON s.season_start < f.date AND s.season_end > f.date
				LEFT JOIN tff_months m
				ON m.month = MONTH(f.date) AND m.year = YEAR(f.date)
				WHERE f.fixture_id IN ($fix_IDs)
				GROUP BY year, month
				ORDER BY year ASC, month ASC");
			
				// declare statement that will create the month leaderboard for each private league with the relevant fields
				$stmt2 = config::$mysqli->prepare("SELECT month.user_team_id, month.user_team_name, month.date, month.total, COALESCE(month.previous_total, 0) AS previous_total, COALESCE(month.previous_rank, 0) AS previous_rank, (@row := @row + 1) AS rank
				FROM (
					SELECT ut.user_team_id, ut.user_team_name, ut.date, COALESCE(SUM(CASE WHEN ts.transfer_in < f.date AND ts.transfer_out > f.date AND MONTH(f.date) = ? AND YEAR(f.date) = ? THEN tp.points ELSE 0 END),0) as total, ldb.points AS previous_total, ldb.rank AS previous_rank
					FROM user_teams ut
					LEFT JOIN pl_month ldb
					ON ldb.user_team_id = ut.user_team_id AND ldb.month_id = ?
					LEFT JOIN user_team_selections ts
					ON ut.user_team_id = ts.user_team_id
					LEFT JOIN team_points tp
					ON ts.team_id = tp.team_id
					LEFT JOIN match_fixtures f
					ON tp.fixture_id = f.fixture_id
					LEFT JOIN tff_seasons s
					ON f.date > s.season_start AND f.date < s.season_end
					LEFT JOIN user_leagues ul
					ON ut.user_team_id = ul.user_team_id
					WHERE ut.season_id = ?
						AND ul.league_id = ?
						AND ((Month(ut.date) <= ? AND Year(ut.date) = ?)
						OR (Month(ut.date) > ? AND Year(ut.date) < ?))
					GROUP BY ut.user_team_id
					ORDER BY total DESC, ut.user_team_name ASC
				) month, (SELECT @row := 0) r
				ORDER BY rank ASC, month.user_team_name ASC");
				$stmt2->bind_param("ssiiissss", $month, $year, $month_id, $s_id, $pl_id, $month, $year, $month, $year);
				
				// declare statements to insert and update pl_month table
				$stmt3 = config::$mysqli->prepare("INSERT INTO pl_month (pl_id, user_team_id, rank, points, previous_rank, previous_points, month_id) VALUES (?, ?, ?, ?, ?, ?, ?)");
				$stmt3->bind_param("iiiiiii", $pl_id, $user_team_id, $rank, $total, $previous_rank, $previous_total, $month_id);
				
				$stmt4 = config::$mysqli->prepare("UPDATE pl_month 
				SET rank = ?, points = ?, previous_rank = ?, previous_points = ? 
				WHERE user_team_id = ? AND month_id = ? AND pl_id = ?
				LIMIT 1");
				$stmt4->bind_param("iiiiiii", $rank, $total, $previous_rank, $previous_total, $user_team_id, $month_id, $pl_id);
				
				// declare statement that will check if team has already been entered for current month
				$stmt5 = config::$mysqli->prepare("SELECT user_team_id
				FROM pl_month
				WHERE user_team_id = ?
				AND month_id = ?
				AND pl_id = ?
				LIMIT 1");
				$stmt5->bind_param("iii", $user_team_id, $month_id, $pl_id);
				
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($month, $year, $s_id, $month_id);
				
				while($stmt->fetch()) {
					$stmt2->execute();
					$stmt2->store_result();
					$stmt2->bind_result($user_team_id, $user_team_name, $date, $total, $previous_total, $previous_rank, $rank);
					while($stmt2->fetch()) {
						// insert or update each months leaderboard
						$stmt5->execute();
						$stmt5->store_result();
						if ($stmt5->num_rows == 0) {
							$stmt3->execute();
						}
						else {
							$stmt4->execute();
						}
					}
				}
				$stmt->close();
				$stmt2->close();
				$stmt3->close();
				$stmt4->close();
				$stmt5->close();
				
				// update table_updates table
				$db->insertUpdateTime('pl_month');
				
				/* the following lines of code will first select the weeks for which results have been entered
				then use a select statement that will create the weekly leaderbaord
				then loop through each week where there has been a result entered
				and then either insert the leaderboard into the pl_week table if it is the first instance for the specific week
				or update each row with the correct details based on the user_team_id and the week_id */
				
				// select every week where results have been entered using fixture_id's
				$stmt = config::$mysqli->prepare("SELECT s.season_id, w.week_id, w.week_start, w.week_end
				FROM match_fixtures f
				LEFT JOIN tff_seasons s
				ON s.season_start < f.date AND s.season_end > f.date
				LEFT JOIN tff_weeks w
				ON w.week_start < f.date AND w.week_end > f.date
				WHERE f.fixture_id IN ($fix_IDs)
				GROUP BY w.week_id
				ORDER BY w.week_end ASC");
				
				// declare statement that will create the week leaderboard for each private league with the relevant fields					
				$stmt2 = config::$mysqli->prepare("SELECT week.user_team_id, week.user_team_name, week.date, week.total, COALESCE(week.previous_total, 0) AS previous_total, COALESCE(week.previous_rank, 0) AS previous_rank, (@row := @row + 1) AS rank
				FROM (
					SELECT ut.user_team_id, ut.user_team_name, ut.date, COALESCE(SUM(CASE WHEN ts.transfer_in < f.date AND ts.transfer_out > f.date AND f.date > ? AND f.date < ? THEN tp.points ELSE 0 END),0) as total, ldb.points AS previous_total, ldb.rank AS previous_rank
					FROM user_teams ut
					LEFT JOIN pl_week ldb
					ON ldb.user_team_id = ut.user_team_id AND ldb.week_id = ?
					LEFT JOIN user_team_selections ts
					ON ut.user_team_id = ts.user_team_id
					LEFT JOIN team_points tp
					ON ts.team_id = tp.team_id
					LEFT JOIN match_fixtures f
					ON tp.fixture_id = f.fixture_id
					LEFT JOIN tff_seasons s
					ON f.date > s.season_start AND f.date < s.season_end
					LEFT JOIN user_leagues ul
					ON ut.user_team_id = ul.user_team_id
					WHERE ut.season_id = ?
						AND ul.league_id = ?
						AND ut.date < ?
					GROUP BY ut.user_team_id
					ORDER BY total DESC, ut.user_team_name ASC
				) week, (SELECT @row := 0) r
				ORDER BY rank ASC, week.user_team_name ASC");
				$stmt2->bind_param("ssiiis", $week_start, $week_end, $week_id, $s_id, $pl_id, $week_end);
				
				// declare statements to insert and update pl_week table
				$stmt3 = config::$mysqli->prepare("INSERT INTO pl_week (pl_id, user_team_id, rank, points, previous_rank, previous_points, week_id) VALUES (?, ?, ?, ?, ?, ?, ?)");
				$stmt3->bind_param("iiiiiii", $pl_id, $user_team_id, $rank, $total, $previous_rank, $previous_total, $week_id);
				
				$stmt4 = config::$mysqli->prepare("UPDATE pl_week 
				SET rank = ?, points = ?, previous_rank = ?, previous_points = ? 
				WHERE user_team_id = ? AND week_id = ? AND pl_id = ?
				LIMIT 1");
				$stmt4->bind_param("iiiiiii", $rank, $total, $previous_rank, $previous_total, $user_team_id, $week_id, $pl_id);
				
				// declare statement that will check if team has already been entered for current week
				$stmt5 = config::$mysqli->prepare("SELECT user_team_id
				FROM pl_week
				WHERE user_team_id = ?
				AND week_id = ?
				AND pl_id = ?
				LIMIT 1");
				$stmt5->bind_param("iii", $user_team_id, $week_id, $pl_id);
				
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($s_id, $week_id, $week_start, $week_end);
				
				while($stmt->fetch()) {
					$stmt2->execute();
					$stmt2->store_result();
					$stmt2->bind_result($user_team_id, $user_team_name, $date, $total, $previous_total, $previous_rank, $rank);
					while($stmt2->fetch()) {
						// insert or update each weeks leaderboard
						$stmt5->execute();
						$stmt5->store_result();
						if ($stmt5->num_rows == 0) {
							$stmt3->execute();
						}
						else {
							$stmt4->execute();
						}
					}
				}
				$stmt->close();
				$stmt2->close();
				$stmt3->close();
				$stmt4->close();
				$stmt5->close();
				
				// update table_updates table
				$db->insertUpdateTime('pl_week');
                                
                                
                                
                                // create overall league table and then insert into database
                                
				
			}
    }
	
    private function pointsCalc($id, $ht_goals, $at_goals, $pens)
    {
        $stmt = config::$mysqli->prepare("
        SELECT th.league_id, h.team_id, ta.league_id, a.team_id
        FROM match_fixtures f
        LEFT JOIN team_names h
        ON f.ht_id = h.team_id
        LEFT JOIN team_names a
        ON f.at_id = a.team_id
        LEFT JOIN tff_teams th
        ON h.team_id = th.team_id
        LEFT JOIN tff_teams ta
        ON a.team_id = ta.team_id
        WHERE f.fixture_id = ?
        GROUP BY f.fixture_id");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($ht_league_id, $ht_id, $at_league_id, $at_id);
        $stmt->fetch();
        $stmt->close();

        // calculate differences between leagues
        $ht_league_dif = ($ht_league_id - $at_league_id) <= 0 || $at_league_id == 0 ? 0 : $ht_league_id - $at_league_id;
        $at_league_dif = ($at_league_id - $ht_league_id) <= 0 || $ht_league_id == 0 ? 0 : $at_league_id - $ht_league_id;

        // calculate points for result
        if ($pens == 0) 
        {
            if ($ht_goals > $at_goals)
            {
                $ht_result_points = self::POINTS_FOR_WIN;
                $at_result_points = self::POINTS_FOR_LOSS;
            }
            elseif ($ht_goals == $at_goals)
            {
                $ht_result_points = self::POINTS_FOR_DRAW;
                $at_result_points = self::POINTS_FOR_DRAW;
            }
            elseif ($ht_goals < $at_goals)
            {
                $ht_result_points = self::POINTS_FOR_LOSS;
                $at_result_points = self::POINTS_FOR_WIN;
            }
        }
        elseif ($pens == $ht_id)
        {
            $ht_result_points = self::POINTS_FOR_WIN;
            $at_result_points = self::POINTS_FOR_LOSS;
        }
        else
        {
            $ht_result_points = self::POINTS_FOR_LOSS;
            $at_result_points = self::POINTS_FOR_WIN;
        }

        // calculate points for goals scored
        $ht_goals_points = $ht_goals * self::POINTS_FOR_GOAL_SCORED;
        $at_goals_points = $at_goals * self::POINTS_FOR_GOAL_SCORED;

        // calculate points for clean sheets
        $ht_cs_points = $at_goals == 0 ? self::POINTS_FOR_CLEAN_SHEET : 0;
        $at_cs_points = $ht_goals == 0 ? self::POINTS_FOR_CLEAN_SHEET : 0;

        // calculate total points
        $ht_points = ((1 + $ht_league_dif) * ($ht_goals_points + $ht_result_points + $ht_cs_points));
        $at_points = ((1 + $at_league_dif) * ($at_goals_points + $at_result_points + $at_cs_points));

        return array($ht_id, $ht_points, $at_id, $at_points);
    }
    
    public function createLeagueTables()
    {
        // put all fixture ID's into variable
        $fixtureIds = implode(',', array_column($this->resultsArray, 'fixtureId'));
        
        // set leagueIds from fixture ID
        $leagueIds = $this->setLeagueIds($fixtureIds);
        foreach($leagueIds as $leagueId)
        {
            // set league tables that need updating based on fixture dates
            $seasonIds = $this->setLeagueTablesToUpdate('season', $fixtureIds, $leagueId);
            $monthIds  = $this->setLeagueTablesToUpdate('month', $fixtureIds, $leagueId);
            $weekIds   = $this->setLeagueTablesToUpdate('week', $fixtureIds, $leagueId);

            // update tables for league
            $this->runLeagueUpdate('season', $leagueId, $seasonIds);
            $this->runLeagueUpdate('month', $leagueId, $monthIds);
            $this->runLeagueUpdate('week', $leagueId, $weekIds);
        }
    }
    
    private function setLeagueIds($fixtureIds)
    {
        $stmt = config::$mysqli->prepare("
        SELECT c.comp_id
        FROM competitions c
        INNER JOIN match_fixtures f
        ON f.comp_id = c.comp_id
        WHERE f.fixture_id IN ($fixtureIds)
        GROUP BY c.comp_id");
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 0)
        {
            return false;
        }
        $stmt->bind_result($leagueId);
        while($stmt->fetch())
        {
            $leagueIds[] = $leagueId;
        }
        $stmt->close();
        return $leagueIds;
    }
    
    private function setLeagueTablesToUpdate($leagueType, $fixtureIds, $leagueId)
    {
        /*
        switch($leagueType)
        {
            case 'season':
                $select = 'season_id';
                $table  = 'tff_seasons';
                $on     = 'd.season_start <= f.date AND f.date <= d.season_end';
                $group  = 'season_id';
            break;
            case 'month':
                $select = 'month_id';
                $table  = 'tff_months';
                $on     = 'd.month = MONTH(f.date) AND d.year = YEAR(f.date)';
                $group  = 'month_id';
            break;
            case 'week':
                $select = 'week_id';
                $table  = 'tff_weeks';
                $on     = 'd.week_start <= f.date AND f.date <= d.week_end';
                $group  = 'week_id';
            break;
        }
         * */
        
        $stmt = config::$mysqli->prepare("
        SELECT d.".$leagueType."_id
        FROM match_fixtures f 
        INNER JOIN tff_".$leagueType."s d 
        ON d.".$leagueType."_start <= f.date AND f.date <= d.".$leagueType."_end
        WHERE f.comp_id = ?
        AND f.fixture_id IN ($fixtureIds)
        GROUP BY ".$leagueType."_id");
	$stmt->bind_param("i", $leagueId);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 0)
        {
            return false;
        }
        $stmt->bind_result($dateId);
        while($stmt->fetch())
        {
            $dateIds[] = $dateId;
        }
        $stmt->close();
        return $dateIds;
    }
    
    private function runLeagueUpdate($leagueType, $leagueId, $dateIds)
    {
        $tableToUpdate = $this->setDatabaseTable($leagueType);
        
        foreach($dateIds as $dateId)
        {
            // make league table
            $importData   = $this->makeLeagueTables($leagueType, $leagueId, $dateId);
            $dateIdColumn = $this->getTableColumn($tableToUpdate);
            
            // check if league table has been inserted before
            if($this->checkLeagueTable($tableToUpdate, $dateId, $dateIdColumn, $leagueId))
            {
                // if true then update table
                $this->updateLeagueTable($tableToUpdate, $importData, $leagueId, $dateId, $dateIdColumn);
                return;
            }
            // if false insert new league table
            $this->insertLeagueTable($tableToUpdate, $importData, $leagueId, $dateId, $dateIdColumn);
        }
    }
    
    private function setDatabaseTable($leagueType)
    {
        switch($leagueType)
        {
            case 'season':
                $tableToUpdate = 'ltbl_season';
            break;
            case 'month':
                $tableToUpdate = 'ltbl_month';
            break;
            case 'week':
                $tableToUpdate = 'ltbl_week';
            break;
        }
        
        return $tableToUpdate;
    }
    
    private function makeLeagueTables($leagueType, $leagueId, $dateId)
    {
        list($startDate, $endDate) = $this->getDates($leagueType, $dateId);
        
        $stmt = config::$mysqli->prepare("
        SELECT
            (@row := @row + 1) AS rank,
            overall.team_id,
            overall.Games, 
            overall.Won, 
            overall.Draw, 
            overall.Lost, 
            overall.gf, 
            overall.ga, 
            overall.gd,
            overall.Pts
        FROM
        (
            SELECT 
                tn.team_id, 
                COALESCE(SUM(x.played), 0) AS Games, 
                COALESCE(SUM(x.win), 0) AS Won, 
                COALESCE(SUM(x.lose), 0) AS Lost, 
                COALESCE(SUM(x.draw), 0) AS Draw, 
                COALESCE(SUM(x.pts), 0) - COALESCE(d.pts,0) AS Pts, 
                COALESCE(SUM(goalsfor), 0) AS gf, 
                COALESCE(SUM(goalsagainst), 0) AS ga, 
                COALESCE(SUM(goalsfor), 0) - COALESCE(SUM(goalsagainst),0) AS gd
            FROM team_names tn
            INNER JOIN
            (
                SELECT
                    f.ht_id as team_id,
                    1 as played,
                    IF(fthg > ftag, 1, 0) as win,
                    IF(fthg < ftag, 1, 0) as lose,
                    IF(fthg = ftag AND (fthg IS NOT NULL OR ftag IS NOT NULL), 1, 0) as draw,
                    CASE WHEN fthg > ftag THEN 3 WHEN fthg < ftag THEN 0 ELSE 1 END as pts,
                    fthg as goalsfor,
                    ftag as goalsagainst
                FROM match_results r
                INNER JOIN match_fixtures f
                ON r.fixture_id = f.fixture_id 
                LEFT JOIN match_results_pp pp
                ON r.fixture_id = pp.fixture_id 
                WHERE f.comp_id = ? 
                AND f.date >= ?
                AND f.date <= ?
                AND f.round = ''
                AND pp.fixture_id IS NULL

                UNION ALL

                SELECT
                    f.at_id as team_id,
                    1 as played,
                    IF(fthg < ftag, 1,0) as win,
                    IF(fthg > ftag, 1,0) as lose,
                    IF(fthg = ftag AND (fthg IS NOT NULL OR ftag IS NOT NULL), 1,0) as draw,
                    CASE WHEN fthg < ftag THEN 3 WHEN fthg > ftag THEN 0 ELSE 1 END as pts,
                    ftag as goalsfor,
                    fthg as goalsagainst
                FROM match_results r
                INNER JOIN match_fixtures f
                ON r.fixture_id = f.fixture_id
                LEFT JOIN match_results_pp pp
                ON r.fixture_id = pp.fixture_id 
                WHERE f.comp_id = ?
                AND f.date >= ?
                AND f.date <= ?
                AND f.round = ''
                AND pp.fixture_id IS NULL
            ) AS x
            ON tn.team_id = x.team_id
            LEFT JOIN
            (
                SELECT
                    pd.team_id,
                    pd.points_deducted AS pts,
                    s.season_start,
                    s.season_end
                FROM ltbl_point_deductions pd
                LEFT JOIN tff_seasons s
                ON s.season_id = pd.season_id
                WHERE s.season_start <= ? AND ? <= s.season_end
            ) AS d
            ON tn.team_id = d.team_id
            GROUP BY tn.team_name
            ORDER BY Pts DESC, gd DESC, gf DESC, tn.team_name ASC
        ) overall, (SELECT @row := 0) r
        ORDER BY rank ASC
        ");
	$stmt->bind_param("ississss", $leagueId, $startDate, $endDate, $leagueId, $startDate, $endDate, $startDate, $endDate);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($rank, $teamId, $gamesPld, $gamesWon, $gamesDrawn, $gamesLost, $goalsFor, $goalsAgainst, $goalDifference, $pts);
        while($stmt->fetch())
        {
            $importData[] = array(
                "team_id" => $teamId,
                "rank"    => $rank,
                "points"  => $pts, 
                "p"       => $gamesPld, 
                "w"       => $gamesWon, 
                "d"       => $gamesDrawn, 
                "l"       => $gamesLost, 
                "gf"      => $goalsFor, 
                "ga"      => $goalsAgainst,
                "gd"      => $goalDifference
            );
        }
        
        return $importData;
    }
    
    private function getDates($leagueType, $dateId)
    {
        $stmt = config::$mysqli->prepare("
        SELECT ".$leagueType."_start, ".$leagueType."_end
        FROM tff_".$leagueType."s
        WHERE ".$leagueType."_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $dateId);
        $stmt->execute();
        $stmt->store_result();
	$stmt->bind_result($startDate, $endDate);
        if ($stmt->num_rows == 0)
        {
            return false;
        }
        $stmt->fetch();
        
        return array($startDate, $endDate);
    }
    
    private function getTableColumn($tableToUpdate)
    {
        switch($tableToUpdate)
        {
            case 'ltbl_season':
                $dateIdColumn = 'season_id';
            break;
            case 'ltbl_month':
                $dateIdColumn = 'month_id';
            break;
            case 'ltbl_week':
                $dateIdColumn = 'week_id';
            break;
        }
        
        return $dateIdColumn;
    }
    
    private function checkLeagueTable($tableToCheck, $dateIdToCheck, $dateIdColumn, $leagueId)
    {
        $stmt = config::$mysqli->prepare("
        SELECT ldb_id
        FROM $tableToCheck
        WHERE $dateIdColumn = ?
        AND league_id = ?
        LIMIT 1");
        $stmt->bind_param("ii", $dateIdToCheck, $leagueId);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 0)
        {
            return false;
        }
        return true;
    }
    
    private function insertLeagueTable($tableToInsertTo, $importData, $leagueId, $dateId, $dateIdColumn)
    {
        foreach($importData as $values)
        {
            if($this->database->completeInsert($tableToInsertTo, 
                                               array('league_id', 'team_id', 'rank', 'points', $dateIdColumn, 'p', 'w', 'd', 'l', 'gf', 'ga', 'gd'), 
                                               array($leagueId, $values['team_id'], $values['rank'], $values['points'], $dateId, $values['p'], $values['w'], $values['d'], $values['l'], $values['gf'], $values['ga'], $values['gd']), 
                                               array('i','i','i','i','i','i','i','i','i','i','i','i')))
            {
                $this->importSuccess($values);
            }
            else
            {
                $this->importFail($values);
            }
        }
    }
    
    private function updateLeagueTable($tableToInsertTo, $importData, $leagueId, $dateId, $dateIdColumn)
    {
        foreach($importData as $values)
        {
            if($this->database->completeQuery('UPDATE '.$tableToInsertTo.' SET rank = ?, points = ?, p = ?, w = ?, d = ?, l = ?, gf = ?, ga = ?, gd = ? WHERE league_id = ? AND team_id = ? AND '.$dateIdColumn.' = ? LIMIT 1', 
                                               array($values['rank'], $values['points'], $values['p'], $values['w'], $values['d'], $values['l'], $values['gf'], $values['ga'], $values['gd'], $leagueId, $values['team_id'], $dateId), 
                                               array('i','i','i','i','i','i','i','i','i','i','i','i')))
            {
                $this->importSuccess($values);
            }
            else
            {
                $this->importFail($values);
            }	
        }
    }
}
