<?php
class news
{
	const NEWS_ARTICLE_LINK             = "<a href=\"%s/news/%s\" title=\"%s\" class=\"%s\">%s</a>";
	const OTHER_ARTICLES_TO_DISPLAY     = 5;
	const HOME_PAGE_ARTICLES_TO_DISPLAY = 10;
	const ARTICLE_TEASER_LENGTH         = 180;
	const ARTICLE_SUFFIX                = "...";
	const INSERT_COMMENT 		        = '';
	const DEFAULT_NAME_TEXT 		    = 'Enter your name...';
	const DEFAULT_COMMENTS_TEXT 		= 'Enter your comments...';
	const DEFAULT_VALIDATOR_TEXT 		= 'What is 3 + 5?';
	const NO_ARTICLES_ERROR             = 'No articles have been posted.';
	
	private $counter = 1;
		
	protected $urlGenerator;
	protected $articleId;
	protected $articleTitle;
	protected $articleContent;
	protected $articleImage;
	protected $articleDate;
	protected $articleTotalComments;
	protected $articleInformation;
	
	public function __construct($ug)
	{
		$this->urlGenerator = $ug;
	}
	
	public function displayHomePageNews()
	{
		$this->setNewsArticleInformation(FALSE, TRUE);
		
		while($this->articleInformation->fetch())
		{			
			if($this->counter == 1)
			{
				echo '
				<div id="news-section-one">
					'.$this->getArticle(300, "grey").'
				</div>';
			}
			elseif($this->counter == 2 || $this->counter == 3)
			{
				if ($this->counter == 2)
				{
					echo '<div id="news-section-two">';
				}
				echo '
				<div class="'.$this->getClass().'">
					'.$this->getArticle(110, "grey").'
				</div>';
				if ($this->counter == 3)
				{
					echo '</div>';
				}
			}
			else
			{
				if ($this->counter == 4)
				{
					echo '<div id="news-section-three">';
				}
				echo '
				<a href="'.$this->getArticleLink().'" title="Read Full Article" class="'.$this->getClass().'">
					<h3>'.$this->articleTitle.'</h3>
					<span class="date">'.$this->getArticleDate().'</span>
				</a>';
				if ($this->counter == self::HOME_PAGE_ARTICLES_TO_DISPLAY)
				{
					echo '</div>';
				}
			}
			$this->counter++;
		}
		$this->articleInformation->close();
	}
	
	public function displayNews()
	{
		$this->setNewsArticleInformation();
		
		if($this->articleInformation)
		{
			while($this->articleInformation->fetch())
			{
				echo '
				<div class="articles '.$this->getNewsClass().'">
					'.$this->getArticleImage().'
					<h2><a href="'.$this->getArticleLink().'" title="'.$this->articleTitle.'">'.$this->articleTitle.'</a></h2>
					<span class="date">
						Posted on '.$this->getArticleDate().'
						<a href="'.$this->getArticleLink().'#comments" title="Read Comments on '.$this->articleTitle.'" class="comments">'.$this->articleTotalComments.' Comments</a>
					</span>
					<p>'.$this->shrinkText(self::ARTICLE_TEASER_LENGTH, $this->articleContent).'</p>
				</div>';
				
				$this->counter++;
			}
		}
		else
		{
			echo notifications::showNotification('error', FALSE, self::NO_ARTICLES_ERROR);
		}
		$this->articleInformation->close();
		echo '<br class="clearfloat" />';
	}
	
	protected function setNewsArticleInformation($articleId = FALSE, $homePage = FALSE, $otherArticles = FALSE)
	{
		$whereOperator = $articleId <> FALSE && $otherArticles == FALSE ? "=" : "<>";
		$limitAmount   = $homePage <> FALSE && $otherArticles == FALSE ? self::HOME_PAGE_ARTICLES_TO_DISPLAY : self::OTHER_ARTICLES_TO_DISPLAY;
			
		$where = $articleId <> FALSE || $otherArticles <> FALSE ? "WHERE p.news_id $whereOperator ?" : "";
		$limit = $homePage <> FALSE || $otherArticles <> FALSE ? "LIMIT $limitAmount" : "";	
		
		$this->articleInformation = config::$mysqli->prepare("
		SELECT p.news_id, p.title, p.post, p.image, p.date, SUM(CASE WHEN c.rejected <> 1 THEN 1 ELSE 0 END)
		FROM news_posts p
		LEFT JOIN news_comments c
		ON p.news_id = c.nid
		$where
		GROUP BY p.news_id ORDER BY p.date DESC
		$limit");
		if($articleId <> FALSE)
		{
			$this->articleInformation->bind_param("i", $articleId);
		}
		$this->articleInformation->bind_result(
			$this->articleId,
			$this->articleTitle, 
			$this->articleContent, 
			$this->articleImage, 
			$this->articleDate, 
			$this->articleTotalComments
		);
		$this->articleInformation->execute();
		if($articleId <> FALSE && $otherArticles == FALSE)
		{
			$this->articleInformation->fetch();
			$this->articleInformation->close();
		}
	}
	
	private function shrinkText($length, $text)
	{
		$text = strip_tags($text);
		if($length > strlen($text))
		{
			return $text;
		}
	
		while(!isset($text[$length]) || $text[$length] != ' ' && $length != 0)
		{
			$length--;
		}
	
		return mb_substr($text, 0, $length).self::ARTICLE_SUFFIX;
	}
	
	private function getArticle($articleLength, $btnClass)
	{
		return 
		$this->getArticleImage().'
		<h3><a href="'.$this->getArticleLink().'" title="Read Full Article">'.$this->articleTitle.'</a></h3>
		<span class="date">'.$this->getArticleDate().'</span>
		<p>'.$this->shrinkText($articleLength, $this->articleContent).'</p>
		<!--<a href="'.$this->getArticleLink().'" title="Read Full Article" class="btn '.$btnClass.'">Read Full Article</a>-->
		<br class=clearfloat />';
	}
	
	private function getClass()
	{
		return $this->counter % 2 == 0 ? 'white' : 'very-very-light-grey';
	}
	
	private function getNewsClass()
	{
		return $this->counter % 4 == 1 || ($this->counter + 1) % 4 == 1 ? 'white' : 'very-very-light-grey';
	}
	
	private function getArticleLink()
	{
		return $this->urlGenerator->makeUrl($this->articleTitle, $this->articleId, 'news');
	}
	
	private function getArticleDate()
	{
		return date("j F Y H:i", strtotime($this->articleDate));
	}
	
	private function getArticleImage()
	{
		$this->setNewsArticleImage();
		return '<img src="'.config::$baseUrl.'/img/content/news/'.$this->articleImage.'.jpg" alt="News Article Image" />';
	}
	
	private function setNewsArticleImage()
	{
		if(empty($this->articleImage))
		{
			$this->articleImage = 'news';
		}
	}
}
?>			