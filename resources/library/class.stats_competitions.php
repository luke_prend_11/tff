<?php
class stats_competitions extends stats
{
    const LEAGUE_TABLE_SIZE_FULL   = "full";
    const LEAGUE_TABLE_SIZE_SHORT  = "short";
    const LEAGUE_TABLE_VIEW_SEASON = "season";
    const LEAGUE_TABLE_VIEW_MONTH  = "month";
    const LEAGUE_TABLE_VIEW_WEEK   = "week";
    const RANK_CHANGE_CLASS_UP     = "up";
    const RANK_CHANGE_CLASS_DOWN   = "down";
    const RANK_CHANGE_CLASS_SAME   = "same";
    const LEAGUE_SEPARATOR_CLASS   = "league-separator";
    const HIGHLIGHT_TEAM_CLASS     = "my-teams";
    
    private $competitionType;
    private $leagueTableData;
    private $leagueTableString;
    private $leagueDateId;
    private $leagueTableSize;
    private $leagueTableType;
    private $leagueCompId;
    private $teamToHighlight;
    
    private $userFunctions;
    
    public function __construct($ug, $uf)
    {
        parent::__construct($ug);
        
        $this->userFunctions = $uf;
        
        $this->checkFormSubmission();
        
        $this->setCompetitionInformation();
    }
    
    private function checkFormSubmission()
    {
        if(filter_has_var(INPUT_POST, 'change_season'))
        {
            $this->setResultFormFields();
        }
    }
	
    private function setResultFormFields()
    {
        $this->selectedSeasonId = isset($_POST['season']) ? $_POST['season'] : config::$curSeasonId;
    }
    
    private function setCompetitionInformation()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            c.comp_type
        FROM competitions c
        WHERE c.comp_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $this->urlId);
        $stmt->bind_result($this->competitionType);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
    }
	
    public function displayCompetitionPage()
    {
        echo '
        <div class="row stretch-parent">
            <div class="col-2-x-3 stretch-child">';
        
        if($this->competitionType == 'league')
        {
            echo '
                <div class="very-light-grey">
                    <h2 class="news dark-grey">League Table</h2>'.
                    $this->showLeagueTableHistoryForm().'
                    <div class="inner-container">'.
                        $this->showLeagueTable(self::LEAGUE_TABLE_SIZE_FULL, self::LEAGUE_TABLE_VIEW_SEASON)
                    .'</div>
                </div>';
        }
        
        echo '
            </div>

            <div class="col-1-x-2 stretch-child very-light-grey">
                <h2 class="news dark-grey">Matches</h2>
                <div class="inner-container">'.
                    $this->showCompetitionMatches()
                .'</div>
            </div>
            <span class="stretch"></span>
        </div>';		
    }
    
    private function showCompetitionFantasyFootballStats()
    {
        
    }
    
    private function showCompetitionMatches()
    {
        // get 20 latest fixtures
        $m = new matches($this->urlGenerator, $this->userFunctions);        
        return $m->displayMatches('all', $this->urlId);
    }
    
    private function getLeagueTableHistoryOptions()
    {
        $string = '';
        
        // get previous seasons
        $stmt = config::$mysqli->prepare("
        SELECT 
            season_id,
            season_name
        FROM tff_seasons
        WHERE season_start < NOW()
        ORDER BY season_start DESC
        ");
        $stmt->bind_result($seasonId, $seasonName);
	$stmt->execute();
	$stmt->store_result();
        if($stmt->num_rows <> 0)
        {
            while($stmt->fetch())
            {
                $this->seasonId[]   = $seasonId;
                $this->seasonName[] = $seasonName;
            }
        }
        return $string;
    }
    
    public function showLeagueTableHistoryForm()
    {
        $this->getLeagueTableHistoryOptions();
        
        return '
        <form action="" method="post" class="mini-form">'.
            $this->userFunctions->showStickyForm('select', 'season', $this->seasonName, FALSE, 'Select Season', $this->seasonId, TRUE).'
            <input type="submit" name="change_season" value="Go" class="button">
        </form>';
    }
    
    public function showLeagueTable($leagueTableSize, $leagueTableType, $dateId = NULL, $compId = NULL, $teamToHighlight = NULL)
    {
        $this->leagueDateId    = $dateId !== NULL ? $dateId : (isset($this->selectedSeasonId) ? $this->selectedSeasonId : config::$curSeasonId);
        $this->leagueTableSize = $leagueTableSize;
        $this->leagueTableType = $leagueTableType;
        $this->leagueCompId    = $compId !== NULL ? $compId : $this->urlId;
        $this->teamToHighlight = $teamToHighlight;
        
        $this->makeLeagueTables();

        if(!empty($this->leagueTableData))
        {
            $this->leagueTableString = $this->leagueTableHeader();
            
            foreach($this->leagueTableData as $values)
            {
                $this->leagueTableString .= $this->leagueTableBody($values);
                config::updateRowClass();
            }
            
            $this->leagueTableString .= '</table>';

            return $this->leagueTableString;
        }
        else
        {
            return notifications::showNotification('error', FALSE, 'No table to display.');
        }
    }
    
    private function makeLeagueTables()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            tn.team_id, 
            tn.team_name, 
            lt.rank,
            lt.points,
            IF(lt.previous_rank = 0, 0, lt.previous_rank - lt.rank),
            lt.p,
            lt.w,
            lt.d,
            lt.l,
            lt.gf,
            lt.ga,
            lt.gd
        FROM ltbl_".$this->leagueTableType." lt
        INNER JOIN team_names tn
        ON tn.team_id = lt.team_id
        WHERE lt.".$this->leagueTableType."_id = ?
        AND league_id = ?
        ORDER BY lt.rank ASC
        ");
	$stmt->bind_param("ii", $this->leagueDateId, $this->leagueCompId);
        $stmt->bind_result($teamId, $teamName, $rank, $pts, $rankChange, $gamesPld, $gamesWon, $gamesDrawn, $gamesLost, $goalsFor, $goalsAgainst, $goalDifference);
	$stmt->execute();
	$stmt->store_result();
        if($stmt->num_rows <> 0)
        {
            while($stmt->fetch())
            {
                $this->leagueTableData[] = array(
                    "team_id"     => $teamId,
                    "team_name"   => $teamName,
                    "rank"        => $rank,
                    "points"      => $pts, 
                    "rank_change" => $rankChange, 
                    "p"           => $gamesPld, 
                    "w"           => $gamesWon, 
                    "d"           => $gamesDrawn, 
                    "l"           => $gamesLost, 
                    "gf"          => $goalsFor, 
                    "ga"          => $goalsAgainst,
                    "gd"          => $goalDifference
                );
            }
        }
        // if no table has been created yet then get the list of teams from tff_teams table
        else
        {
            $stmt = config::$mysqli->prepare("
            SELECT 
                t.team_id, 
                t.team_name, 
                (@row := @row + 1) AS rank
            FROM 
            (
                SELECT 
                    tn.team_id, 
                    tn.team_name
                FROM team_names tn
                LEFT JOIN tff_teams tt
                ON tn.team_id = tt.team_id 
                WHERE tt.league_id = ?
                GROUP BY tn.team_id
                ORDER BY team_name ASC
            ) AS t, (SELECT @row := 0) r
            ORDER BY rank ASC
            ");
            $stmt->bind_param("i", $this->leagueCompId);
            $stmt->bind_result($teamId, $teamName, $rank);
            $stmt->execute();
            $stmt->store_result();
            while($stmt->fetch())
            {
                $this->leagueTableData[] = array(
                    "team_id"     => $teamId,
                    "team_name"   => $teamName,
                    "rank"        => $rank,
                    "points"      => 0, 
                    "rank_change" => 0, 
                    "p"           => 0, 
                    "w"           => 0, 
                    "d"           => 0, 
                    "l"           => 0, 
                    "gf"          => 0, 
                    "ga"          => 0,
                    "gd"          => 0
                );
            }
        }
    }
    
    private function leagueTableHeader()
    {
        $tHeadClass   = $this->leagueTableSize == self::LEAGUE_TABLE_SIZE_SHORT ? ' class="short"' : '';
        $tHeadRank    = $this->leagueTableSize == self::LEAGUE_TABLE_SIZE_SHORT ? '<th>&nbsp;' : '<th><span>Pos</span>';

        return '
        <table'.$tHeadClass.'>
            <thead>
                <tr>'
                    .$tHeadRank.'
                    <th><span>Team</span>
                    <th class="align-right"><span>Pld</span>
                    <th class="align-right"><span>W</span>
                    <th class="align-right"><span>D</span>
                    <th class="align-right"><span>L</span>
                    <th class="align-right hide"><span>F</span>
                    <th class="align-right hide"><span>A</span>
                    <th class="align-right hide"><span>GD</span>
                    <th class="align-right"><span>Pts</span>
            <tbody>';
    }
    
    private function leagueTableBody($values)
    {
        return '
        <tr class="'.$this->getRowClass($values['rank'], $values['team_id']).'">
            <td class="'.$this->getRankChangeClass($values['rank_change']).'">'
                .$values['rank'].'
            <td class="club" style="background-image:url('.config::$baseUrl.'/img/content/team-badges/small/'.$values['team_id'].'.png); background-size: 18px 18px;">'
                .$this->urlGenerator->showLink($values['team_name'], $values['team_id'], 'team').'
            <td class="align-right">'
                .$values['p'].'
            <td class="align-right">'
                .$values['w'].'
            <td class="align-right">'
                .$values['d'].'
            <td class="align-right">'
                .$values['l'].'
            <td class="align-right hide">'
                .$values['gf'].'
            <td class="align-right hide">'
                .$values['ga'].'
            <td class="align-right hide">'
                .$values['gd'].'
            <td class="align-right">'
                .$values['points'];
    }
	
    private function getRowClass($rank, $teamId)
    {
        // set league separators class
        switch($this->leagueCompId) {
            case 1:
                $leagueSeparators = array(4, 5, 17);
                break;
            case 2:
                $leagueSeparators = array(2, 6, 21);
                break;
            case 3:
                $leagueSeparators = array(2, 6, 20);
                break;
            case 4:
                $leagueSeparators = array(3, 7, 22);
                break;
        }
        
        $rowClass = $this->teamToHighlight !== NULL && ($this->teamToHighlight == $teamId || (is_array($this->teamToHighlight) && in_array($teamId, $this->teamToHighlight))) ? self::HIGHLIGHT_TEAM_CLASS : 'table-row'.config::$rowClass;
        
        return in_array($rank, $leagueSeparators) ? $rowClass.' '.self::LEAGUE_SEPARATOR_CLASS : $rowClass;
    }
	
    private function getRankChangeClass($rankChange)
    {
        if($rankChange < 0)
        {
            return self::RANK_CHANGE_CLASS_DOWN;
        }
        elseif($rankChange > 0)
        {
            return self::RANK_CHANGE_CLASS_UP;
        }
        else
        {
            return self::RANK_CHANGE_CLASS_SAME;
        }
    }
    
    private function showCompetitionInfo()
    {
        
    }
}
