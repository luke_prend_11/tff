<?php
class account
{
	const NO_TEAMS_ERROR    = 'You have not yet submitted a team';
	const WELCOME_OPENING   = '<strong>Hello %s</strong>, welcome to your account.<br />';
	const WELCOME_MESSAGE_1 = 'If your teams have not been performing too well lately, consider making some transfers. Click on <a href="teams.php" title="My Teams">My Teams</a> and then select which of your teams you would like to make transfers for. Then from the menu that appears on the left, select Transfers.';
	const WELCOME_MESSAGE_2 = 'Did you know you can submit upto 3 teams in total? If you would like to submit an additional team just click Submit a Team on the left.';
	const WELCOME_MESSAGE_3 = 'Like us on <a href="%s" title="Like Us On Facebook" target="_blank">Facebook</a> or Follow Us on <a href="%s" title="Follow Us On Twitter" target="_blank">Twitter</a> to help spread the word about %s. The more people that enter the bigger the prizes become.';
	const WELCOME_MESSAGE_4 = 'If you have any feedback to give us to help us improve %s or any comments to make in general then <a href="%s/help/contact-us.php" title="Contact Us">click here</a> to submit your comments.';
	const WELCOME_MESSAGE_5 = '[Team Name] are performing really well, with a bit of fine tuning in the transfers department you could be fighting for top honours come the end of the season.';
	
	private $userTeamDetails;
	private $formErrors;
	
	protected $userId;
	protected $db;
	protected $urlGenerator;
	protected $userFunctions;
	protected $userTeamIds;
	protected $userTeamNames;
	
	public $welcomeMessage;
		
	public function __construct($db, $ug, $uf)
	{
		$this->setUserId();
		$this->setWelcomeMessage();
		$this->setUserTeamDetails();
		
		$this->db            = $db;
		$this->urlGenerator  = $ug;
		$this->userFunctions = $uf;
	}
	
	private function setUserId()
	{
		if (isset($_COOKIE['user_id']))
		{
			$this->userId = $_COOKIE['user_id'];
		}
	}

	private function getFirstName()
	{
		$stmt = config::$mysqli->prepare("SELECT first_name 
		FROM members 
		WHERE user_id = ?
		LIMIT 1");
		$stmt->bind_param("i", $this->userId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($name);
		$stmt->fetch();
		$stmt->close();
		
		return $name;
	}
	
	private function setWelcomeMessage()
	{
		$this->welcomeMessage = sprintf(self::WELCOME_OPENING, $this->getFirstName());
		
		$num = Rand (1,4); 
		
		switch ($num)
		{
			case 1:
			$this->welcomeMessage .= self::WELCOME_MESSAGE_1;
			break;
			case 2:
			$this->welcomeMessage .= self::WELCOME_MESSAGE_2;
			break;
			case 3:
			$this->welcomeMessage .= sprintf(self::WELCOME_MESSAGE_3, config::FACEBOOK, config::TWITTER, config::SITE_NAME);
			break;
			case 4:
			$this->welcomeMessage .= sprintf(self::WELCOME_MESSAGE_4, config::SITE_NAME, config::$baseUrl);
		}
	}

	private function setUserTeamDetails()
	{
		$stmt = config::$mysqli->prepare("
		SELECT month_points,
			   total_points,
			   week_points,
			   ut.user_team_id, 
			   ut.user_team_name, 
			   ut.date
		FROM user_team_points utp
		INNER JOIN user_teams ut
		ON utp.user_team_id = ut.user_team_id
		WHERE ut.user_id = ?
		AND ut.date > ?
		GROUP BY ut.user_team_id
		");
		$stmt->bind_param("is", $this->userId, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();  
		$stmt->bind_result($monthPoints, $totalPoints, $weekPoints, $userTeamIds, $userTeamNames, $entryDates);
		
		while($stmt->fetch())
		{
			$this->userTeamIds[]     = $userTeamIds;
			$this->userTeamNames[]   = $userTeamNames;
			$this->userTeamDetails[] = array('monthPoints'  => $monthPoints,
											 'totalPoints'  => $totalPoints,
											 'weekPoints'   => $weekPoints,
											 'userTeamId'   => $userTeamIds,
											 'userTeamName' => $userTeamNames,
											 'entryDate'    => $entryDates);
		}
	}
	
	protected function createBoxLink($class, $link, $linkText)
	{
		echo '
		<a class="box-link '.$class.' stretch-child" href="'.config::$baseUrl.$link.'">
			'.$linkText.'
		</a>';
	}
	
	private function showSubmitTeamBoxLink()
	{
		$this->createBoxLink('dark-grey', '/my-account/submit-team.php', 'Submit a New Team');
	}
	
	public function showUserTeams()
	{
		if(empty($this->userTeamDetails))
		{
			echo notifications::showNotification('error', FALSE, self::NO_TEAMS_ERROR);
			$this->showSubmitTeamBoxLink();
			echo '<br class="clearfloat" />';
			return;
		}
		else
		{
			echo '<div class="stretch-parent">';
			foreach($this->userTeamDetails as $userTeamDetails)
			{
				echo '
				<a class="box-info stretch-child" href="'.$this->urlGenerator->makeUrl($userTeamDetails['userTeamName'], $userTeamDetails['userTeamId'], 'user_team').'">
					<span class="team-name">'.$userTeamDetails['userTeamName'].'</span>
					<span class="box-text">
						Total Points: <strong>'.$userTeamDetails['totalPoints'].'</strong><br />
						Month Points: <strong>'.$userTeamDetails['monthPoints'].'</strong><br />
						Week Points: <strong>'.$userTeamDetails['weekPoints'].'</strong><br />
						Transfers Remaining: <strong></strong>
					</span>
				</a>';
				
				unset($this->urlGenerator->linkString);
				unset($this->urlGenerator->urlId);
				unset($this->urlGenerator->urlType);
			}
		
			if(count($this->userTeamDetails) < config::MAX_USER_TEAMS)
			{
				$this->showSubmitTeamBoxLink();
			}
			
			echo '<span class="stretch"></span>';
			echo '</div>';
		}
		return;
	}
}
?>
