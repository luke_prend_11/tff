<?php
class stats
{
    protected $urlGenerator;
    protected $urlId;
    
    public function __construct($ug)
    {
        $this->urlGenerator = $ug;
        
        $this->checkUrlHasParameter();
    }
    
    private function checkUrlHasParameter()
    {
        if(isset($_GET) && !empty($_GET))
        {
            $this->setIdFromUrl();
        }
    }

    private function setIdFromUrl()
    {
        foreach($_GET as $key => $value)
        {
            $urlKey = $key;
        }
        
        switch($urlKey)
        {
            case 'comp_id':
                $this->urlId = htmlspecialchars($_GET['comp_id']);
                break;
            case 'team_id':
                $this->urlId = htmlspecialchars($_GET['team_id']);
                break;
            case 'user_team_id':
                $this->urlId = htmlspecialchars($_GET['user_team_id']);
                break;
            case 'fixture_id':
                $this->urlId = htmlspecialchars($_GET['fixture_id']);
                break;
        }
    }
    
    public function displayStatsPage()
    {
        echo '
        <div class="row stretch-parent">
            <div class="col-2-x-3 stretch-child">
                <div class="very-light-grey">
                    <h2 class="news dark-grey">League Tables</h2>
                    <div class="inner-container">
                    </div>
                </div>
            </div>

            <div class="col-1-x-2 stretch-child very-light-grey">
                <h2 class="news dark-grey">Matches</h2>
                <div class="inner-container">
                </div>
            </div>
            <span class="stretch"></span>
        </div>';
    }
}