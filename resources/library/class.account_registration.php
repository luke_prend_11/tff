<?php
class account_registration
{
	const WELCOME_MESSAGE        = 'Registration to %s is completely FREE and only takes 2 minutes. Enter a few details below to create your account, you will then be directed to make your team selections. All fields are required unless stated.<br /><br />If you already have an account <a href="%s/login.php" title="Log in to Your Account">click here</a> to log in.';
	const NO_FIRST_NAME_ERROR    = "Enter your first name";
	const NO_LAST_NAME_ERROR     = "Enter your last name";
	const NO_GENDER_ERROR        = "Select your gender";
	const NO_LOCATION_ERROR      = "Select your location";
	const NO_EMAIL_ERROR         = "Enter a valid email address";
	const EMAIL_MATCH_ERROR      = "The email addresses you have entered don't match";
	const EMAIL_REGISTERED_ERROR = "An account has already been registered to the email address provided";
	const NO_PASSWORD_ERROR      = "Enter a password with between 6 and 15 characters";
	const PASSWORD_MATCH_ERROR   = "The passwords you have entered don't match";
	const REFERRAL_ID_ERROR      = "Referral ID's can only contain numbers";
	const USER_LOCATIONS         = array('Buckinghamshire', 'Cambridgeshire', 'Cumbria', 'Derbyshire', 'Devon', 'Dorset', 'East Sussex', 'Essex', 'Gloucestershire', 'Hampshire', 'Hertfordshire', 'Kent', 'Lancashire', 'Leicestershire', 'Lincolnshire', 'Norfolk', 'Northamptonshire', 'North Yorkshire', 'Nottinghamshire', 'Oxfordshire', 'Somerset', 'Staffordshire', 'Suffolk', 'Surrey', 'Warwickshire', 'West Sussex', 'Worcestershire', 'Other');
	
	private $userFunctions;
	private $database;
	private $welcomeMessage;
	private $firstName;
	private $lastName;
	private $gender;
	private $location;
	private $refferalId;
	
	public $emailAddress1;
	public $emailAddress2;
	public $password1;
	public $password2;
	public $seededPassword;
	public $formErrors;
		
	public function __construct($uf, $db)
	{
		$this->userFunctions = $uf;
		$this->database      = $db;
		
		$this->setWelcomeMessage();
	}
	
	public function registrationSuccessAction()
	{
		if (isset($_POST['submit_form']))
		{
			$this->setRegistrationFormFields();
			$this->checkRegistrationFormFields();
			
			if(empty($this->formErrors))
			{
				$this->registrationSuccess();
			}
		}
	}
	
	public function registrationPageDisplay()
	{
		if (isset($_POST['submit_form']))
		{
			if(!empty($this->formErrors))
			{
				echo notifications::showNotification('error', $this->formErrors);
				$this->showRegistration();
			}
		}
		else
		{
			$this->showRegistration();
		}
	}

	private function setWelcomeMessage()
	{
		$this->welcomeMessage = sprintf(self::WELCOME_MESSAGE, config::SITE_NAME, config::$baseUrl);
	}
	
	private function registrationForm()
	{ 
		echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
			<h2>Player Information</h2>
			<div>'
				.$this->userFunctions->showStickyForm('text','first_name')
				.$this->userFunctions->showStickyForm('text','last_name')
				.$this->userFunctions->showStickyForm('select','gender',array('Male','Female'))
				.$this->userFunctions->showStickyForm('select','location',self::USER_LOCATIONS).
			'</div>
			<div>'
				.$this->userFunctions->showStickyForm('text','email_address')
				.$this->userFunctions->showStickyForm('text','confirm_email','',true)
				.$this->userFunctions->showStickyForm('password','password')
				.$this->userFunctions->showStickyForm('password','confirm_password').
			'</div>
			<!--<div>'
			  	.$this->userFunctions->showStickyForm('text','referred_by').
			'</div>-->
			
			<input type="submit" value="Submit" name="submit_form" class="button blue" />
		</form>';
	}
	
	private function showRegistration()
	{
		echo '<p>'.$this->welcomeMessage.'</p>';
		$this->registrationForm();
	}
	
	private function setRegistrationFormFields()
	{
		$this->firstName     = ucwords($_POST['first_name']);
		$this->lastName      = ucwords($_POST['last_name']);
		$this->gender        = $_POST['gender'];
		$this->location      = $_POST['location'];
		$this->emailAddress1 = $_POST['email_address'];
		$this->emailAddress2 = $_POST['confirm_email'];
		$this->password1     = $_POST['password'];
		$this->password2     = $_POST['confirm_password'];
		$this->refferalId    = isset($_POST['referred_by']) ? $_POST['referred_by'] : '';
	}
	
	private function checkRegistrationFormFields()
	{
		if (empty($this->firstName)) {
			$this->formErrors[] = self::NO_FIRST_NAME_ERROR;
		}
		if (empty($this->lastName)) {
			$this->formErrors[] = self::NO_LAST_NAME_ERROR;
		}
		if (empty($this->gender)) {
			$this->formErrors[] = self::NO_GENDER_ERROR;
		}
		if (empty($this->location)) {
			$this->formErrors[] = self::NO_LOCATION_ERROR;
		}
		
		$this->checkEmail();
		$this->checkPassword();
		
		if (!empty($this->refferalId) && !is_numeric($this->refferalId)) {
			$this->formErrors[] = self::REFERRAL_ID_ERROR;
		}
	}
	
	public function checkEmail()
	{
		if (!$this->userFunctions->valid_email($this->emailAddress1)) {
			$this->formErrors[] = self::NO_EMAIL_ERROR;
		}
		if ($this->emailAddress1 != $this->emailAddress2) {
			$this->formErrors[] = self::EMAIL_MATCH_ERROR;
		}		
		if ($this->userFunctions->userExists($this->emailAddress1)) {
			$this->formErrors[] = self::EMAIL_REGISTERED_ERROR;
		}
	}
	
	public function checkPassword()
	{
		if (!$this->userFunctions->valid_password($this->password1)) {
			$this->formErrors[] = self::NO_PASSWORD_ERROR;
		}
		if ($this->password1 != $this->password2) {
			$this->formErrors[] = self::PASSWORD_MATCH_ERROR;
		}
	}
	
	private function insertNewUser()
	{
		$this->seededPassword = $this->setSeededPassword($this->password1);
		
		if ($this->database->completeInsert('members', 
											array('first_name', 'last_name', 'gender', 'location', 'password', 'email', 'referral', 'registration_date'), 
											array($this->firstName, 
												  $this->lastName, 
												  $this->gender, 
												  $this->location, 
												  $this->seededPassword, 
												  $this->emailAddress1, 
												  $this->refferalId, 
												  config::$curDate), 
											array('s','s','s','s','s','s','i','s'))) 
		{
			if ($this->sendActivationEmail())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public function setSeededPassword($password)
	{
		return sha1($password.login::$seed);
	}
	
	private function sendActivationEmail()
	{
		$m = new mail();
		
		$message = "Thank you for registering on ".config::SITE_NAME.
		
		"Your account information you will need to login with:
		
		Email Address: ".$this->emailAddress1.
		"Password: ".$this->password1.
		
		"If you have received this message in error then please send an email to ".config::CONTACT_EMAIL.
		
		"Kind Regards"
		.config::SITE_NAME." Administration";
		
		if ($m->sendMail($this->emailAddress1, config::SITE_NAME." Registration", $message, config::SITE_NAME." <no_reply@".config::DOMAIN.">"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private function registrationSuccess()
	{
		$lg = new login();
		
		$this->insertNewUser();
		// set cookies to keep user logged in
		$lg->setUserCookie($this->database->insertId, $this->emailAddress1);
		
		// create a session variable to display just registered message on submit team page
		$_SESSION['just_registered'] = true;
		
		// Insert data into user_login table to keep for records
		$lg->insertLogin($this->database->insertId);
		
		header('Location: '.config::$baseUrl.'/my-account/submit-team.php');
	}
}
?>