<?php
class config extends config_base
{
    const BASE_URL_TEST            = "http://localhost/tff"; // http://localhost:8888/tff
    const BASE_URL_LIVE            = "http://www.thefootballfrenzy.co.uk";
    const BASE_URL_LIVE_TESTING    = "http://uk.ezeeinternet.net/~vjrnrejl/test/tff";
    const DOMAIN                   = "thefootballfrenzy.co.uk";
    const SITE_NAME                = "The Football Frenzy";
    const REMOVE_FROM_BASE_URL     = array("tff","new_site","~vjrnrejl","test");
    const PAGE_TITLE_SEPARATOR     = " | ";
    const CONTACT_EMAIL            = "webmaster@thefootballfrenzy.co.uk";
    const FACEBOOK                 = "http://www.facebook.com/pages/The-Football-Frenzy/125997104158911";
    const TWITTER                  = "https://twitter.com/TheFootyFrenzy";
    const BUDGET                   = 5700; // budget in £millions
    const MAX_USER_TEAMS           = 3;
    const MAX_SEASON_TRANSFERS     = 40;
    const MAX_TRANSFERS_PRE_SEASON = 9999;
    const DEFAULT_TRANSFER_OUT     = "2100-01-01 00:00:01";
    const FANTASY_FOOTBALL_ACTIVE  = TRUE;
    const NEWS_ACTIVE              = FALSE;

    private $websiteCreds;
    
    public static $curSeasonId;
    public static $curSeasonName;
    public static $curSeasonStart;
    public static $curSeasonEnd;
    public static $curSeasonOpeningFixtureDate;
    public static $maxTransfers;
    public static $rowClass = 0;
	
    public function __construct()
    {
        // set website credentials
        $this->setWebsiteCreds();
        
        parent::__construct(self::BASE_URL_TEST, self::BASE_URL_LIVE, self::BASE_URL_LIVE_TESTING, self::REMOVE_FROM_BASE_URL, $this->websiteCreds);

        // set current season credentials
        $this->setCurrentSeasonCredentials();

        // set season opening fixture to get maximum number of transfers
        $this->setSeasonOpeningFixture();

        // set maximum user transfers
        $this->setMaxTransfers();
    }
    
    private function setWebsiteCreds()
    {
        $this->websiteCreds = array(
            "db" => array(
                "live" => array(
                    "name"   => "vjrnrejl_tff",
                    "username" => "vjrnrejl_luke",
                    "password" => "Password1",
                    "host"     => "localhost"
                ),
                "test" => array(
                    "name"      => "tff",
                    "username"    => "root",
                    "password"    => "", // IB1333143435
                    "passwordmac" => "root",
                    "host"        => "localhost"
                )
            )
        );
    }
	
    private function setCurrentSeasonCredentials()
    {
        $stmt = self::$mysqli->prepare("SELECT season_id, season_start, season_end, season_name
        FROM tff_seasons
        WHERE current = 1
        LIMIT 1");
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($seasonId, $seasonStart, $seasonEnd, $seasonName);
        $stmt->fetch();
        $stmt->close();

        self::$curSeasonId    = $seasonId;
        self::$curSeasonName  = $seasonName;
        self::$curSeasonStart = $seasonStart;
        self::$curSeasonEnd   = $seasonEnd;
    }
	
    private function setSeasonOpeningFixture()
    {
        self::$curSeasonOpeningFixtureDate = $this->getFirstSeasonFixture() ? $this->getFirstSeasonFixture() : $this->getFirstFridayAugust();
    }
	
    private function getFirstSeasonFixture()
    {
        $stmt = self::$mysqli->prepare("
        SELECT MIN(f.date)
        FROM match_fixtures f
        INNER JOIN tff_seasons s
        ON s.season_start < f.date AND s.season_end > f.date
        WHERE s.current = 1
        LIMIT 1");
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($fixtureDate);
        $stmt->fetch();
        $stmt->close();

        return $fixtureDate;
    }
	
    private function getFirstFridayAugust()
    {
        date_default_timezone_set("Europe/London");
        return date('Y-m-d 19:45:00', strtotime('First Friday of '.date('Y', strtotime(self::$curSeasonStart)).'-08'));
    }
	
    private function setMaxTransfers()
    {
        self::$maxTransfers = self::$curDate < self::$curSeasonOpeningFixtureDate ? self::MAX_TRANSFERS_PRE_SEASON : self::MAX_SEASON_TRANSFERS;
    }
	
    public static function updateRowClass()
    {
        self::$rowClass = 1 - self::$rowClass;
    }
}
