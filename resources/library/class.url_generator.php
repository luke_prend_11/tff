<?php
class url_generator
{
	const URL_CHARACTERS_TO_REMOVE       = '#[^-a-zA-Z0-9_ ]#';
	const URL_CHARACTERS_TO_REPLACE      = '#[-_ ]+#';
	const URL_CHARACTERS_TO_REPLACE_WITH = '-';
	
	// needs to be public as used in navigation class
	public $linkString;
	public $urlFolder;
	public $urlParameter;
	public $urlTitle;
	
	// for use with user teams only to generate the correct link if user is logged in
	private $seasonId;
	private $userId;
	private $trailingSlash;
	
	public function __construct()
	{
		if (isset($_COOKIE['user_id']))
		{
			$this->userId   = $_COOKIE['user_id'];
			$this->seasonId = config::$curSeasonId;
		}
	}
	
	public function showLink($linkString, $urlId, $urlType, $additionalLinkText = '', $folderPath = FALSE)
	{
		$this->setUrlParmaeters($linkString, $urlId, $urlType, $folderPath);
		
		if(($this->urlType == 'team' && $this->checkTeamInfo()) || ($this->urlType != 'team' && !empty($this->urlId)))
		{
			return '
			<a href="'.$this->makeUrl().'" title="'.$this->linkString.' '.$this->urlTitle.'">'.
				$this->linkString.$additionalLinkText.'
			</a>
			';
		}
		elseif($this->urlType == 'team' && !$this->checkTeamInfo())
		{
			return $this->linkString;
		}
		else
		{
			echo notifications::showNotification('error', FALSE, 'Error generating the link');
		}
	}
	
	public function makeUrl($linkString = FALSE, $urlId = FALSE, $urlType = FALSE, $folderPath = FALSE)
	{
		if(!empty($linkString) && !empty($urlId) && !empty($urlType))
		{
			$this->setUrlParmaeters($linkString, $urlId, $urlType, $folderPath);
		}
		
		if($this->urlType == 'user_team')
		{
			$this->setUserTeamInfo();
		}
		
		$this->prepareUrlText();
		
		return config::$baseUrl.'/'.$this->urlFolder.'/'.$this->urlString.'-'.$this->urlParameter.$this->urlId.$this->trailingSlash;
	}
	
	private function setUrlParmaeters($linkString, $urlId, $urlType, $folderPath)
	{
		$this->linkString = $linkString;
		$this->urlId      = $urlId;
		$this->urlType    = $urlType;
		
		switch ($this->urlType)
		{
			case "private_league":
				$this->urlFolder    = 'my-account/leagues';
				$this->urlParameter = 'P';
				$this->trailingSlash = '';
				$this->urlTitle     = 'Leaderboard';
				break;
			case "league_table":
				$this->urlFolder    = 'stats';
				$this->urlParameter = 'C';
				$this->trailingSlash = '';
				$this->urlTitle     = 'Table';
				break;
			case "user_team":
				$this->setUserTeamInfo();
				$this->urlFolder     = $this->checkUserTeamCurrentSeason() && ((isset($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) && $this->userId == $_COOKIE['user_id']) ? 'my-account/teams' : 'stats';
				$this->trailingSlash = $this->checkUserTeamCurrentSeason() && ((isset($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) && $this->userId == $_COOKIE['user_id']) ? '/' : '';
				$this->urlParameter  = 'U';
				$this->urlTitle      = 'Stats';
				break;
			case "team":
				$this->urlFolder    = 'stats';
				$this->trailingSlash = '';
				$this->urlParameter = 'T';
				$this->urlTitle     = 'Stats';
				break;
			case "news":
				$this->urlFolder    = 'news';
				$this->trailingSlash = '';
				$this->urlParameter = 'A';
				$this->urlTitle     = 'Article';
				break;
			case "match":
				$this->urlFolder    = 'stats/matches/'.str_replace("-","",$folderPath);
				$this->trailingSlash = '';
				$this->urlParameter = 'M';
				$this->urlTitle     = 'Match';
				break;
		}
	}
	
	// needs to be public as used in navigation class
	public function prepareUrlText()
	{
		$this->urlString = preg_replace(self::URL_CHARACTERS_TO_REMOVE, '', $this->linkString);
		$this->urlString = trim($this->urlString);
		$this->urlString = preg_replace(self::URL_CHARACTERS_TO_REPLACE, self::URL_CHARACTERS_TO_REPLACE_WITH, $this->urlString);
		$this->urlString = strtolower($this->urlString);
	}
	
	private function setUserTeamInfo()
	{
		$stmt = config::$mysqli->prepare("SELECT user_id, season_id FROM user_teams WHERE user_team_id = ? LIMIT 1");
		$stmt->bind_param("i", $this->urlId);
		$stmt->execute();
		$stmt->store_result();
		
		if ($stmt->num_rows != 0)
		{
			$stmt->bind_result($this->userId, $this->seasonId);
			$stmt->fetch();
			$stmt->close();
			return true;
		}
		$stmt->close();
		return false;
	}
	
	private function checkUserTeamCurrentSeason()
	{
		if($this->seasonId == config::$curSeasonId)
		{
			return true;
		}
		return false;
	}
	
	private function checkTeamInfo()
	{
		$stmt = config::$mysqli->prepare("SELECT * FROM team_info WHERE team_id = ? LIMIT 1");
		$stmt->bind_param("i", $this->urlId);
		$stmt->execute();
		$stmt->store_result();
		
		if ($stmt->num_rows != 0)
		{
			$stmt->close();
			return true;
		}
		$stmt->close();
		return false;
	}
}
?>