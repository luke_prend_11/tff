<?php
class matches_page extends stats
{
    private $competitions;
    private $matches;
    
    public function __construct($ug, $uf)
    {
        parent::__construct($ug);
        
        $this->competitions = new stats_competitions($ug, $uf);
        $this->matches      = new matches($ug, $uf);
        
        $this->setMatchInformation();
    }
    
    private function setMatchInformation()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            h.team_name,
            h.team_id,
            a.team_name,
            a.team_id,
            f.date,
            f.comp_id,
            c.comp_name,
            c.comp_type
        FROM match_fixtures f
        INNER JOIN competitions c
        ON c.comp_id = f.comp_id
        INNER JOIN team_names h 
        ON h.team_id = f.ht_id
        INNER JOIN team_names a 
        ON a.team_id = f.at_id
        WHERE f.fixture_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $this->urlId);
        $stmt->bind_result(
            $this->homeTeamName,
            $this->homeTeamId,
            $this->awayTeamName,
            $this->awayTeamId,
            $this->matchDate,
            $this->matchCompId,
            $this->competitionName,
            $this->competitionType
            );
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
    }
    
    public function displayMatchesPage()
    {
        $matchDate = DateTime::createFromFormat('Y-m-d', $this->matchDate)->format('l j F Y');
        
        echo '
        <span class="match-date">'.$matchDate.'</span>
        <div class="row stretch-parent">
            <div class="col-2-x-3 stretch-child">
                <div class="very-light-grey">
                    <h2 class="news dark-grey">'.$this->homeTeamName.' Matches</h2>
                    <div class="inner-container">'.
                        $this->matches->displayMatches('all', NULL, 10, $this->homeTeamId, FALSE).'
                    </div>
                </div>
                
                <div class="very-light-grey">
                    <h2 class="news dark-grey">'.$this->awayTeamName.' Matches</h2>
                    <div class="inner-container">'.
                        $this->matches->displayMatches('all', NULL, 10, $this->awayTeamId, FALSE).'
                    </div>
                </div>
            </div>

            <div class="col-1-x-2 stretch-child very-light-grey">
                <h2 class="news dark-grey">League Table</h2>
                <div class="inner-container">'.
                    $this->competitions->showLeagueTable('short', 'season', NULL, $this->matchCompId, array($this->homeTeamId, $this->awayTeamId)).'
                    <a href="'.$this->urlGenerator->makeUrl($this->competitionName, $this->matchCompId, 'league_table').'" title="Full '.$this->competitionName.' Table" class="btn grey">Full Table</a>
                </div>
            </div>
            <span class="stretch"></span>
        </div>';
    }
}