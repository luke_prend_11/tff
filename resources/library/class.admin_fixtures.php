<?php
class admin_fixtures
{
    // list of arrays that will be used to store data
    public $header_row    = array();
    public $import_data   = array();
    public $skipped_data  = array();
    public $success_items = array();
    public $failed_items  = array();

    // list of public variables that will be used as counters
    public $fixtures_total   = 0;
    public $fixtures_import  = 0;
    public $fixtures_skipped = 0;
    public $import_success   = 0;
    public $import_failed    = 0;

    public function __construct()
    {
        $this->getPageId();
    }

    private function getPageId()
    {
        if (isset($_GET['id']) && !empty($_GET['id']))
        {
            $_SESSION['fix_page_id'] = htmlspecialchars($_GET['id']);
        }
    }
	
    public function showInsertFixturesPage()
    {
        echo'
        <a href="fixtures.php?id=1" title="Browse Fixtures" class="btn blue">Browse Fixtures to Update</a>
        <a href="fixtures.php?id=2" title="Select a Fixture" class="btn dark-grey">Select a Fixture to Update</a>
        <a href="fixtures.php?id=3" title="Insert a New Fixture" class="btn orange">Insert a New Fixture</a>
        <a href="fixtures.php?id=4" title="Insert Fixtures from CSV" class="btn light-grey">Insert Fixtures from CSV</a>
        ';
    }
	
    public function showSelectFixtureComp($matches)
    {
        $matchQuery = $matches == 'fixtures' ? 'WHERE results.fixture_id IS NULL' : 'WHERE results.fixture_id IS NOT NULL';

        $stmt = config::$mysqli->prepare("
        SELECT comp_name
        FROM match_fixtures f
        LEFT JOIN match_results r
        ON f.fixture_id = r.fixture_id
        LEFT JOIN competitions c
        ON f.comp_id = c.comp_id
        ".$matchQuery."
        GROUP BY c.comp_id
        ORDER BY c.comp_id ASC
        ");
        $stmt->execute();
        $stmt->store_result();

        echo '
        <form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
            <div class="very-light-grey">
                <div>
                    <label title="select_comp">Select Competition</label>
                    <select name="comp">';

                    if ($stmt->num_rows == 0)
                    {
                        echo "<option>There are currently no fixtures to modify.</option>";
                    }
                    else
                    {
                        $stmt->bind_result($comp_name);
                        while ($stmt->fetch())
                        {
                                echo '<option>'.$comp_name.'</option>';
                        }
                    }
                    $stmt->close();

                    echo '</select>
                </div>
            </div>

            <input name="select_comp" type="submit" value="Submit" class="button" />
        </form>';
    }
	
    public function showSelectFixtures($comp)
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            home.team_id AS home_id,
            home.team_name AS home_team,
            away.team_id AS away_id,
            away.team_name AS away_team,
            fixtures.fixture_id,
            fixtures.date
        FROM match_fixtures f
        LEFT JOIN teams home
        ON f.ht_id = home.team_id
        LEFT JOIN teams away
        ON f.at_id = away.team_id
        LEFT JOIN match_results r
        ON f.fixture_id = r.fixture_id
        LEFT JOIN competitions c
        ON f.comp_id = c.comp_id
        WHERE c.comp_name = ?
        AND r.fixture_id IS NULL
        GROUP BY f.fixture_id
        ORDER BY DATE(f.date) ASC, c.comp_id ASC, f.date ASC, home_team ASC
        ");
        $stmt->bind_param("s", $comp);
        $stmt->execute();
        $stmt->store_result();

        echo '<form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">

        <div class="very-light-grey">
            <div>
                <label title="select_fixture">Select Fixture</label>
                <select name="fixture">';
                if ($stmt->num_rows == 0)
                {
                    echo "<option>There are currently no fixtures to modify.</option>";
                }
                else
                {
                    $stmt->bind_result($home_id, $home_team, $away_id, $away_team, $fixture_id, $date);
                    while($stmt->fetch())
                    {
                        echo '<option value="'.$fixture_id.'">'.date("d/m/y H:i", strtotime($date)).', '.$home_team.' vs '.$away_team.'</option>';
                    }
                }
                $stmt->close();
                echo '</select>
            </div>
        </div>

        <input name="select_fixture" type="submit" value="Submit" class="button" />
        <input name="reset_fixture" type="submit" value="Reset" class="button dark-grey" />
    </form>';
    }
	
    public function showModifyFixtures($fix_id = 0)
    {
        $fixQuery   = $fix_id == 0 ? '<>' : '=';
        $submitName = $fix_id == 0 ? 'submit_fixtures' : 'submit_fixture';

        $stmt = config::$mysqli->prepare("
        SELECT 
            h.team_id AS home_id,
            h.team_name AS home_team,
            a.team_id AS away_id,
            a.team_name AS away_team,
            f.fixture_id,
            f.date,
            c.comp_name
        FROM match_fixtures f
        LEFT JOIN team_names h
        ON f.ht_id = h.team_id
        LEFT JOIN team_names a
        ON f.at_id = a.team_id
        LEFT JOIN match_results r
        ON f.fixture_id = r.fixture_id
        LEFT JOIN competitions c
        ON f.comp_id = c.comp_id
        LEFT JOIN tff_seasons s
        ON f.date > s.season_start AND f.date < s.season_end
        WHERE r.fixture_id IS NULL
        AND f.fixture_id ".$fixQuery." ?
        AND s.current = 1
        GROUP BY f.fixture_id
        ORDER BY DATE(f.date) ASC, c.comp_id ASC, f.date ASC, home_team ASC
        LIMIT 15
        ");
        $stmt->bind_param("s", $fix_id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 0)
        {
            echo notifications::showNotification("error", TRUE, "There are currently no fixtures to modify for the current season.");
        }
        else
        {
            echo '
            <form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
                <table>
                    <thead>
                      <tr>
                        <th><span>New Date
                        <th><span>Date
                        <th><span>Competition
                        <th class="align-right"><span>Home Team
                        <th>&nbsp;
                        <th><span>Away Team
                    <tbody>';
                    $i = 0;
                    $row_class = 0;
                    $stmt->bind_result($home_id, $home_team, $away_id, $away_team, $fixture_id, $date, $comp_name);
                    while ($stmt->fetch())
                    {
                        echo '
                        <tr class="table-row'.$row_class.'">
                            <input name="fixture_id['.$i.']" value="'.$fixture_id.'" type="hidden" />
                            <input name="old_fixture_date['.$i.']" value="'.$date.'" type="hidden" />
                            <td>
                                    <input name="fixture_date['.$i.']" type="datetime" value="'; 
                                    if(isset($_POST[$submit_name]) && isset($_POST['fixture_date'][$i])) echo $_POST['fixture_date'][$i]; 
                                    else echo date("Y-m-d H:i", strtotime($date)); 
                                    echo '" />
                            <td>'.date("Y-m-d H:i", strtotime($date)).'
                            <td>'.$comp_name.'
                            <td class="align-right">'.$home_team.'
                            <td class="align-center">vs
                            <td>'.$away_team;
                            $i++;
                            $row_class = 1 - $row_class;
                    }
                    $stmt->close();

                echo '</table>
                <input name="'.$submitName.'" type="submit" value="Submit" class="button" />';
                if($fix_id != 0)
                {
                    echo '<input name="reset_fixture" type="submit" value="Reset" class="button" />';
                }
                echo '</form>';
        }
    }
	
	public function checkUpdateFixtures($fixture_id, $fixture_date, $old_fixture_date)
	{
		$fixturesAry = array();
		foreach(array_keys($fixture_id) as $n) {
			//Unset any rows that do not contain new fixture dates
			if (empty($fixture_date[$n]) || (date('Y-m-d H:i:s', strtotime($fixture_date[$n])) == date('Y-m-d H:i:s', strtotime($old_fixture_date[$n])))) {
				unset($fixturesAry[$n]);	
			}
			else {
				$fd = strtotime($fixture_date[$n]);
				if ($fd === false) {
					echo '<p class="error">Fixture date '.$fixture_date[$n].' is invalid</p>';
					return false;
				}
				$id = intval($fixture_id[$n]);
				$date = date('Y-m-d H:i:s', strtotime($fixture_date[$n]));
				$fixturesAry[] = array($id, $date);
			}
		}
		
		if(empty($fixturesAry)) {
			echo '<p class="error">You have not submitted any changes to fixtures</p>';
			return false;
		}
		else {
			$stmt = config::$mysqli->prepare("UPDATE match_fixtures SET date = ?
			WHERE fixture_id = ?");
			$stmt->bind_param("si", $date, $id);
					
			foreach ($fixturesAry as $values) {
				$id = $values[0];
				$date = $values[1];
				$stmt->execute();
			}
			$stmt->close();
			return true;
		}
	}
	
    public function processCSV($csv_file_name) {
		// open the csv file in read only mode 'r'
		$csv_file = fopen($csv_file_name, 'r');
		if($csv_file !== FALSE) {
			
			// select the header row
			$header = fgetcsv($csv_file, 0);
			// store the header row for use later
			$this->header_row[] = $header;
			
			// while end of file has not been reached execute the following code
			while(!feof($csv_file)) {
				// get the rows of data from the file with no maximum line length to ensure no interuption
				$fixtures = fgetcsv($csv_file, 0);
				
				// add 1 to fixture count
				if($this->fixtureCount($fixtures)) {
					$this->fixtures_total++;
				}
				
				// add 1 to fixtures import and store in array for inserting
				if($this->checkImport($fixtures)) {
					$this->fixtures_import++;
					$this->import_data[] = $fixtures;
				}
				else {
					$this->fixtures_skipped++;
					$this->skipped_data[] = $fixtures;
				}
					
			}
		}
		// close the csv file
		fclose($csv_file);
    }
	
	// prepare the data to be inserted
	public function insertQuery($import_data) {
		$db = new database();
		
		foreach ($import_data as $values) {
			if($db->completeInsert(config::$mysqli, 'fixtures', array('comp_id', 'ht_id', 'at_id', 'date', 'round'), array($values[1], $values[2], $values[3], $values[4], $values[5]), array('i','i','i','s','s'))) {
				$this->import_success++;
				$this->success_items[] = $values;
			}
			else {
				$this->import_failed++;
				$this->failed_items[] = $values;
			}
		}
	}

	// use the header row and relevant fixture array to create a table to display
	public function fixtureList($header, $fixtureData) {
		$html = '<table>';
		// arrange the header row fields
		foreach ($header as $values) {
			$html .= '
			<tr>
				<td>'.$values[0].'</td>
				<td>'.$values[1].'</td>
				<td>'.$values[2].'</td>
				<td>'.$values[3].'</td>
				<td>'.$values[4].'</td>
				<td>'.$values[5].'</td>
			</tr>
			';
		}
		
		// check to see if fixture array is empty
		$errors = array_filter($fixtureData);
		// if it is then display a message
		if (empty($errors)) {
			$html .= '
			<tr>
				<td colspan="6">There are no fixtures to diplay</td>
			</tr>
			';
		}
		// else arrange the data row fields
		else {
			foreach ($fixtureData as $values) {
				$html .= '
				<tr>
					<td>'.$values[0].'</td>
					<td>'.$values[1].'</td>
					<td>'.$values[2].'</td>
					<td>'.$values[3].'</td>
					<td>'.$values[4].'</td>
					<td>'.$values[5].'</td>
				</tr>
				';
			}
		}
		$html .= '</table>';
		// return the concatenated data
		return $html;
	}
	
	// count the fixtures by counting each row that has an entry for the comp_id field
	private function fixtureCount($fixtures) {
		if($fixtures[1] != null) {
			return true;
		}
    }	
	
	// fixture id field needs to be empty as it is an autoincrement value and ht_id, at_id, comp_id and date should all have values
	private function checkImport($fixtures) {
		if($fixtures[0] == null && $fixtures[1] != null && $fixtures[4] != null && $fixtures[3] != null && $fixtures[4] != null) {
			return true;
		}
		else {
			return false;
		}
    }
}
?>