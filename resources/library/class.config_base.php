<?php
class config_base
{
    private $host;
    private $username;
    private $password;
    private $database;

    public static $curDate;
    public static $testingMode;
    public static $baseUrl;
    public static $currentPageRoute;
    public static $mysqli;

    public function __construct($testUrl, $liveUrl, $liveTestingUrl, $removeFromUrl, $websiteCreds)
    {
        // start a session
        $this->sessionStart();

        // set current date credentials
        $this->setCurrentDateCredentials();
        
        //set testing mode
        $this->setTestingMode();
        
        // set base url
        $this->setBaseUrl($testUrl, $liveUrl, $liveTestingUrl);
        
        // set current page
        $this->setCurrentPage($removeFromUrl);

        // set database credentials
        $this->setDatabaseCredentials($websiteCreds);

        // set connection to the database
        $this->setDatabaseConnection();
    }

    private function sessionStart()
    {
        // Check if session has already been started
        if (session_status() == PHP_SESSION_NONE)
        {
            $session_name = 'sec_session_id'; // Set a custom session name
            $secure       = false; // Set to true if using https.
            $httponly     = true; // This stops javascript being able to access the session id. 

            ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
            $cookieParams = session_get_cookie_params(); // Gets current cookies params.
            session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
            session_name($session_name); // Sets the session name to the one set above.
            session_start(); // Start the php session
            //session_regenerate_id(true); // Regenerated the session, delete the old one.    
        }
    }

    private function setCurrentDateCredentials()
    {
        $timezone      = new DateTimeZone('Europe/London'); 
        $date          = new DateTime(null, $timezone);  
        self::$curDate = $date->format('Y-m-d H:i:s');
    }
    
    private function setTestingMode()
    {
        self::$testingMode = strpos($_SERVER['HTTP_HOST'], 'localhost') !== FALSE ? TRUE : FALSE;
    }
    
    private function setBaseUrl($testUrl, $liveUrl, $liveTestingUrl)
    {
        self::$baseUrl = self::$testingMode !== FALSE ? $testUrl : (strpos($liveTestingUrl, $_SERVER['SERVER_NAME']) ? $liveTestingUrl : $liveUrl);
    }

    protected function setCurrentPage($alsoToRemove)
    {
        self::$currentPageRoute = $_SERVER['PHP_SELF'];
        
        // check for any additional items to replace - typically when testing on localhost
        if(!empty($alsoToRemove))
        {
            self::$currentPageRoute = str_replace($alsoToRemove, "", self::$currentPageRoute);
        }
        
        // finally replace any regular characters to leave only the page route i.e leaderboard_index
        $toReplace   = array("/",".php","-","___","__");
        $replaceWith = array("_","","_","_","_");

        self::$currentPageRoute = substr(str_replace($toReplace, $replaceWith, self::$currentPageRoute),1);
    }
	
    protected function setDatabaseCredentials($websiteCreds)
    {
        $this->host     = self::$testingMode !== FALSE ? $websiteCreds['db']['test']['host'] : $websiteCreds['db']['live']['host'];
        $this->username = self::$testingMode !== FALSE ? $websiteCreds['db']['test']['username'] : $websiteCreds['db']['live']['username'];
        $this->password = self::$testingMode !== FALSE ? $websiteCreds['db']['test']['password'] : $websiteCreds['db']['live']['password'];
        $this->database = self::$testingMode !== FALSE ? $websiteCreds['db']['test']['name'] : $websiteCreds['db']['live']['name'];
    }

    protected function setDatabaseConnection()
    {
        if (!isset(self::$mysqli))
        {
            self::$mysqli = new mysqli($this->host, $this->username, $this->password, $this->database);
        }
        // check for database connection
        if (self::$mysqli->connect_errno)
        {
            printf("Connect failed: %s\n", self::$mysqli->connect_error);
            exit();
        }
    }

    public static function printArray($array)
    {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
        exit;
    }
}
