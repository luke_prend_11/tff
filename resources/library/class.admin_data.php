<?php
class admin_data
{
    protected $database;
    private $csvData;
    
    private $csvRowCount          = 0;
    private $csvImportCount       = 0;
    private $csvSkippedCount      = 0;
    protected $importSuccessCount = 0;
    protected $importFailedCount  = 0;

    private $csvHeaderRow        = array();
    private $csvSkippedData      = array();
    protected $importSuccessData = array();
    protected $importFailedData  = array();
    protected $csvImportData     = array();
    
    public function __construct($db)
    {
        $this->database = $db;
    }
    
    protected function processCSV($csvFileName)
    {
        // open the csv file in read only mode 'r'
        $csvFile = fopen($csvFileName, 'r');
        
        if($csvFile !== FALSE)
        {
            // select the header row and store for later
            $header = fgetcsv($csvFile, 0);
            $this->csvHeaderRow[] = $header;

            // while end of file has not been reached execute the following code
            while(!feof($csvFile))
            {
                // get the rows of data from the file with no maximum line length to ensure no interuption
                $this->csvData = fgetcsv($csvFile, 0);

                // add 1 to row count
                $this->csvRowCount++;
                
                // store csv data for inserting
                $this->storeCsvData();
            }
        }
        // close the csv file
        fclose($csvFile);
    }
    
    private function storeCsvData()
    {
        if($this->checkImport())
        {
            $this->csvImportCount++;
            $this->csvImportData[] = $this->csvData;
        }
        else
        {
            $this->csvSkippedCount++;
            $this->csvSkippedData[] = $this->csvData;
        }
    }
    
    private function checkImport()
    {
        // need to set a specific column number to check
        if($this->csvData[0] != null)
        {
            return true;
        }
        return false;
    }
    
    protected function importSuccess($values)
    {
        $this->importSuccessCount++;
        $this->importSuccessData[] = $values;
    }
    
    protected function importFail($values)
    {
        $this->importFailedCount++;
        $this->importFailedData[] = $values;
    }
}
