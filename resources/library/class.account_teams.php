<?php
class account_teams extends account
{
	const RESTRICTED_ACCESS_ERROR = 'Restricted Access. Changes may only be made to teams that you have submitted.';
	
	private $userTeams;
	private $formErrors;
	private $transferOutTeamId;
	private $transferOutTeamName;
	private $transferOutTeamValue;
	private $transferOutTeamGroup;
	private $teamTotalValue;
	private $remainingBudget;
	private $transferInTeamIds;
	private $transferInTeamNames;
        
        private $monthTransfers;
        private $totalTransfers;
        private $remainingTransfers;
	
	public function __construct($db, $ug, $uf, $linkString)
	{
		parent::__construct($db, $ug, $uf);
		
		$this->userTeams = new stats_user_teams($this->urlGenerator);
		
		$this->checkTeamUser();
		
                // if session is set and link string is not empty and session is not equal to link string then  set session
		if(
                        (!isset($_SESSION['userTeamName']) && $linkString != '') || 
                        ((isset($_SESSION['userTeamName']) && $linkString != '') && $_SESSION['userTeamName'] != $linkString))
		{
			$_SESSION['userTeamName'] = $linkString;
		}
		
		$this->checkForTransferSubmission();
	}
        /*
        public function __destruct()
        {
            unset($_SESSION['userTeamName']);
        }
	*/
	public function showUserTeamPage()
	{
		echo '
		<div class="row">
			<a href="#transfers" class="small-btn blue">Transfers</a>';
			//<a href="matches" class="small-btn orange">Matches</a>
			//<a href="stats" class="small-btn dark-grey">Stats</a>
		echo '
		</div>

		<div class="row">
			<div class="very-light-grey">
				<h2 class="news dark-grey">Team Selections</h2>
				<div class="inner-container">'.
					$this->userTeams->showTeamSelections(TRUE)
				.'</div>
			</div>
			
			<br />
			<a name="transfers"></a>
			<div class="very-light-grey">
				<h2 class="news dark-grey">Transfers</h2>
				<div class="inner-container">'.
					$this->showTransferOut()
				.'</div>
			</div>';
			
			if(!empty($this->userTeams->transferredTeamInformation))
			{
				echo '
				<br />
				<div class="very-light-grey">
					<h2 class="news dark-grey">Transfer History</h2>
					<div class="inner-container">'.
						$this->userTeams->showTransferHistory()
					.'</div>
				</div>';
			}
			
			echo '
			<br />
			<div class="very-light-grey">
				<h2 class="news dark-grey">Leaderboard Snapshot</h2>
				<div class="inner-container">'.
					$this->userTeams->showLeaderboardSnapshot()
				.'</div>
			</div>
		</div>';
	}
	
	public function showUserTeamTransfersPage()
	{
		$this->setTransferOutTeamInfo();
		$this->setTeamTotalValue();
		
		$this->remainingBudget = config::BUDGET - ($this->teamTotalValue - $this->transferOutTeamValue);
		
		
		if(isset($_POST['transfer_in']))
		{
			$this->checkTransferIn($_POST['team_in'], $_POST['rem_budget']);
			if(empty($this->formErrors))
			{
				if($this->insertTransfer($_POST['team_in']))
				{
					unset($_SESSION['team_out']);
					echo notifications::showNotification('success', TRUE, 'Thank you. Your transfer has been successful');
					
					echo '<div class="row">
						<a href="'.$this->urlGenerator->makeUrl($_SESSION['userTeamName'], $this->userTeams->userTeamId, 'user_team').'" class="small-btn dark-grey">Go Back</a>
					</div>';
				}
			}
			else
			{
				echo notifications::showNotification('error', TRUE, $this->formErrors);
				echo $this->showTransferInForm();  
			}
		}
		else
		{
			echo $this->showTransferInForm();   
		}
	}
	
	private function checkForTransferSubmission()
	{
            if(isset($_POST['transfer_out']))
            {
                $this->checkTransferOut($_POST['team_out'], $_POST['rem_transfers']);
                if(empty($this->formErrors))
                {
                    $_SESSION['teamTransferOut'] = $_POST['team_out'];
                    header("Location: ".$this->urlGenerator->makeUrl($_SESSION['userTeamName'], $this->userTeams->userTeamId, 'user_team')."transfers");
                    die();
                }
            }
	}
	
	private function checkTransferOut($teamId, $remainingTransfers)
	{
		if($remainingTransfers <= 0)
		{
			$this->formErrors[] = 'Sorry, you have already reached your limit of '.config::$maxTransfers.' transfers';
		}
		elseif($teamId == '')
		{
			$this->formErrors[] = 'Please select a team to transfer out';
		}
	}
	
	private function checkTransferIn($teamId, $remainingBudget)
	{
		// get value of team being transferred in
		$stmt = config::$mysqli->prepare("
		SELECT value
		FROM tff_teams
		WHERE team_id = ?
		LIMIT 1");
		$stmt->bind_param("i", $teamId);
		$stmt->execute();
		$stmt->bind_result($value);
		$stmt->fetch();
		$stmt->close();
		
		// check for team_id to ensure submit button hasn't been pressed twice by mistake
		$stmt = config::$mysqli->prepare("
		SELECT team_id 
		FROM user_team_selections
		WHERE user_team_id = ?
		AND team_id = ?
		AND transfer_out > NOW()
		LIMIT 1");
		$stmt->bind_param("ii", $this->userTeams->userTeamId, $teamId);
		$stmt->execute();
		$stmt->store_result();
		
		if($teamId == '')
		{
			$this->formErrors[] = 'Please select a team to transfer in';
		}
		if($value > $remainingBudget)
		{
			$this->formErrors[] = 'Team selected exceeds available budget of £'.$remainingBudget.'m';
		}
		if ($stmt->num_rows != 0)
		{
			$this->formErrors[] = 'Team has already been transfered in';
		}
		$stmt->close();
	}
	
	private function checkTeamUser()
	{
		$stmt = config::$mysqli->prepare("
		SELECT user_id
		FROM user_teams 
		WHERE user_team_id = ?
		LIMIT 1");
		$stmt->bind_param("i", $this->userTeams->userTeamId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($teamUserId);
		$stmt->fetch();
		$stmt->close();
		
		if($teamUserId != $this->userId)
		{
			echo notifications::showNotification('error', TRUE, self::RESTRICTED_ACCESS_ERROR);
		}
	}
	
	private function showTransferOut()
	{
            $this->setTransferTotals();
            $string = $this->showTransferOutInfo();

            unset($this->urlGenerator->linkString);
            unset($this->urlGenerator->urlId);
            unset($this->urlGenerator->urlType);

            if(!empty($this->formErrors))
            {
                $string .= '<br />';
                $string .= notifications::showNotification('error', TRUE, $this->formErrors);
            }
			
            $string .= $this->showTransferOutForm();

            return $string;
	}
        
        private function setTransferTotals()
        {
            $stmt = config::$mysqli->prepare("
            SELECT
                COALESCE(SUM(CASE WHEN Month(transfer_out) = Month(?) AND Year(transfer_out) = Year(?) AND transfer_out > ? THEN 1 ELSE 0 END),0) AS month, 
                COALESCE(SUM(CASE WHEN Year(transfer_out) < 2100 THEN 1 ELSE 0 END),0) AS total
            FROM user_team_selections
            WHERE user_team_id = ?");
            $stmt->bind_param("sssi", config::$curDate, config::$curDate, config::$curSeasonStart, $this->userTeams->userTeamId);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($this->monthTransfers, $this->totalTransfers);
            $stmt->fetch();
            $stmt->close();
            
            $this->remainingTransfers = config::$maxTransfers - $this->totalTransfers;
            return;
        }
        
        private function showTransferOutInfo()
        {
            return '
            <table>
                <thead>
                    <tr>
                        <th><span>Transfer Information</span></th>
                        <th class="align-right"><span>Number of Transfers</span></th>
                    </tr>
                <tbody>
                    <tr class="table-row0">
                        <td>Season Total</td>
                        <td class="align-right">'.$this->totalTransfers.'</td>
                    </tr>
                    <tr class="table-row1">
                        <td>Month Total</td>
                        <td class="align-right">'.$this->monthTransfers.'</td>
                    </tr>
                    <tr class="table-row0">
                        <td><strong>Total Remaining</strong></td>
                        <td class="align-right"><strong>'.$this->remainingTransfers.'</strong></td>
                    </tr>
            </table>';
        }
        
        private function showTransferOutForm()
        {
            return '
            <form id="transfer" method="post" action="'.htmlspecialchars($this->urlGenerator->makeUrl($_SESSION['userTeamName'], $this->userTeams->userTeamId, 'user_team')).'" class="form">
                <input type="hidden" name="rem_transfers" value="'.$this->remainingTransfers.'" />
                <div class="very-light-grey">
                    <h3>Make a Transfer</h3>
                    <label for="team_out">Select a Team to Transfer Out</label>
                    <select name="team_out">
                        <option></option>'.
                        $this->setTeamOptions().'
                    </select>
                    <input type="submit" value="Make Transfer" name="transfer_out" id="Submit Form" class="button float-right" />
                </div>
            </form>';
        }
        
        private function setTeamOptions()
        {
            $string = '';
            foreach($this->userTeams->teamInformation as $teamInfo)
            {
                $string .= '<option value="'.$teamInfo['team_id'].'">'.$teamInfo['team_name'].'</option>';
            }
            return $string;
        }
	
	private function setTransferOutTeamInfo()
	{
		$this->transferOutTeamId = $_SESSION['teamTransferOut'];
		$stmt = config::$mysqli->prepare("
		SELECT
			tn.team_name,
			tt.value,
			tt.selection_group
		FROM team_names tn
		INNER JOIN tff_teams tt
		ON tt.team_id = tn.team_id
		WHERE tn.team_id = ?
		LIMIT 1");
		$stmt->bind_param("i", $this->transferOutTeamId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($this->transferOutTeamName, $this->transferOutTeamValue, $this->transferOutTeamGroup);
		$stmt->fetch();
		$stmt->close();
	}
	
	private function setTeamTotalValue()
	{
		$stmt = config::$mysqli->prepare("
		SELECT
            COALESCE(SUM(CASE WHEN uts.user_team_id = ? AND uts.transfer_in < NOW() AND uts.transfer_out > NOW() THEN tt.value ELSE 0 END),0) AS total_value
		FROM user_team_selections uts
		INNER JOIN tff_teams tt
		ON uts.team_id = tt.team_id
        LIMIT 1");
		$stmt->bind_param("i", $this->userTeams->userTeamId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($this->teamTotalValue);
		$stmt->fetch();
		$stmt->close();
	}
	
	private function showTransferInForm()
	{	
		$stmt = config::$mysqli->prepare("
		SELECT
			tn.team_id,
			tn.team_name,
			tt.value
		FROM team_names tn
		INNER JOIN tff_teams tt
		ON tn.team_id = tt.team_id
        WHERE tt.selection_group = ?
        AND tn.team_id <> ?
		ORDER BY tt.value DESC");
		$stmt->bind_param("ii", $this->transferOutTeamGroup, $this->transferOutTeamId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($teamIds, $teamNames, $teamValues);
		
		$string = '
		<div class="row">
			<a href="'.$this->urlGenerator->makeUrl($_SESSION['userTeamName'], $this->userTeams->userTeamId, 'user_team').'" class="small-btn dark-grey">Go Back</a>
			<a class="small-btn blue">Team Out: <strong>'.$this->transferOutTeamName.'</strong></a>
			<a class="small-btn orange">Budget Remaining: <strong>£'.$this->remainingBudget.'m</strong></a>
		</div>
		
		<div class="row very-light-grey">
			<h2 class="news dark-grey">Select Team to Transfer In</h2>
			<div class="inner-container very-light-grey">
				<form id="transfer" method="post" action="'.htmlspecialchars($this->urlGenerator->makeUrl($_SESSION['userTeamName'], $this->userTeams->userTeamId, 'user_team')).'transfers" class="form">
				<input type="hidden" name="rem_budget" value="'.$this->remainingBudget.'" />
					<div>
						<h3>Make a Transfer</h3>
						<label for="team_in">Select a Team to Transfer In</label>
						<select name="team_in">
							<option></option>';
							while($stmt->fetch())
							{
								$string .= '<option value="'.$teamIds.'">'.$teamNames.' (£'.$teamValues.'m)</option>';
							}
						$string .= '
						</select>
						<input type="submit" value="Make Transfer" name="transfer_in" id="Submit Form" class="button float-right" />
					</div>
				</form>
			</div>
		</div>';
		
		$stmt->close();
		
		return $string;
	}
	
	private function insertTransfer($teamIn)
	{
		$this->transferOutTeamId = $_SESSION['teamTransferOut'];
		
		$stmt = config::$mysqli->prepare("INSERT INTO user_team_selections (user_team_id, team_id, transfer_in, transfer_out)
		VALUES (?, ?, NOW(), '2100-01-01 00:00:01')");
		$stmt->bind_param("ii", $this->userTeams->userTeamId, $teamIn);
		$stmt->execute();
		$stmt->store_result();
		$stmt->close();
		
		$stmt = config::$mysqli->prepare("UPDATE user_team_selections SET transfer_out = NOW()
		WHERE user_team_id = ?
		AND team_id = ?
		AND transfer_out = '2100-1-1 00:00:01'");
		$stmt->bind_param("ii", $this->userTeams->userTeamId, $this->transferOutTeamId);
		$stmt->execute();
		$stmt->store_result();
		$stmt->close();
		
		return true;
	}
	
	private function showTransferHistory()
	{
		/*
		<h2>Transfer History</h2>
		<h3 class="team-name"><?php echo $user_team_name; ?></h3>
		<?php
		$stmt = $mysqli->prepare("SELECT teams.team_id, teams.team_name, user_team_selections.transfer_in, user_team_selections.transfer_out, competitions.short_name, tff_teams.selection_group, COALESCE(SUM(CASE WHEN user_team_selections.transfer_in < fixtures.date AND user_team_selections.transfer_out > fixtures.date THEN team_points.points ELSE 0 END),0) AS total
		FROM user_teams
		LEFT JOIN user_team_selections
		ON user_teams.user_team_id = user_team_selections.user_team_id
		LEFT JOIN teams
		ON user_team_selections.team_id = teams.team_id
		LEFT JOIN tff_teams
		ON teams.team_id = tff_teams.team_id
		LEFT JOIN competitions
		ON tff_teams.league_id = competitions.comp_id
		LEFT JOIN team_points
		ON teams.team_id = team_points.team_id
		LEFT JOIN fixtures
		ON team_points.fixture_id = fixtures.fixture_id
		WHERE user_teams.user_team_id = ? 
		AND transfer_out < CONVERT_TZ(NOW(), '-08:00', '+00:00')
		GROUP BY teams.team_id
		ORDER BY tff_teams.selection_group ASC");
		$stmt->bind_param("i", $user_team_id);
		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows == 0) {
			echo '<p class="error">You haven\'t made any transfers for '.$user_team_name.' yet. To make a transfer <a href="transfers.php?user_team_id='.$user_team_id.'" title="Make Transfers">click here</a>.</p>';
		}
		else {
			$stmt->bind_result($team_id, $team_name, $transfer_in, $transfer_out, $short_name, $selection_group, $total);
			
			echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr class="table-header">
				<td>Group</td>
				<td>Division</td>
				<td>Team</td>
				<td>Transfer In Date</td>
				<td>Transfer Out Date</td>
				<td class="right">Points Scored</td>
			  </tr>';
			
			$row_class = 0;
			while ($stmt->fetch()) {
				echo '<tr class="table-row'.$row_class.'">
					<td>'.$selection_group.'</td>
					<td>'.$short_name.'</td>
					<td>'.show_team_link($team_name, $team_id).'</td>
					<td>'.date("j M y", strtotime($transfer_in)).'</td>
					<td>'.date("j M y", strtotime($transfer_out)).'</td>
					<td class="right">'.$total.'</td>
				</tr>';
			$row_class = 1 - $row_class;
			}
			echo '</table>';
		}
		*/
	}
}
?>
