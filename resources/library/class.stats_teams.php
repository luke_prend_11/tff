<?php
class stats_teams extends stats
{	
    private $userFunctions;
    private $pagination;
    
    private $teamWebsite;
    private $teamNickname;
    private $teamStadium;
    private $teamDivision;
    private $teamDivisionId;
    private $teamSeasonPoints;
    private $teamMonthPoints;
    private $teamPercentSelected;
    private $teamTransferredIn;
    private $teamTransferredOut;
    private $teamAveragePointsPerGame;
	
    public function __construct($ug, $uf)
    {
        parent::__construct($ug);
        
        $this->userFunctions = $uf;
        $this->pagination = new pagination();
        $this->setTeamInformation();
    }

    private function setTeamInformation()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            ti.website,
            ti.nickname,
            ti.ground,
            c.comp_name,
            c.comp_id
        FROM team_info ti
        INNER JOIN tff_teams t
        ON t.team_id = ti.team_id
        INNER JOIN competitions c
        ON t.league_id = c.comp_id
        WHERE ti.team_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $this->urlId);
        $stmt->bind_result($this->teamWebsite, $this->teamNickname, $this->teamStadium, $this->teamDivision, $this->teamDivisionId);
        $stmt->execute();
        $stmt->fetch(); 

        $stmt->close();
    }
	
    public function displayTeamPage()
    {
        echo '
        <div class="row stretch-parent">
            <div class="col-2-x-3 stretch-child">';
            
            if(config::FANTASY_FOOTBALL_ACTIVE === TRUE)
            {
                echo '
                <div class="very-light-grey">
                    <h2 class="news dark-grey">Stats</h2>
                    <div class="inner-container">'.
                        $this->showTeamStats()
                    .'</div>
                </div>';
            }
            
            echo '
                <div class="very-light-grey">
                    <h2 class="news dark-grey">Matches</h2>
                    <div class="inner-container">'.
                        $this->showTeamMatches()
                    .'</div>
                </div>

                <div id="curve_chart" style="width: 100%;"></div>

                <div class="very-light-grey">
                    <h2 class="news dark-grey">League Table</h2>
                    <div class="inner-container">'.
                        $this->showLeagueTable().'
                    </div>
                </div>
            </div>

            <div class="col-1-x-2 stretch-child very-light-grey">
                <div class="inner-container">'.
                    $this->showTeamInfo()
                .'</div>
            </div>
            <span class="stretch"></span>
        </div>';
    }
	
    private function showTeamInfo()
    {
        return '
        <img class="center" src="'.config::$baseUrl.'/img/content/team-badges/'.$this->urlId.'.png" alt="Default User Team Logo" />

        <span class="team-info-heading">Nickname</span>
        <span class="team-info-text">'.$this->teamNickname.'</span>
        <span class="team-info-heading">Stadium</span>
        <span class="team-info-text">'.$this->teamStadium.'</span>
        <span class="team-info-heading">League</span>
        <span class="team-info-text">'.$this->urlGenerator->showLink($this->teamDivision, $this->teamDivisionId, 'league_table').'</span>
        <span class="team-info-heading">Website</span>
        <span class="team-info-text"><a href="http://'.$this->teamWebsite.'" title="Team Website" target="_blank">'.$this->teamWebsite.'</a></span>';
    }
	
    private function showTeamStats()
    {
        $this->getTeamStats();

        return '
        <table>
            <tbody>
                <tr class="table-row0">
                    <td>Season Points
                    <td class="align-right">'
                        .$this->teamSeasonPoints.'
                <tr class="table-row1">
                    <td>Month Points
                    <td class="align-right">'
                        .$this->teamMonthPoints.'
                <tr class="table-row0">
                    <td>Average Points per Game
                    <td class="align-right">'
                        .round($this->teamAveragePointsPerGame).'
                <tr class="table-row1">
                    <td>Team Selections
                    <td class="align-right">'
                        .round($this->teamPercentSelected).'%
                <tr class="table-row0">
                    <td>Transferred In
                    <td class="align-right">'
                        .$this->teamTransferredIn.'
                <tr class="table-row1">
                    <td>Transferred Out
                    <td class="align-right">'
                        .$this->teamTransferredOut.'
        </table>';
    }
	
    private function getTeamStats()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            COUNT(*) / (SELECT COUNT(*) FROM user_teams WHERE date > ?) * 100, 
            COALESCE(SUM(CASE WHEN ut.date <> transfer_in AND transfer_in < CONVERT_TZ(NOW(), '-08:00', '+00:00') AND transfer_in > ? AND transfer_in < ? THEN 1 ELSE 0 END),0) AS trans_in, 
            COALESCE(SUM(CASE WHEN transfer_out < CONVERT_TZ(NOW(), '-08:00', '+00:00') AND transfer_out > ? AND transfer_out < ? THEN 1 ELSE 0 END),0) AS trans_out, 
            COALESCE(x.total, 0), 
            COALESCE(x.month, 0), 
            COALESCE(x.total / x.pld, 0)
        FROM user_teams ut
        LEFT JOIN user_team_selections uts
        ON ut.user_team_id = uts.user_team_id
        LEFT JOIN (
            SELECT 
                tp.team_id, 
                SUM(CASE WHEN f.date >= ? AND f.date <= ? THEN tp.points ELSE 0 END) as total, 
                SUM(CASE WHEN Month(f.date) = ? AND Year(f.date) = ? THEN tp.points ELSE 0 END) AS month, 
                COUNT(DISTINCT f.fixture_id) AS pld
            FROM team_points tp
            LEFT JOIN match_fixtures f
            ON tp.fixture_id = f.fixture_id
            WHERE f.date >= ? 
            AND f.date <= ?
            GROUP BY tp.team_id
        ) AS x
        ON x.team_id = uts.team_id
        WHERE uts.team_id = ?
        AND ut.date > ?");
        $stmt->bind_param(
            "sssssssssssis", 
            config::$curSeasonStart, 
            config::$curSeasonStart, 
            config::$curSeasonEnd, 
            config::$curSeasonStart, 
            config::$curSeasonEnd, 
            config::$curSeasonStart, 
            config::$curSeasonEnd,
            config::$curDate,
            config::$curDate,
            config::$curSeasonStart, 
            config::$curSeasonEnd, 
            $this->urlId, 
            config::$curSeasonStart
        );
        $stmt->execute();
        $stmt->bind_result(
            $this->teamPercentSelected, 
            $this->teamTransferredIn, 
            $this->teamTransferredOut, 
            $this->teamSeasonPoints, 
            $this->teamMonthPoints, 
            $this->teamAveragePointsPerGame
        );
        $stmt->fetch();
        $stmt->close();
    }

    private function showTeamMatches()
    {
        // connect to matches class to bring through...
        $m = new matches($this->urlGenerator, $this->userFunctions);
        return $m->displayMatches('all', NULL, 10, $this->urlId, FALSE);
        
        /*
        $m = new matches($this->urlGenerator, $this->userFunctions);
        $m->setMatches('all', NULL, 200, $this->urlId);
        
        
        
        
        echo '
        <script type="text/javascript">
        $(document).ready(function() {

            $.post( "getpicture.php", { pic: "1"}, function( data ) {
              $("#picture").html( data );
            });

            $("#picture").on("click",".get_pic", function(e){
                var picture_id = $(this).attr(\'data-id\');
                $("#picture").html("<div style="margin:50px auto;width:50px;"><img src="loader.gif" /></div>");
                $.post( "getpicture.php", { pic: picture_id}, function( data ) {
                    $("#picture").html( data );
                });
                return false;
            });

        });
        </script>
        ';
        
        //get id or limit from ajax request
        if(isset($_POST["pic"]) && is_numeric($_POST["pic"]))
        {
            $current_picture = filter_var($_POST["pic"], FILTER_SANITIZE_NUMBER_INT);
        }else{
            $current_picture=1;
        }

        //get next results basic info
        $result = $mysqli->query("SELECT id FROM pictures WHERE id > $current_picture ORDER BY id ASC LIMIT 1")->fetch_object();
        if($result){
            $next_id = $result->id;
        }

        //get previous results basic info
        $result = $mysqli->query("SELECT id FROM pictures WHERE id < $current_picture ORDER BY id DESC LIMIT 1")->fetch_object();
        if($result){
            $prev_id = $result->id;
        }

        //get details of current results
        $result = $mysqli->query("SELECT PictureTitle, PictureName FROM pictures WHERE id = $current_picture LIMIT 1")->fetch_object();
        
        // if results exist then display results
        if($m->matchesData)
        {
            //construct next/previous button
            $prev_button = (isset($prev_id) && $prev_id > 0) ? '<a href="#" data-id="'.$prev_id.'" class="get_pic">Previous</a>' : '';
            $next_button = (isset($next_id) && $next_id > 0) ? '<a href="#" data-id="'.$next_id.'" class="get_pic">Next</a>' : '';

            //output html
            echo '<table width="500" border="0" cellpadding="5" cellspacing="0">';
            echo '<tr>';
            echo '<td><table width="100%" border="0" cellpadding="5" cellspacing="0">';
            echo '<tr>';
            echo '<td width="10%">'.$prev_button.'</td>';
            echo '<td width="80%" align="center"><h3>'.$result->PictureTitle.'</h3></td>';
            echo '<td width="10%">'.$next_button.'</td>';
            echo '</tr>';
            echo '</table></td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td align="center"><img src="pictures/'.$result->PictureName.'" /></td>';
            echo '</tr>';
            echo '</table>';
        }
        */
    }
    
    private function showLeagueTable()
    {
        // connect to competitions class to bring through...
        $sc = new stats_competitions($this->urlGenerator, $this->userFunctions);
        return $sc->showLeagueTable($sc::LEAGUE_TABLE_SIZE_FULL, $sc::LEAGUE_TABLE_VIEW_SEASON, NULL, $this->teamDivisionId, $this->urlId);
    }
}
