<?php
class account_profile extends account
{
	const NO_DETAILS_ERROR         = 'Your personal details could not be found.';
	const NO_CHANGES_ERROR         = 'Your have not entered any details to change.';
	const EDIT_PROFILE_SUCCESS     = 'You have successfully updated your details.';
	const DATABASE_ENTRY_ERROR     = 'There was an error inserting your details into the database.';
	const NO_PASSWORD_ERROR        = 'You have not supplied a password.';
	const INCORRECT_PASSWORD_ERROR = 'You have supplied an incorrect password.';
	
	private $newFirstName;
	private $newLastName;
	private $newLocation;
	private $newEmailAddress1;
	private $newEmailAddress2;
	private $newPassword1;
	private $newPassword2;
	private $currentPassword;
	private $firstName;
	private $lastName;
	private $gender;
	private $location;
	private $password;
	private $email;
	private $referralId;
	private $registrationDate;
	private $referralLink;
	private $formErrors;
		
	public function __construct($db, $ug, $uf)
	{
		parent::__construct($db, $ug, $uf);
		
		$this->setPersonalDetails();
	}
	
	public function checkProfilePageDisplay()
	{
		if(isset($_POST['profile_edit']))
		{
			$this->setNewUserDetails();
			$this->checkProfileChanges();
			$this->setProfileChanges();
			
			if(empty($this->formErrors))
			{
				$this->editProfileSuccess();
			}
			else
			{
				echo notifications::showNotification('error', TRUE, $this->formErrors);
				$this->showPersonalDetails();
				$this->showEditProfile();
			}
		}
		else
		{
			$this->showPersonalDetails();
			$this->showEditProfile();
		}
	}
	
	private function setNewUserDetails()
	{
		$this->newFirstName     = $_POST['first_name'];
		$this->newLastName      = $_POST['last_name'];
		$this->newLocation      = $_POST['location'];
		$this->newEmailAddress1 = $_POST['email_address'];
		$this->newEmailAddress2 = $_POST['confirm_email'];
		$this->newPassword1     = $_POST['new_password'];
		$this->newPassword2     = $_POST['confirm_password'];
		$this->currentPassword  = $_POST['current_password'];
	}
	
	private function setProfileChanges()
	{
		if(empty($this->newFirstName))
		{
			$this->newFirstName = $this->firstName;
		}
		if(empty($this->newLastName))
		{
			$this->newLastName = $this->lastName;
		}
		if(empty($this->newLocation))
		{
			$this->newLocation = $this->location;
		}
		if(empty($this->newEmailAddress1))
		{
			$this->newEmailAddress1 = $this->email;
		}
		if(empty($this->newPassword1))
		{
			$this->newPassword1 = $this->password;
		}
	}
	
	private function showPersonalDetails()
	{
		echo '
		<h2>Personal Details</h2>
		<ul class="list-none">
			<li>First Name: <strong>'.$this->firstName.'</strong></li>
			<li>Last Name: <strong>'.$this->lastName.'</strong></li>
			<li>Location: <strong>'.$this->location.'</strong></li>
			<li>Email Address: <strong>'.$this->email.'</strong></li>
			<li>Registered Since: <strong>'.$this->registrationDate.'</strong></li>
			<li>Referral Link: <strong>'.$this->referralLink.'</strong></li>
		</ul>
		';
	}
	
	private function showEditProfile()
	{
		echo '
		<h2>Edit Profile</h2>
		<form name="profile_edit" method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
			<h3>Details</h3>
			<div class="very-light-grey">'
				.$this->userFunctions->showStickyForm('text','first_name')
				.$this->userFunctions->showStickyForm('text','last_name')
				.$this->userFunctions->showStickyForm('select','location',account_registration::USER_LOCATIONS)
				.$this->userFunctions->showStickyForm('text','email_address')
				.$this->userFunctions->showStickyForm('text','confirm_email','',true)
				.$this->userFunctions->showStickyForm('text','new_password')
				.$this->userFunctions->showStickyForm('text','confirm_password').
			'</div>
			<h3>Password</h3>
			<div class="very-light-grey">'
				.$this->userFunctions->showStickyForm('text','current_password').
			'</div>
			<input type="submit" value="Edit Profile" name="profile_edit" id="Submit Form" class="button" />
		</form>
		';
	}
	
	private function setPersonalDetails()
	{
		$stmt = config::$mysqli->prepare("
		SELECT first_name,
		       last_name,
			   gender,
			   location,
			   password,
			   email,
			   registration_date
		FROM members
		WHERE user_id = ?
		");
		$stmt->bind_param("i", $this->userId);
		$stmt->execute();
		$stmt->store_result();  
		$stmt->bind_result($firstName, $lastName, $gender, $location, $password, $email, $registrationDate);
		$stmt->fetch();
		
		$this->firstName        = $firstName;
		$this->lastName         = $lastName;
		$this->gender           = $gender;
		$this->location         = $location;
		$this->password         = $password;
		$this->email            = $email;
		$this->referralId       = $this->userId;
		$this->registrationDate = $registrationDate;
		
		$this->generateReferralLink();
		
		$stmt->close();
		return;
	}
	
	private function generateReferralLink()
	{
		$this->referralLink = config::$baseUrl.'/registration.php?referral_id='.$this->referralId;	
	}
	
	private function checkProfileChanges()
	{
		$reg = new account_registration($this->userFunctions, $this->db);
		
		if(empty($this->newFirstName) && empty($this->newLastName) && empty($this->newLocation) && empty($this->newEmailAddress1) && empty($this->newPassword1))
		{
			$this->formErrors[] = self::NO_CHANGES_ERROR;
		}
		elseif(empty($this->currentPassword))
		{
			$this->formErrors[] = self::NO_PASSWORD_ERROR;
		}
		elseif(!$this->confirmPassword())
		{
			$this->formErrors[] = self::INCORRECT_PASSWORD_ERROR;
		}
		else
		{
			if(!empty($this->newEmailAddress1))
			{
				$reg->emailAddress1 = $this->newEmailAddress1;
				$reg->emailAddress2 = $this->newEmailAddress2;
				$reg->checkEmail();
			}
			if(!empty($this->newPassword1))
			{
				$reg->password1 = $this->newPassword1;
				$reg->password2 = $this->newPassword2;
				$reg->checkPassword();
			}
			$this->formErrors = $reg->formErrors;
		}
	}
	
	private function editProfileSuccess()
	{
		$this->updateUserDetails();
		echo notifications::showNotification('success', TRUE, self::EDIT_PROFILE_SUCCESS);
	}
	
	private function updateUserDetails()
	{
		if($this->db->completeQuery(
			'UPDATE members SET first_name = ?, last_name = ?, location = ?, password = ?, email = ? WHERE user_id = ? LIMIT 1', 
			array($this->newFirstName, $this->newLastName, $this->newLocation, $this->newPassword1, $this->newEmailAddress1, $this->userId), 
			array('s', 's', 's', 's', 's', 'i'))) 
		{
			return true;
		}
		else {
			echo notifications::showNotification('error', TRUE, self::DATABASE_ENTRY_ERROR);
		}
	}
	
	private function confirmPassword()
	{
		$reg = new account_registration($this->userFunctions, $this->db);
		
		$seededPassword = $reg->setSeededPassword($this->currentPassword);
		
		$stmt = config::$mysqli->prepare("SELECT user_id 
		FROM members 
		WHERE user_id = ?
		AND password = ?
		LIMIT 1");
		$stmt->bind_param("is", $this->userId, $seededPassword);
		$stmt->execute();
		$stmt->store_result();
		$stmt->fetch();
		
		if ($stmt->num_rows != 1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
?>
