<?php
class stats_user_teams extends stats
{	
    public $teamInformation;

    private $userName;
    private $userTeamRank;
    private $userTeamTransfers;
    private $userTeamPrivateLeagues;
    private $transferredTeamInformation;
    private $userTeamTotalPoints;
    private $userTeamMonthPoints;
    private $transferredTeamsTotalPoints = 0;
    
    public $userTeamId;

    public function __construct($ug)
    {
        parent::__construct($ug);

        $this->setUserTeams();
        $this->setUserTeamInformation();
        $this->setTransferredTeamInformation();
        $this->setOverallTotal();
        
        $this->userTeamId = $this->urlId;
    }

    private function setUserTeamInformation()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            CONCAT(u.first_name, ' ', u.last_name) AS name,
            l.rank,
            SUM(CASE WHEN uts.transfer_out < NOW() THEN 1 ELSE 0 END) as transfers,
            COUNT(ul.league_id) as private_leagues
        FROM user_teams ut
        INNER JOIN members u
        ON u.user_id = ut.user_id
        LEFT JOIN user_team_selections uts
        ON uts.user_team_id = ut.user_team_id
        LEFT JOIN user_leagues ul
        ON ul.user_team_id = ut.user_team_id
        LEFT JOIN ldb_season l
        ON l.user_team_id = ut.user_team_id
        WHERE ut.user_team_id = ?
        LIMIT 1");
        $stmt->bind_param("i", $this->urlId);
        $stmt->bind_result($this->userName, $this->userTeamRank, $this->userTeamTransfers, $this->userTeamPrivateLeagues);
        $stmt->execute();
        $stmt->fetch(); 

        $stmt->close();
    }
	
    private function setUserTeams()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            t.team_id, 
            t.team_name, 
            tff.selection_group,  
            tff.value, 
            c.short_name, 
            COALESCE(SUM(CASE WHEN uts.transfer_in < f.date AND uts.transfer_out > f.date THEN tp.points END),0) AS total_points,
            COALESCE(SUM(CASE WHEN Month(f.date) = ? AND Year(f.date) = ? AND uts.transfer_in < f.date AND uts.transfer_out > f.date THEN tp.points ELSE 0 END),0) as month
        FROM user_team_selections uts
        LEFT JOIN team_names t
        ON t.team_id = uts.team_id
        LEFT JOIN tff_teams tff
        ON tff.team_id = uts.team_id
        LEFT JOIN competitions c
        ON c.comp_id = tff.league_id
        LEFT JOIN team_points tp
        ON uts.team_id = tp.team_id 
        LEFT JOIN match_fixtures f
        ON tp.fixture_id = f.fixture_id
        WHERE uts.user_team_id = ?
        AND transfer_out > NOW()
        GROUP BY t.team_id
        ORDER BY tff.selection_group ASC
        LIMIT 11");
        $stmt->bind_param("ssi", config::$curDate, config::$curDate, $this->urlId);
        $stmt->bind_result($teamId, $teamName, $selectionGroup, $teamValue, $shortName, $totalPoints, $monthPoints);
        $stmt->execute();
        while($stmt->fetch())
        {
            $this->teamInformation[] = array(
                'team_id' 	  => $teamId,
                'team_name' 	  => $teamName,
                'selection_group' => $selectionGroup,
                'value'           => $teamValue,
                'league'          => $shortName,
                'total_points' 	  => $totalPoints,
                'month_points' 	  => $monthPoints,
            );  

            $this->userTeamTotalPoints += $totalPoints; 
            $this->userTeamMonthPoints += $monthPoints; 
        }
        $stmt->close();
    }
	
    private function setTransferredTeamInformation()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            t.team_id, 
            t.team_name, 
            tff.selection_group, 
            tff.value, 
            uts.transfer_in,
            uts.transfer_out,
            COALESCE(SUM(CASE WHEN uts.transfer_in < f.date AND uts.transfer_out > f.date THEN tp.points END),0) AS total_points
        FROM user_team_selections uts
        LEFT JOIN team_names t
        ON t.team_id = uts.team_id
        LEFT JOIN tff_teams tff
        ON tff.team_id = uts.team_id
        LEFT JOIN team_points tp
        ON uts.team_id = tp.team_id 
        LEFT JOIN match_fixtures f
        ON tp.fixture_id = f.fixture_id
        WHERE uts.user_team_id = ?
        AND transfer_out < NOW()
        GROUP BY t.team_id
        ORDER BY tff.selection_group ASC");
        $stmt->bind_param("i", $this->urlId);
        $stmt->bind_result($teamId, $teamName, $selectionGroup, $teamValue, $transferIn, $transferOut, $totalPoints);
        $stmt->execute();
        if($stmt->num_rows <> 0)
        {
            while($stmt->fetch())
            {
                $this->transferredTeamInformation[] = array(
                    'team_id'         => $teamId,
                    'team_name'       => $teamName,
                    'selection_group' => $selectionGroup,
                    'value'           => $teamValue,
                    'transfer_in'     => $transferIn,
                    'transfer_out'    => $transferOut,
                    'total_points'    => $totalPoints,
                );     

                $this->transferredTeamsTotalPoints += $totalPoints; 
            }
        }
        $stmt->close();
    }
	
    private function setOverallTotal()
    {
        $this->userTeamTotalPoints += $this->transferredTeamsTotalPoints;
    }
	
    public function displayUserTeamPage()
    {
        echo '
        <div class="row stretch-parent">
            <div class="col-2-x-3 stretch-child">
                <div class="very-light-grey">
                    <h2 class="news dark-grey">Team Selections</h2>
                    <div class="inner-container">'.
                        $this->showTeamSelections()
                    .'</div>
                </div>';

                if(!empty($this->transferredTeamInformation))
                {
                    echo '
                    <div class="very-light-grey">
                        <h2 class="news dark-grey">Transfer History</h2>
                        <div class="inner-container">'.
                            $this->showTransferHistory()
                        .'</div>
                    </div>';
                }

                echo '
                <div class="very-light-grey">
                    <h2 class="news dark-grey">Leaderboard Snapshot</h2>
                    <div class="inner-container">'.
                        $this->showLeaderboardSnapshot()
                    .'</div>
                </div>
            </div>

            <div class="col-1-x-3 stretch-child very-light-grey">
                <div class="inner-container">'.
                    $this->showUserTeamInfo()
                .'</div>
            </div>
            <span class="stretch"></span>
        </div>';		
    }
	
    private function showUserTeamInfo()
    {
        return '
        <img class="center" src="'.config::$baseUrl.'/img/content/default-badge.png" alt="Default User Team Logo" />

        <span class="team-info-heading">Manager</span>
        <span class="team-info-text">'.$this->userName.'</span>
        <span class="team-info-heading">Total Points</span>
        <span class="team-info-text">'.$this->userTeamTotalPoints.'</span>
        <span class="team-info-heading">Month Points</span>
        <span class="team-info-text">'.$this->userTeamMonthPoints.'</span>
        <span class="team-info-heading">Leaderboard Position</span>
        <span class="team-info-text">'.$this->userTeamRank.'</span>
        <span class="team-info-heading">Private Leagues</span>
        <span class="team-info-text">'.$this->userTeamPrivateLeagues.'</span>
        <span class="team-info-heading">Transfers</span>
        <span class="team-info-text">'.$this->userTeamTransfers.'</span>';
    }
	
    public function showTeamSelections($transfers = FALSE)
    {
        $string = '
        <table>
            <thead>
                <tr>
                    <th><span>Grp</span>
                    <th class="hidden"><span>Div</span>
                    <th><span>Team</span>';
                    $string .= $transfers == TRUE ? '<th class="hidden align-right"><span>Value</span>' : '';
                    $string .= '<th class="align-right"><span>Month Points</span>
                                <th class="align-right"><span>Season Points</span>
            <tbody>';

            foreach($this->teamInformation as $teamInfo)
            {
                $string .= '
                <tr class="table-row'.config::$rowClass.'">
                    <td>'
                        .$teamInfo['selection_group'].'
                    <td class="hidden">'
                        .$teamInfo['league'].'
                    <td class="club" style="background-image:url('.config::$baseUrl.'/img/content/team-badges/'.$teamInfo['team_id'].'.png); background-size:18px 18px;">'
                        .$this->urlGenerator->showLink($teamInfo['team_name'], $teamInfo['team_id'], 'team');

                    $string .= $transfers == TRUE ? '<td class="hidden align-right">£'.$teamInfo['value'].'m' : '';
                    $string .= 
                    '<td class="align-right">'
                            .$teamInfo['month_points'].'
                    <td class="align-right">'
                            .$teamInfo['total_points'];

                config::updateRowClass();
            }

            $string .= '			
                <tr class="table-row'.config::$rowClass.'">
                    <td>&nbsp;
                    <td class="hidden">&nbsp;
                    <td class="total">Transferred Out Total';
                    $string .= $transfers == TRUE ? '<td class="hidden">&nbsp;' : '';
                    $string .= '
                    <td class="align-right">&nbsp;
                    <td class="align-right">'
                        .$this->transferredTeamsTotalPoints;

            config::updateRowClass();

            $string .= '
                <tr class="table-row'.config::$rowClass.'">
                    <td>&nbsp;
                    <td class="hidden">&nbsp;
                    <td class="total">Total';
                    $string .= $transfers == TRUE ? '<td class="hidden">&nbsp;' : '';
                    $string .= '
                    <td class="align-right">'
                        .$this->userTeamMonthPoints.'
                    <td class="align-right">'
                        .$this->userTeamTotalPoints;

        $string .= '
        </table>';
        return $string;
    }
	
    public function showTransferHistory()
    {
        $string = '
        <table>
            <thead>
                <tr>
                    <th><span>Grp</span>
                    <th><span>Team</span>
                    <th><span>Transfer In</span>
                    <th><span>Transfer Out</span>
                    <th class="align-right"><span>Total Points</span>
            <tbody>';

            foreach($this->transferredTeamInformation as $teamInfo)
            {
                $string .= '
                <tr class="table-row'.config::$rowClass.'">
                    <td>'
                        .$teamInfo['selection_group'].'
                    <td class="club" style="background-image:url('.config::$baseUrl.'/img/content/team-badges/'.$teamInfo['team_id'].'.png); background-size:18px 18px;">'
                        .$this->urlGenerator->showLink($teamInfo['team_name'], $teamInfo['team_id'], 'team').'
                    <td>'
                        .$teamInfo['transfer_in'].'
                    <td>'
                        .$teamInfo['transfer_out'].'
                    <td class="align-right">'
                        .$teamInfo['total_points'];

                config::rowClassUpdate();
            }
        $string .= '
        </table>';
        return $string;
    }
	
    public function showLeaderboardSnapshot()
    {
        /* 
        // determine start and end of leaderboard snapshot based on team rank
        if($this->userTeamRank <= 4)
        {
                $leaderboardStart = 1;
                $leaderboardEnd   = $leaderboardStart + 6;
        }
        elseif($this->userTeamRank >= ($totalLeaderboardTeams - 3))
        {
                $leaderboardEnd   = $totalLeaderboardTeams;
                $leaderboardStart = $leaderboardEnd - 6;
        }
        else
        {
                $leaderboardStart = $this->userTeamRank - 3;
                $leaderboardEnd   = $this->userTeamRank + 3;
        }*/

        $leaderboardStart = $this->userTeamRank - 3 < 0 ? 0 : $this->userTeamRank - 3;
        $leaderboardEnd   = $this->userTeamRank + 3;
        $leaderboardLimit = 'LIMIT '.$leaderboardStart.', '.$leaderboardEnd;

        $l = new leaderboards($this->urlGenerator);
        return $l->showLeaderboard('full', 'season', $this->urlId, $leaderboardLimit);
    }
}
