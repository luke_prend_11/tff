<?php
class admin_teams
{
    private $userFunctions;
    private $database;
    
    private $editTeamId;
    private $editTeamWebsite;
    private $editTeamNickname;
    private $editTeamGround;
    private $editTeamRss;
    private $editTeamName;
    
    private $teamIdsList;
    private $teamNamesList;
    
    public function __construct($uf, $db)
    {
        $this->userFunctions = $uf;
        $this->database      = $db;
        $this->checkForm();
        $this->setTeamsList();
    }
    
    private function checkForm()
    {
        if(isset($_POST['edit_team']) && !empty($_POST['edit_team']))
        {
            $this->editTeamId = $_POST['team'];
        }
    }
    
    private function setTeamsList()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            team_id,
            team_name
        FROM team_names
        ORDER BY team_name ASC");
        $stmt->bind_result($teamId, $teamName);
        $stmt->execute();
        while($stmt->fetch())
        {
            $this->teamIdsList[]   = $teamId;
            $this->teamNamesList[] = $teamName;
        }
    }
    
    public function showAdminTeamsPage()
    {
        if(isset($_POST['submit_team_details']))
        {
            $this->setNewTeamDetails();
            $this->insertTeamDetails();
        }
        if(isset($_POST['edit_team']))
        {
            $this->getTeamDetails();
            $this->teamDetailsForm();
        }
        else
        {
            $this->modifyTeamForm().'
            <h2>Insert New Team</h2>';
        }
    }
    
    private function modifyTeamForm()
    {
        echo '
        <h2>Modify Existing Team</h2>        
        <form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
            <div>'.
                $this->userFunctions->showStickyForm('select','team',$this->teamNamesList,FALSE,'Select a Team',$this->teamIdsList).'
            </div>
            <input name="edit_team" type="submit" value="Submit" class="button" />
        </form>';
    }
    
    private function getTeamDetails()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            ti.website,
            ti.nickname,
            ti.ground,
            ti.rss,
            tn.team_name
        FROM team_names tn
        LEFT JOIN team_info ti
        ON tn.team_id = ti.team_id
        WHERE tn.team_id = ?");
        $stmt->bind_param("i", $this->editTeamId);
        $stmt->bind_result($_POST['website'], $_POST['nickname'], $_POST['ground'], $_POST['rss'], $_POST['team_name']);
        $stmt->execute();
        $stmt->fetch();
    }
    
    private function teamDetailsForm()
    {
        echo '
        <h2>'.$_POST['team_name'].'</h2>        
        <form action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" method="post" class="form">
            <input name="team_id" type="hidden" value="'.$this->editTeamId.'" />
            <div>'.
		$this->userFunctions->showStickyForm('text','team_name').
		$this->userFunctions->showStickyForm('text','website').
		$this->userFunctions->showStickyForm('text','nickname').
		$this->userFunctions->showStickyForm('text','ground').
		$this->userFunctions->showStickyForm('text','rss').'
            </div>
            <input name="submit_team_details" type="submit" value="Edit Team" class="button" />
        </form>';
    }
    
    private function setNewTeamDetails()
    {
        $this->editTeamWebsite  = $_POST['website'];
        $this->editTeamNickname = $_POST['nickname'];
        $this->editTeamGround   = $_POST['ground'];
        $this->editTeamRss      = $_POST['rss'];
        $this->editTeamId       = $_POST['team_id'];
        $this->editTeamName     = $_POST['team_name'];
    }
    
    private function insertTeamDetails()
    {
        // check if team is already in team names table and if so update else insert 
        if($this->checkForTeamName())
        {
            if($this->database->completeQuery('UPDATE team_names SET team_name = ? WHERE team_id = ? LIMIT 1', 
                                               array($this->editTeamName, $this->editTeamId), 
                                               array('s','i')))
            {
                echo notifications::showNotification('success', FALSE, 'Team Name inserted successfully');
            }
            else
            {
                echo notifications::showNotification('error', FALSE, 'Error inserting Team Name');
            }
        }
        else
        {
            if($this->database->completeInsert('team_names', 
                                               array('team_name'), 
                                               array($this->editTeamName), 
                                               array('i','s')))
            {
                echo notifications::showNotification('success', FALSE, 'Team Name inserted successfully');
            }
            else
            {
                echo notifications::showNotification('error', FALSE, 'Error inserting Team Name');
            }
            
            // if new team id will need applying to $this->editTeamId ready for inserting to team_info table
            $this->editTeamId = $this->database->insertId;
        }
        
        // check if team is already in team info table and if so update else insert 
        if($this->checkForTeamInfo())
        {
            if($this->database->completeQuery('UPDATE team_info SET website = ?, nickname = ?, ground = ?, rss = ? WHERE team_id = ? LIMIT 1', 
                                           array($this->editTeamWebsite, $this->editTeamNickname, $this->editTeamGround, $this->editTeamRss, $this->editTeamId), 
                                           array('s','s','s','s','i')))
            {
                echo notifications::showNotification('success', FALSE, 'Team Info inserted successfully');
            }
            else
            {
                echo notifications::showNotification('error', FALSE, 'Error inserting Team Info');
            }
        }
        else
        {
            if($this->database->completeInsert('team_info', 
                                               array('team_id', 'website', 'nickname', 'ground', 'rss'), 
                                               array($this->editTeamId, $this->editTeamWebsite, $this->editTeamNickname, $this->editTeamGround, $this->editTeamRss), 
                                               array('i','s','s','s','s')))
            {
                echo notifications::showNotification('success', FALSE, 'Team Info inserted successfully');
            }
            else
            {
                echo notifications::showNotification('error', FALSE, 'Error inserting Team Info');
            }
        }
    }
    
    private function checkForTeamName()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            team_id
        FROM team_names
        WHERE team_id = ?");
        $stmt->bind_param('i', $this->editTeamId);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 0)
        {
            exit($this->editTeamId);
            return false;
        }
        return true;
    }
    
    private function checkForTeamInfo()
    {
        $stmt = config::$mysqli->prepare("
        SELECT 
            team_id
        FROM team_info
        WHERE team_id = ?");
        $stmt->bind_param('i', $this->editTeamId);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 0)
        {
            return false;
        }
        return true;
    }
}
