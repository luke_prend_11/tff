<?php
class url_redirector
{
    const TEAM_QUERY           = "SELECT team_name FROM team_names WHERE team_id = ?";
    const USER_TEAM_QUERY      = "SELECT user_team_name FROM user_teams WHERE user_team_id = ?";
    const LEAGUE_QUERY         = "SELECT comp_name FROM competitions WHERE comp_id = ?";
    const NEWS_QUERY           = "SELECT title FROM news_posts WHERE news_id = ?";
    const PRIVATE_LEAGUE_QUERY = "SELECT league_name FROM private_leagues WHERE league_id = ?";
    const MATCH_QUERY          = "SELECT CONCAT(h.team_name, '-', a.team_name) FROM match_fixtures INNER JOIN team_names h ON ht_id = h.team_id INNER JOIN team_names a ON at_id = a.team_id WHERE fixture_id = ?";

    private $urlGenerator;
    private $properUrl;
    private $urlParameter;
    private $urlParameterValue;
    private $properUrlQuery;
    // need to be public to use in class account_teams for transfer form action url
    public $linkString;
    private $urlType;
	
    public function __construct($ug)
    {
        $this->urlGenerator = $ug;
        $this->getUrlParamater();
    }

    private function getUrlParamater()
    {
        $allParamatersFromUrl = array_keys($_GET);

        $this->urlParameter = in_array('fixture_id', $allParamatersFromUrl) ? 'fixture_id' : array_shift($allParamatersFromUrl);
    }

    private function checkUrl()
    {
        $urlToMatch = config::$testingMode === TRUE || strpos(config::BASE_URL_LIVE_TESTING, $_SERVER['SERVER_NAME']) 
                ? "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] 
                : config::$baseUrl.$_SERVER['REQUEST_URI'];

        if ($urlToMatch != $this->properUrl)
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: '.$this->properUrl);
            exit();
        }
    }

    private function setUrlParameterValue()
    {
        if (isset($_GET[$this->urlParameter]) && !empty($_GET[$this->urlParameter]))
        {
            $this->urlParameterValue = htmlspecialchars($_GET[$this->urlParameter]);
            return;
        }
        exit('Unauthorized Access');
    }

    public function fixUrl()
    {
        $this->getProperUrl();
        $this->checkUrl();
    }

    private function getProperUrl()
    {
        $this->setUrlParameterValue();
        switch ($this->urlParameter)
        {
            case "private_league_id":
                $this->properUrlQuery = self::PRIVATE_LEAGUE_QUERY;
                $this->urlType        = 'private_league';
                $this->urlFolder      = FALSE;
                break;
            case "comp_id":
                $this->properUrlQuery = self::LEAGUE_QUERY;
                $this->urlType        = 'league_table';
                $this->urlFolder      = FALSE;
                break;
            case "user_team_id":
                $this->properUrlQuery = self::USER_TEAM_QUERY;
                $this->urlType        = 'user_team';
                $this->urlFolder      = FALSE;
                break;
            case "team_id":
                $this->properUrlQuery = self::TEAM_QUERY;
                $this->urlType        = 'team';
                $this->urlFolder      = FALSE;
                break;
            case "news_id":
                $this->properUrlQuery = self::NEWS_QUERY;
                $this->urlType        = 'news';
                $this->urlFolder      = FALSE;
                break;
            case "fixture_id":
                $this->properUrlQuery = self::MATCH_QUERY;
                $this->urlType        = 'match';
                $this->urlFolder      = $this->getFixtureDate($this->urlParameterValue);
                break;
        }

        $this->runSelectStatement();
        $this->properUrl = $this->urlGenerator->makeUrl($this->linkString, $this->urlParameterValue, $this->urlType, $this->urlFolder);
    }

    private function getFixtureDate($fixtureId)
    {
        $stmt = config::$mysqli->prepare("
        SELECT f.date
        FROM match_fixtures f
        WHERE f.fixture_id = ?
        GROUP BY f.fixture_id
        LIMIT 1");
        $stmt->bind_param("i", $fixtureId);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($fixtureDate);
        $stmt->fetch();

        return str_replace("-", "", $fixtureDate);
    }

    private function runSelectStatement()
    {
        if ($stmt = config::$mysqli->prepare($this->properUrlQuery))
        {
            $stmt->bind_param("i", $this->urlParameterValue);
            $stmt->execute();
            $stmt->bind_result($this->linkString);
            $stmt->fetch();
            $stmt->close();
            return;
        }
        printf("Errormessage: %s\n", config::$mysqli->error);
    }
}
