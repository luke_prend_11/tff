<?php
class news_article extends news
{	
	public function __construct($ug)
	{
		parent::__construct($ug);
		
		$this->checkUrlHasNewsId();
		$this->setNewsArticleInformation($this->articleId);
	}
	
	private function checkUrlHasNewsId()
	{
		if(isset($_GET['news_id']) && !empty($_GET['news_id']))
		{
			$this->setNewsIdFromUrl();
		}
		else
		{
			exit('Unauthorized Access');
		}
	}
	
	private function setNewsIdFromUrl()
	{
		$this->articleId = htmlspecialchars($_GET['news_id']);
	}
	
	public function displayNewsArticle()
	{
		echo '
		<p class="date">
			Posted on '.date('jS F Y \a\t G:i', strtotime($this->articleDate)).'
			<a href="#comments" title="Read Comments">'.$this->articleTotalComments.' Comments</a>
		</p>
		<img src="'.config::$baseUrl.'/img/content/news/'.$this->articleImage.'.jpg" alt="News Article Image" />'.
		$this->articleContent;
		
		$this->displayOtherNews();
	}
	
	private function displayOtherNews()
	{
		$this->setNewsArticleInformation($this->articleId, FALSE, TRUE);
		
		if($this->articleInformation)
		{
			echo '
			<div id="other-news">
				<h3 class="comment-heading2">You may also be interested in...</h3>
				<ul class="list-none">';
				
				while($this->articleInformation->fetch())
				{
					echo '
					<li>
						<a href="'.$this->urlGenerator->makeUrl($this->articleTitle, $this->articleId, 'news', 'A').'" title="'.$this->articleTitle.'">'.
							$this->articleTitle.'
						</a>
					</li>';
				}
			
				echo '
				</ul>
			</div>';
		}
		$this->articleInformation->close();
	}
	
	private function getNewsArticleComments()
	{
		
	}
	
	private function displayNewsArticleComments()
	{
		echo '
		<a name="comments" id="comments"></a>
		<h3 class="comment-heading2">'.$news_comments.' Comments</h3>';
		
		if(empty($news_comments)) {
			echo '<p>Be the first to leave a comment</p>';
		}
		else {
			$stmt = config::$mysqli->prepare("SELECT id, author, comment, date 
			FROM news_comments 
			WHERE nid = ?
			AND rejected <> 1
			ORDER BY date DESC");
			$stmt->bind_param("i", $news_id);
			$stmt->execute();
			$stmt->bind_result($comment_id, $comment_author, $comment_comment, $comment_date);
			
			while ($stmt->fetch()) {
				echo '<p class="comment-box">'.$comment_comment.'</p>
				<div class="comment-info">
				<p class="comment-name">'.$comment_author.'</p>
				<p class="comment-date">'.date('jS F Y \a\t G:i', strtotime($comment_date)).'</p>
			</div>';
			}
			$stmt->close();
		}
		
		echo '<h3 class="comment-heading1">Leave a Comment</h3>';

		if(isset($_POST['submit'])) {
		
			$errors = checkNewsComment($_POST['author'], $_POST['comment'], $_POST['validator'], $defualt_name_text, $defualt_comments_text);
			if(empty($errors)) {
				// no errors have occured the comment will be inserted by the code at the top of the page
				if(isset($insert_comment) && !empty($insert_comment)) {
					echo '<p class="success">Your comment was added succesfully!</p>';
					show_commentform($insert_comment, $defualt_name_text, $defualt_comments_text, $defualt_validator_text);
				}
				else {
					echo '<p class="error">There was an error when submitting your comment, please try again.</p>';
					show_commentform($insert_comment, $defualt_name_text, $defualt_comments_text, $defualt_validator_text);
				}
			}
			else {
				echo showNotification('error', TRUE, $errors);	
				show_commentform($insert_comment, $defualt_name_text, $defualt_comments_text, $defualt_validator_text);
			}
		}
		else {
		show_commentform($insert_comment, $defualt_name_text, $defualt_comments_text, $defualt_validator_text);
		}
	}
}
?>			