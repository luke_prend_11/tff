<?php
class notifications
{
    const ERROR_OPENING   = "Please correct the following";
    const SUCCESS_OPENING = "Success";
    const NO_ERRORS       = "There are no eroors to display.";
    const OPENING_MESSAGE = "<strong>%s:</strong><br />";
    
    public static $scoringPointsInfo;
    public static $formNotification;

    public function __construct()
    {
        $this->setScoringPointsInfo();
    }
    
    public static function showNotification($notificationType, $openingMessage = TRUE, $notificationMessageOverride = '')
    {
        $string = "<p class='".$notificationType."'>";
        if($openingMessage == TRUE)
        {
            $string .= $notificationType == "error" ? sprintf(self::OPENING_MESSAGE, self::ERROR_OPENING) : ($notificationType == "success" ? sprintf(self::OPENING_MESSAGE, self::SUCCESS_OPENING) : "");
        }
        
        if(!empty($notificationMessageOverride) && !is_array($notificationMessageOverride))
        {
            $string .= $notificationMessageOverride;
        }
        elseif(!empty($notificationMessageOverride) && is_array($notificationMessageOverride))
        {	
            foreach ($notificationMessageOverride as $notification)
            {
                $string .= $notification."<br />";
            }
        }
        elseif(is_array(self::$formNotification))
        {	
            foreach (self::$formNotification as $notification)
            {
                $string .= $notification."<br />";
            }
        }
        elseif(empty(self::$formNotification))
        {
            $string .= self::NO_ERRORS;
        }
        else
        {
            $string .= self::$formNotification;
        }

        $string .= "</p>";
        return $string;
    }

    private function setScoringPointsInfo()
    {
        self::$scoringPointsInfo = "
        <p>Teams will score points based on their real-life results. Each team will score points as follows:</p>
        
        <ul class='list-points'>
            <li>Win - 30 points</li>
            <li>Draw - 10 points</li>
            <li>Goal Scored - 5 points</li>
            <li>Clean Sheet - 5 points</li>
        </ul>
        
        <p><strong>Example 1</strong>: Man City 1-2 Liverpool</p>
        
        <ul class='list-points'>
            <li>Man City score 5 points - <em>5 points for 1 goal scored</em></li>
            <li>Liverpool score 40 points - <em>30 points for a win and 10 points for 2 goals they scored</em></li>
        </ul>
        
        <p><strong>Example 2</strong>: Chelsea 3-0 Tottenham</p>
        
        <ul class='list-points'>
            <li>Chelsea score 50 points - <em>30 points for a win, 15 points for 3 goals scored and 5 points for a clean sheet</em></li>
            <li>Tottenham score 0 points</li>
        </ul>
        
        <p>In cup matches when a teams opponent is in a division ranked higher than their own, their points will be multiplied by the number of divisions above they are plus 1. So if a teams opponent is 1 division above their own, then their points would be multiplied by 2, If they were playing a team 2 divisions above, then their points would be multiplied by 3 and so on. For teams who are in a division ranked higher than their opponent the standard points scoring will be applied.</p>
        <p><strong>Example 3</strong>: Man City 1-2 Portsmouth</p>
        
        <ul class='list-points'>
            <li>Man City score 5 points - <em>5 points for 1 goal scored (the standard amount of points given they are in a division ranked higher than their opponents)</em></li>
            <li>Portsmouth score 160 points - <em>120 points for a win and 40 points for 2 goals scored (the standard amount of points multiplied by 4 as they are ranked 3 divisions below)</em></li>
        </ul>";
    }
}
