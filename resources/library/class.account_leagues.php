<?php
class account_leagues extends account
{
	const DATABASE_ENTRY_ERROR     = 'There was an error inserting your private league into the database. Please check your list of leagues and try again if necessary';
	const NO_LEAGUES_ERROR_MESSAGE = 'You have not yet created or joined a private league';
	const NO_TEAM_SELECTED_ERROR   = 'Please select a Team to enter';
	const TEAM_EXISTS_ERROR        = 'The team you have selected is already entered into that private league';
	const NO_LEAGUE_NAME_ERROR     = 'Enter a League Name';
	const NO_LEAGUE_MATCH_ERROR    = 'The League Name you entered does not match any private League';
	const NO_PASSCODE_ERROR        = 'Enter a Passcode';
	const NO_PASSCODE_MATCH_ERROR  = 'The Passcode you entered is incorrect';
	const CREATE_LEAGUE_SUCCESS    = 'You have successfully created a private league with the following details:<br /><br />League Name: <strong>%s</strong><br />Passcode: <strong>%s</strong><br /><br />You will need to give the above details to anyone you would like to join your league.';
	const JOIN_LEAGUE_SUCCESS      = 'You have successfully joined a private league.';
	
	private $privateLeagueId;
	private $privateLeagueName;
	private $privateLeaguePasscode;
	private $privateLeagueTeamId;
	private $formErrors;
		
	public function __construct($db, $ug, $uf)
	{
		parent::__construct($db, $ug, $uf);
	}
	
	private function getUserLeagues()
	{
		//// NEEDS FINISHING OFF - select total number of teams in each league
		$stmt = config::$mysqli->prepare("
		SELECT pl.league_id,
			   pl.league_name,
		       pl.league_password,
			   CONCAT(u.first_name,' ',u.last_name) AS chairman
		FROM private_leagues pl
		INNER JOIN user_leagues ul
		ON pl.league_id = ul.league_id
		INNER JOIN user_teams ut
		ON ul.user_team_id = ut.user_team_id
        INNER JOIN members u
        ON pl.user_id = u.user_id
		WHERE ut.user_id = ?
		AND pl.date > ?
		GROUP BY pl.league_id
		");
		$stmt->bind_param("is", $this->userId, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		
		return $stmt;
	}
	
	private function showCreateLeagueBoxLink()
	{
		$this->createBoxLink('dark-grey', '/my-account/leagues/create', 'Create a League');
	}
	
	private function showJoinLeagueBoxLink()
	{
		$this->createBoxLink('blue', '/my-account/leagues/join', 'Join a League');
	}
	
	private function setPrivateLeagueName()
	{
		$this->privateLeagueName = $_POST['league_name'];
	}
	
	private function setPrivateLeaguePasscode()
	{
		$this->privateLeaguePasscode = $_POST['passcode'];
	}
	
	private function setPrivateLeagueTeamId()
	{
		$this->privateLeagueTeamId = $_POST['team'];
	}
	
	public function checkLeaguePageDisplay()
	{
		if(isset($_POST['Create_league']))
		{
			$this->setPrivateLeagueName();
			$this->generatePasscode();
			$this->setPrivateLeagueTeamId();
			$this->checkCreateLeague();
			
			if(empty($this->formErrors)) {
				$this->createLeagueSuccess();
			}
			else {
                                echo notifications::showNotification('error', TRUE, $this->formErrors);
				$this->showCreateLeague();
			}
		}
		elseif(isset($_POST['Join_league']))
		{
			$this->setPrivateLeagueName();
			$this->setPrivateLeaguePasscode();
			$this->setPrivateLeagueTeamId();
			$this->checkJoinLeague();
			
			if(empty($this->formErrors)) {
				$this->joinLeagueSuccess();
			}
			else {
                                echo notifications::showNotification('error', TRUE, $this->formErrors);
				$this->showJoinLeague();
			}
		}
		elseif(!empty($_GET['pleague_id']))
		{
			$l = new leaderboards($this->urlGenerator);
                        $l->showPrivateLeagueLeaderboardPage();
                }
		else
		{
			$page = !empty($_GET['page']) ? htmlspecialchars($_GET['page']) : 'home';
			
			switch($page)
			{
				case 'home':   $this->showUserLeagues();  break;
				case 'create': $this->showCreateLeague(); break;
				case 'join':   $this->showJoinLeague();   break;
			}
		}
	}
	
	private function showUserLeagues()
	{
		$stmt = $this->getUserLeagues();
		if($stmt->num_rows == 0)
		{
                        echo notifications::showNotification('error', FALSE, self::NO_LEAGUES_ERROR_MESSAGE);
			echo '<div class="stretch-parent">';	
			$this->showCreateLeagueBoxLink();
			$this->showJoinLeagueBoxLink();
			echo '<span class="stretch"></span>';
			echo '</div>';
			return;
		}
		else
		{
			echo '<div class="stretch-parent">';	  
			$stmt->bind_result($league_id, $league_name, $league_password, $chairman);
			while ($stmt->fetch()) {
				echo '
				<a class="box-info stretch-child" href="'.$this->urlGenerator->makeUrl($league_name, $league_id, 'private_league').'">
					<span class="team-name">'.$league_name.'</span>
					<span class="box-text">
						Chairman: <strong>'.$chairman.'</strong><br />
						League Passcode: <strong>'.$league_password.'</strong><br />
						Number of Teams: <strong></strong>
					</span>
				</a>';
			}

			$this->showCreateLeagueBoxLink();
			$this->showJoinLeagueBoxLink();
		}
		
		echo '<span class="stretch"></span>';
		echo '</div>';
		$stmt->close();
		return;
	}
	
	private function buildLeagueForm($form_type)
	{
		$formTypeLower = strtolower($form_type);
		echo '
		<div class="row">
			<h2 class="news dark-grey">'.$form_type.' a League</h2>
			<form name="'.$form_type.'_league" method="post" action="'.htmlspecialchars(config::$baseUrl.'/my-account/leagues/'.$formTypeLower).'" class="form" style="margin:0;">
				<div>'
					.$this->userFunctions->showStickyForm('text','league_name')
					.$this->userFunctions->showStickyForm('select','team',$this->userTeamNames,FALSE,'',$this->userTeamIds);
					if ($form_type == 'Join')
					{
						echo $this->userFunctions->showStickyForm('text','passcode');
					}
			echo '</div>
				<input type="submit" value="'.$form_type.' League" name="'.$form_type.'_league" id="Submit Form" class="button" />
			</form>
		</div>
		';
	}
	
	private function showCreateLeague()
	{
		$this->buildLeagueForm('Create');
	}
	
	private function showJoinLeague()
	{
		$this->buildLeagueForm('Join');
	}
	
	private function checkCreateLeague()
	{
		if(empty($this->privateLeagueName)) {
			$this->formErrors[] = self::NO_LEAGUE_NAME_ERROR;
		}
		if(empty($this->privateLeagueTeamId)) {
			$this->formErrors[] = self::NO_TEAM_SELECTED_ERROR;
		}
	}
	
	private function checkJoinLeague()
	{
		if(empty($this->privateLeagueName)) {
			$this->formErrors[] = self::NO_LEAGUE_NAME_ERROR;
		}
		else {
			if($this->checkLeagueName()) {
				$this->formErrors[] = self::NO_LEAGUE_MATCH_ERROR;
			}
		}
		if(empty($this->privateLeagueTeamId)) {
			$this->formErrors[] = self::NO_TEAM_SELECTED_ERROR;
		}
		else
		{
			if($this->checkTeamEntryExists()) {
				$this->formErrors[] = self::TEAM_EXISTS_ERROR;
			}
		}
		if(empty($this->privateLeaguePasscode)) {
			$this->formErrors[] = self::NO_PASSCODE_ERROR;
		}
		else
		{
			if($this->checkPasscode()) {
				$this->formErrors[] = self::NO_PASSCODE_MATCH_ERROR;
			}
		}
	}
	
	private function checkTeamEntryExists()
	{
		$this->privateLeagueId = $this->getLeagueId();
		
		$stmt = config::$mysqli->prepare("
		SELECT user_team_id
		FROM user_leagues
		WHERE user_team_id = ?
		AND league_id = ?
		AND date > ?
		");
		$stmt->bind_param("iss", $this->privateLeagueTeamId, $this->privateLeagueId, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows > 0)
		{
			return true;
		}
	}
	
	private function checkLeagueName()
	{
		$stmt = config::$mysqli->prepare("
		SELECT league_name
		FROM private_leagues
		WHERE league_name = ?
		AND date > ?
		");
		$stmt->bind_param("ss", $this->privateLeagueName, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows == 0)
		{
			return true;
		}
	}
	
	private function checkPasscode()
	{
		$stmt = config::$mysqli->prepare("
		SELECT league_password
		FROM private_leagues
		WHERE league_password = ?
		AND league_name = ?
		AND date > ?
		");
		$stmt->bind_param("iss", $this->privateLeaguePasscode, $this->privateLeagueName, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows == 0)
		{
			return true;
		}
	}
	
	private function generatePasscode()
	{
		$passcode = mt_rand(100001,999999);
		
		while($this->checkPasscodeUsed($passcode))
		{
			$passcode = mt_rand(100001,999999);
		}
		
		$this->privateLeaguePasscode = $passcode;
	}
	
	private function checkPasscodeUsed($passcode)
	{
		$stmt = config::$mysqli->prepare("
		SELECT league_password
		FROM private_leagues
		WHERE league_password = ?
		");
		$stmt->bind_param("i", $passcode);
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows > 0)
		{
			return false;
		}
	}
	
	private function createLeagueSuccess()
	{
		$this->insertCreateLeague();
		echo notifications::showNotification('success', TRUE, sprintf(self::CREATE_LEAGUE_SUCCESS, $this->privateLeagueName, $this->privateLeaguePasscode));
		$this->showInvitePlayers();
	}
	
	private function joinLeagueSuccess()
	{
		$this->insertJoinLeague();
		echo notifications::showNotification('success', TRUE, self::JOIN_LEAGUE_SUCCESS);
		$this->showInvitePlayers();
	}
	
	private function showInvitePlayers()
	{
		// echo '<h2>Challenge a Friend</h2>';
		// echo '<p>Invite more players to join:</p>';
		// give people the league name and passcode above or use email or social media below:
		// send an email to upto 10 people
		// post on facebook or twitter
	}
	
	private function insertCreateLeague()
	{
		if($this->db->completeInsert(
			'private_leagues', 
			array('league_name', 'league_password', 'user_id', 'date'), 
			array($this->privateLeagueName, $this->privateLeaguePasscode, $this->userId, config::$curDate), 
			array('s', 'i', 'i', 's'))) 
		{
			$this->insertJoinLeague();
		}
		else {
			echo notifications::showNotification('error', TRUE, self::DATABASE_ENTRY_ERROR);
		}
	}
	
	private function insertJoinLeague()
	{
		$this->setLeagueId();
		if($this->db->completeInsert(
			'user_leagues', 
			array('user_team_id', 'league_id', 'date'), 
			array($this->privateLeagueTeamId, $this->privateLeagueId, config::$curDate), 
			array('i', 'i', 's'))) 
		{
			return true;
		}
		else {
			echo notifications::showNotification('error', TRUE, self::DATABASE_ENTRY_ERROR);
		}
	}
	
	private function getLeagueId()
	{
		$stmt = config::$mysqli->prepare("
		SELECT league_id
		FROM private_leagues
		WHERE league_password = ?
		AND league_name = ?
		AND date > ?
		");
		$stmt->bind_param("iss", $this->privateLeaguePasscode, $this->privateLeagueName, config::$curSeasonStart);
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows == 0)
		{
			return false;
		}
		else
		{
			$stmt->bind_result($league_id);
			$stmt->fetch();
			return $league_id;
		}
	}
	
	private function setLeagueId()
	{
		$this->privateLeagueId = $this->getLeagueId() ? $this->getLeagueId() : $this->db->insertId;
	}
}
?>
