<?php
$lg->notLoggedInRedirect();
require_once (__DIR__.'/../../resources/templates/tpl.header.php');
$ac = new account($db, $ug, $uf);

if(isset($_SESSION['user_team'])) {
?>

		<div id="team-menu">
			<ul class="stretch-parent">
				<li class="stretch-child blank"></li>
				<li class="stretch-child"><a href="<?php echo config::$baseUrl; ?>/my-account/teams/" title="Team Overview">Overview</a></li>
				<li class="stretch-child border"></li>
				<li class="stretch-child"><a href="<?php echo config::$baseUrl; ?>/my-account/teams/stats.php" title="Team Stats">Stats</a></li>
				<li class="stretch-child border"></li>
				<li class="stretch-child"><a href="<?php echo config::$baseUrl; ?>/my-account/teams/matches.php" title="Team Matches">Matches</a></li>
				<li class="stretch-child border"></li>
				<li class="stretch-child"><a href="<?php echo config::$baseUrl; ?>/my-account/teams/transfers.php" title="Team Transfers">Transfers</a></li>
				<li class="stretch-child border"></li>
				<li class="stretch-child"><a href="<?php echo config::$baseUrl; ?>/my-account/teams/history.php" title="Team Transfer History">Transfer History</a></li>
				<li class="stretch-child blank"></li>
				<span class="stretch"></span>
			</ul>
		</div>
		
<?php
}
?>
		<h2><?php echo $mc->pageH2; ?></h2>
		
		
