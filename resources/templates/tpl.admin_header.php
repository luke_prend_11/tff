<?php
$a_login = new admin_login(); 
// check if logged in and redirect to login page if necessary
if($a_login->login_check(config::$mysqli) == false)
{ 
	header('Location: '.config::$baseUrl.'/admin/login.php');
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo $mc->pageTitle; ?></title>
<meta name="Description" content="<?php echo $mc->metaDescription; ?>" />
<meta name="Developer" content="Luke Prendergast" />

<link href="<?php echo config::$baseUrl; ?>/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo config::$baseUrl; ?>/css/master.css" rel="stylesheet" type="text/css" />
<link href="<?php echo config::$baseUrl; ?>/css/admin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo config::$baseUrl; ?>/css/forms.css" rel="stylesheet" type="text/css" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:800,700,400,300' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo config::$baseUrl; ?>/img/favicon.ico" type="image/x-icon" />

<script src="<?php echo config::$baseUrl; ?>/js/responsive-nav.js"></script>
<!-- Force html5 elements to work in ie7 and ie8 -->
<!--[if lte IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- Force media queries to work in ie7 and ie8 -->
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/respond.js"></script>
</head>
<body>
<header>
	<div class="content">
		<p class="header">TFF Admin | <a href="../../admin/resources/templates/functions/logout.php">Log Out</a> | <a href="<?php echo config::$baseUrl; ?>">Main Site</a></p>
		
		<div id="menu">
			<nav class="nav-collapse">
				<ul>
					<li><a href="<?php echo config::$baseUrl; ?>/admin/">Home</a></li>
					<li><a href="<?php echo config::$baseUrl; ?>/admin/administrators.php">Administrators</a></li>
					<li><a href="<?php echo config::$baseUrl; ?>/admin/email.php">Email Members</a></li>
					<li><a href="<?php echo config::$baseUrl; ?>/admin/member-update.php">Member Update</a></li>
					<li class="align-right">
						<a href="<?php echo config::$baseUrl; ?>/admin/leaderboards.php">Leaderboards</a>
						<ul>
							<div class="content">
								<li><a href="<?php echo config::$baseUrl; ?>/admin/leaderboards.php?id=1">Overall</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/leaderboards.php?id=2">Month</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/leaderboards.php?id=3">Week</a></li>
							</div>
						</ul>
					</li>
					<li class="align-right">
						<a href="#" title="Leaderboards">Website Updates</a>
						<ul>
							<div class="content">
								<li><a href="<?php echo config::$baseUrl; ?>/admin/fixtures.php">Fixtures</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/results.php">Results</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/teams.php">Teams</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/leagues.php">Private Leagues</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/news/">News</a></li>
								<li><a href="<?php echo config::$baseUrl; ?>/admin/data/import.php">Import Results BETA</a></li>
							</div>
						</ul>
					</li>
				</ul>
			</nav>
		</div>	
	</div>
	<br class="clearfloat" />
</header>

<div class="content">
	<?php
	echo '<h1>'.$mc->pageH1.'</h1>';
	?>