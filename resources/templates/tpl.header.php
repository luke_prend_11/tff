<?php date_default_timezone_set('Europe/London'); ?>
<?php
// check if login button has been pressed and if credentials are correct redirect to account page
if (isset($_POST['login']))
{
    $lg->checkLogin($_POST['email_address'], $_POST['password']);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
if(config::$testingMode !== TRUE && !strpos(config::BASE_URL_LIVE_TESTING, $_SERVER['SERVER_NAME']))
{
    include_once($_SERVER["DOCUMENT_ROOT"]."/resources/analyticsTracking.php");
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo $mc->pageTitle; ?></title>
<meta name="Description" content="<?php echo $mc->metaDescription; ?>" />
<meta name="Keywords" content="<?php echo $mc->metaKeywords; ?>" />
<meta name="Developer" content="Luke Prendergast" />

<link rel="stylesheet" type="text/css" href="<?php echo config::$baseUrl; ?>/css/reset.css" />
<link rel="stylesheet" type="text/css" href="<?php echo config::$baseUrl; ?>/css/master.css" />
<?php echo $mc->css; ?>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:800,700,400,300' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo config::$baseUrl; ?>/img/favicon.ico.png" type="image/x-icon" />

<script src="<?php echo config::$baseUrl; ?>/js/responsive-nav.js"></script>
<!-- Force html5 elements to work in ie7 and ie8 -->
<!--[if lte IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- Force media queries to work in ie7 and ie8 -->
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/respond.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo config::$baseUrl; ?>/js/less.min.js" type="text/javascript"></script>

<?php
// if EU Cookie Law hasn't been accepted then run script
if(!isset($_COOKIE['eucookie'])) {
?>
<!-- If EU Cookie Law hasn't been accepted then run script -->
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/eu-cookie.js"></script>
<?php
}

// only load header scroll on required pages
if(isset($header_scroll) && $header_scroll == TRUE) {
?>
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/header-scroll.js"></script>
<?php
}

// only load submit a team scripts on submit a team page
if(isset($submit_team) && $submit_team == TRUE) {
?>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript">google.load("jquery", "1")</script>
<script type="text/javascript">
$(document).ready(function()
{	
var total = <?php echo config::BUDGET; ?>;
	
function calcTotal()
{
	$("input:checked").each(function()
	{
		//This happens for each checked input field
		var value = $(this).attr("id");
		total -= parseInt(value); 
	});
}

function negativeTotal()
{
	if(total < 0 && d.className.indexOf('negative') <= 0)
	d.className  = d.className.replace(' positive', ''),
	d.className += ' negative';
}

//This happens when the page loads
calcTotal();
$('#total').before('<span id="budget-remaining" class="remaining positive">£' + total + 'm</span>');

var d = document.getElementById("budget-remaining");
negativeTotal();
		
$("input:checkbox, input:radio").click(function()
	{
		total = <?php echo config::BUDGET; ?>;
		calcTotal();
		$("span.remaining").html("£" + total + "m");
		
		negativeTotal();
			
		if(total >= 0 && d.className.indexOf('positive') <= 0)
			d.className  = d.className.replace(' negative', ''),
			d.className += ' positive';
	});
});
</script>
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/fixed-wrapper.js"></script>
<?php
}
?>
</head>
<body>
<?php
// If EU Cookie Law hasn't been accepted then display message and run script
if(!isset($_COOKIE['eucookie'])) {
?>
	<div id="eucookielaw" >
		<div class="content">
			<p>This website uses cookies to track visitor activity and behaviour. By browsing our site you agree to our use of cookies.</p>
			<a id="removecookie" title="Accept Cookies" class="btn dark-grey float-left margin-right">Accept</a>
			<a id="more" href="<?php echo config::$baseUrl; ?>/help/privacy.php" title="Privacy Policy" class="btn dark-grey float-left">Find Out More</a>
			<br class="clearfloat" />
		</div>
	</div>
	<script type="text/javascript">
    if( document.cookie.indexOf("eucookie") ===-1 ){
    $("#eucookielaw").show();
    }
    $("#removecookie").click(function () {
    SetCookie('eucookie','eucookie',365*10);
    $("#eucookielaw").remove();
    });
    </script>
<?php
}
?>
	<header>
		<div id="menu-bar">
			<div class="content">
				<a href="<?php echo config::$baseUrl; ?>" title="<?php echo config::SITE_NAME; ?> Home">
					<img src="<?php echo config::$baseUrl; ?>/img/content/logo.png" alt="<?php echo config::SITE_NAME; ?> Logo" width="330" height="30" class="logo" />
				</a>

				<?php echo navigation::$navMenu; ?>
			</div>
			
			<br class="clearfloat" />
			
			<?php echo navigation::$activeSubMenu; ?>
		</div>
<?php if(config::FANTASY_FOOTBALL_ACTIVE == TRUE)
{
    ?>

		<div id="login" class="very-light-grey">
			<div class="content">
				<?php $lg->showLoginStatus(); ?>
			</div>
		</div>
<?php
}
?>
		
		<?php 
		if(config::$currentPageRoute == 'index')
		{ 
		?>
		<div id="home-banner">
			<div class="content">
                            <?php if(config::FANTASY_FOOTBALL_ACTIVE === TRUE) { ?>
				<h1>
					Free <br class="show-2" />Fantasy <br class="show-3" />Football<br />
					<span><?php echo config::$curSeasonName; ?></span>
				</h1>
				<div id="center-list" class="hide-3">
					<ul>
						<li>11 Teams<span class="hide-2"></span></li>
						<li>£5.7bn Budget<span class="hide-1"></span><br class="show-1" /></li>
						<li>40 Transfers<span class="hide-2"></span></li>
						<li>Private Leagues</li>
					</ul>
				</div>
				<?php echo navigation::$playNowUrl; ?>
				<br class="clearfloat" />
                            <?php } else { ?>
                                <h1>
					Free <br class="show-2" />Football <br class="show-3" />Stats<br />
					<span><?php echo config::$curSeasonName; ?></span>
				</h1>
				<div id="center-list" class="hide-3">
					<ul>
						<li>Comprehensive Stats<span class="hide-2"></span></li>
						<li>Betting Tips<span class="hide-1"></span><br class="show-1" /></li>
						<li>Free Bet Offers</li>
					</ul>
				</div>
				<br class="clearfloat" />
                            <?php } ?>
			</div>
		</div>
		
                            <?php if(config::FANTASY_FOOTBALL_ACTIVE === TRUE) { ?>
		<div id="home-intro" class="very-light-grey">
			<div class="content">
				<p class="intro">Welcome to <?php echo config::SITE_NAME; ?>...a <strong>FREE</strong> to play <strong>fantasy football</strong> game unlike any other.</p>
				<p>This unique fantasy football game requires you to select <strong>11 teams</strong> from a <strong>£5.7bn budget</strong>, with teams scoring points based on real life results. You have unlimited transfers until the season starts and a total of <strong>40 transfers</strong> to keep your selection of clubs at maximum strength throughout the season. Manage your teams to victory in the new <strong>Ultimate Leagues</strong> and challenge friends and colleagues in <strong>Private Leagues</strong> to compete for the ultimate fantasy football bragging rights!</p>
				<!--<p>Not only that but we offer some of the best <strong>bettings tips</strong> you will find. Our formula has been back tested and tweaked until a <strong>70% win ratio</strong> was reached. Start beating the bookies today!</p>-->
			</div>
		</div>
		<?php
                            }
		}
		else
		{
			echo '
			<div id="breadcrumb">
				<div class="content">'
				.url_utils::$fullBreadcrumb.
				'</div>
			</div>';
		}
		?>
	</header>
	
	<div class="content">
		<div id="ad-bar">
                <?php
                if(config::$testingMode !== TRUE && !strpos(config::BASE_URL_LIVE_TESTING, $_SERVER['SERVER_NAME']))
                {
                ?>
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- TFF Wide Skyscraper 160x600 -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:160px;height:600px"
                         data-ad-client="ca-pub-2285741200737359"
                         data-ad-slot="9489948714"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                <?php 
                }
                ?>
		</div>
		<div id="outer">
			<div id="main-content">
				<?php
				// show breadcrumb if current page is not the home page
                                if(config::$currentPageRoute != 'index')
				{
					echo '<h1>'.$mc->pageH1.'</h1>';
				}
				?>
