			<br class="clearfloat" />
			</div>
		</div>
	</div>
        
        <br class="clearfloat" />
        <div id="footer-top"></div>
	<footer>
		<div class="content">
			<div class="stretch-parent">
				<div class="stretch-child">
					<h4>Quick Links</h4>
					<ul>
						<li><a href="<?php echo config::$baseUrl; ?>" title="Home Page">Home</a></li>
						<!--<li><a href="<?php echo config::$baseUrl; ?>/registration.php" title="Sign Up Now">Sign Up</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/my-account/" title="My Account">My Account</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/stats/leaderboards/" title="Leaderboards">Leaderboards</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/help/prizes.php" title="Prizes">Prizes</a></li>-->
						<li><a href="<?php echo config::$baseUrl; ?>/stats/" title="Statistics">Stats</a></li>
					</ul>
				</div>
				<div class="stretch-child">
					<h4>Need Help?</h4>
					<ul>
						<li><a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us">About Us</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us">Contact Us</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/help/privacy.php" title="Privacy Policy">Privacy Policy</a></li>
						<!--<li><a href="<?php echo config::$baseUrl; ?>/help/rules.php" title="How to Play">How to Play</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/site-map.php" title="Site Map">Site Map</a></li>-->
					</ul>
				</div>
				<div class="stretch-child">
					<h4>Social Updates</h4>
					<ul>
						<li><a href="<?php echo config::FACEBOOK; ?>" title="Like Us On Facebook" target="_blank">Facebook</a></li>
					</ul>
				</div>
				<span class="stretch"></span>
			</div>
		</div>
			
		<div class="content">
			<p><?php echo config::DOMAIN; ?> &copy; <?php echo date('Y'); ?> | Website Designed &amp; Developed by <a href="https://lpwebdesign.co.uk" title="LP Web Design" target="_blank">LP Web Design</a></p>
		</div>
	</footer>
<script>
  var nav = responsiveNav(".nav-collapse");
</script>
<?php
// only load header scroll on required pages
if(isset($header_scroll) && $header_scroll == TRUE) {
?>
<script type="text/javascript">
var jsvar = ".<?php if(isset($_SESSION['anchor'])) echo $_SESSION['anchor']; ?>"

$(document).ready(function(){
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $(jsvar).offset().top - 100
    }, 2000);
    return false;
});
</script>
<?php if(isset($_SESSION['anchor'])) unset($_SESSION['anchor']);
}
?>
</body>
</html>
